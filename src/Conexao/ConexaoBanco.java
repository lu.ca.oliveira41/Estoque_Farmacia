package Conexao;

import DAO.UsuarioDAO;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipException;
import javax.swing.JOptionPane;
import org.hsqldb.lib.tar.DbBackup;
import org.hsqldb.lib.tar.DbBackupMain;
import org.hsqldb.lib.tar.TarMalformatException;


public final class ConexaoBanco {
    private static Connection conexao;
    private static final String database = "farmacia";
    private static final String url = "jdbc:hsqldb:file:farmacia";
    private static final String username =  "SA";
    private static final String senha = "";
    
    private static final String criarApresentacao ="CREATE TABLE if not exists apresentacao (" +
                                            "idapresentacao int NOT NULL IDENTITY," +
                                            "tipoApresentacao varchar(45) NOT NULL," +
                                            "PRIMARY KEY (idapresentacao)"+
                                            ");";
    private static final String criarConcentracao ="CREATE TABLE if not exists concentracao (" +
                                            "  idconcentracao int NOT NULL IDENTITY," +
                                            "  tipoConcentracao varchar(45) NOT NULL," +
                                            "  PRIMARY KEY (idconcentracao)"+
                                            ");";
    
    private static final String criarModalidade ="CREATE TABLE if not exists modalidade (" +
                                            "idmodalidade int NOT NULL IDENTITY," +
                                            "tipoModalidade varchar(45) NOT NULL," +
                                            "PRIMARY KEY (idmodalidade)"+
                                            ");";
    private static final String criarUnidade ="CREATE TABLE if not exists unidade (" +
                                    "idunidade int NOT NULL IDENTITY," +
                                    "descricao varchar(45) NOT NULL," +
                                    "PRIMARY KEY (idunidade)"+
                                    ");";
    
    private static final String criarEstoque="CREATE TABLE if not exists estoque (" +
                                     " idestoque int NOT NULL IDENTITY," +
                                    " quantidade int NOT NULL," +
                                    " validade timestamp NOT NULL," +
                                    "PRIMARY KEY (idestoque)"+
                                    ");";
    
    private static final String criarMedicamento ="CREATE TABLE if not exists  medicamento (" + 
                                            "  idmedicamento int NOT NULL IDENTITY," +
                                            "  dcb varchar(1000) NOT NULL," +
                                            "  idapresentacao int NOT NULL," +
                                            "  idconcentracao int NOT NULL," +
                                            "  idmodalidade int NOT NULL," +
                                            "  PRIMARY KEY (idmedicamento),"+
                                            "  FOREIGN KEY (idapresentacao) REFERENCES apresentacao(idapresentacao)," +
                                            "  FOREIGN KEY (idconcentracao) REFERENCES concentracao(idconcentracao)," +
                                            "  FOREIGN KEY (idmodalidade) REFERENCES modalidade(idmodalidade)" +
                                            ");";
    
    private static final String criarPPS ="CREATE TABLE if not exists pps (" +
                                   "idpps int NOT NULL IDENTITY," +
                                   "descricao varchar(1000) NOT NULL," +
                                   "idunidade int NOT NULL," +
                                   "idmodalidade int NOT NULL," +
                                   "PRIMARY KEY (idpps),"+
                                   "  FOREIGN KEY (idunidade) REFERENCES unidade (idunidade)," +
                                   "  FOREIGN KEY (idmodalidade) REFERENCES modalidade (idmodalidade)" +
                                   ");";
    
    private static final String criarEstoqueMed = "CREATE TABLE if not exists estoquemed (" + 
                                            "  idestoqueMed int NOT NULL IDENTITY," +
                                            "  lote varchar(45) NOT NULL," +
                                            "  idmedicamento int NOT NULL," +
                                            "  idestoque int NOT NULL,"+
                                            "  PRIMARY KEY (idestoqueMed,lote),"+
                                            "  FOREIGN KEY (idmedicamento) REFERENCES medicamento (idmedicamento)," +
                                            "  FOREIGN KEY (idestoque) REFERENCES estoque (idestoque)" +
                                            ") ;";
    
    private static final String criarEstoquePps ="CREATE TABLE if not exists estoquepps (" +
                                          " idestoquePps int NOT NULL IDENTITY," +
                                          " lote varchar(45) NOT NULL," +
                                          " idpps int NOT NULL," +
                                          " idestoque int NOT NULL," +
                                          "  PRIMARY KEY (idestoquePps,lote),"+
                                          "  FOREIGN KEY (idpps) REFERENCES pps (idpps)," +
                                          "  FOREIGN KEY (idestoque) REFERENCES estoque (idestoque)" +
                                          ") ;";
   
    
    private static final String criarSaida ="CREATE TABLE if not exists saida (" +
                                    "idsaida int NOT NULL IDENTITY," +
                                    "quantidade int NOT NULL," +
                                    "dataSaida timestamp NOT NULL," +
                                    "idestoque int NOT NULL," +
                                    "PRIMARY KEY (idsaida),"+
                                    "FOREIGN KEY (idestoque) REFERENCES estoque (idestoque)" +
                                    ");";

    private static final String criarUsuario ="CREATE TABLE if not exists usuario (" +
                                        " idusuario int NOT NULL IDENTITY," +
                                        " nome varchar(50) NOT NULL," +
                                        " senha varchar(8) NOT NULL," +
                                        " PRIMARY KEY (idusuario)"+
                                        ");";
    private static final String criarEntrada=  "CREATE TABLE if not exists entrada (" + 
                                        "  identrada int NOT NULL IDENTITY," +
                                        "  quantidade int NOT NULL," +
                                        "  dataEntrada timestamp NOT NULL," +
                                        "  idestoque int NOT NULL," +
                                        "  PRIMARY KEY (identrada),"+
                                        "  FOREIGN KEY (idestoque) REFERENCES estoque (idestoque)"+
                                        ");";
    static String[] querys =   {criarApresentacao,criarConcentracao,criarModalidade,criarUnidade,criarEstoque,criarPPS,criarMedicamento,
                        criarEstoqueMed,criarEstoquePps,criarSaida,criarUsuario,criarEntrada
                        };
    public static void iniciarConexao() throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        try{ 
            Class.forName("org.hsqldb.jdbc.JDBCDriver").newInstance();
            conexao =  DriverManager.getConnection(url,username,senha);
            criaTabelas();
            UsuarioDAO.novoUser();
        }     
        catch( SQLException error ){
            System.out.println(error.getMessage());
        }
    }
     public static void retomaConexao() throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        try{ 
            Class.forName("org.hsqldb.jdbc.JDBCDriver").newInstance();
            conexao =  DriverManager.getConnection(url,username,senha);
            criaTabelas();
        }     
        catch( SQLException error ){
            System.out.println("Nao criou de novo"+error.getMessage());
        }
    }
    public static java.sql.Connection getConexao(){
        return ConexaoBanco.conexao;
    }
    public static void fechaConexao() throws SQLException{
        conexao.close();
        conexao = null;
    }
    public static void criaTabelas() throws SQLException{
        for (String query : querys) {
            PreparedStatement preparedStatement = conexao.prepareStatement(query);
            preparedStatement.execute();
           
        }
    }
    public static void backupBanco(String caminho){
        String tabelasdebackup = "BACKUP DATABASE TO '"+caminho+"\\' BLOCKING";
        try {
            PreparedStatement preparedStatement = conexao.prepareStatement(tabelasdebackup);
            preparedStatement.execute();
            JOptionPane.showMessageDialog(null, "Backup realizado","AVISO",JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possivel realizar o BackUp","AVISO",JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public static boolean restaurarBackUp(File caminho) throws SQLException, ClassNotFoundException, IOException, TarMalformatException, InstantiationException, IllegalAccessException, InterruptedException{    
        File arquivo = new File("arquivo.extensao");
        File propertiesAntigo = new File(arquivo.getAbsoluteFile().getParent()+"/farmacia.properties");     
        File scriptAntigo = new File(arquivo.getAbsoluteFile().getParent()+"/farmacia.script");
              
        try {
            PreparedStatement preparedStatement = conexao.prepareStatement("SHUTDOWN");
            preparedStatement.executeUpdate();
            
            propertiesAntigo = new File(arquivo.getAbsoluteFile().getParent()+"/farmacia.properties");     
            scriptAntigo = new File(arquivo.getAbsoluteFile().getParent()+"/farmacia.script");
            
            propertiesAntigo.renameTo(new File("temporario.properties"));
            scriptAntigo.renameTo(new File("temporario.script"));
            propertiesAntigo.delete();
            scriptAntigo.delete();
            
            DbBackupMain.main(new String[] {
                "--extract", caminho.getAbsolutePath(),
                 arquivo.getAbsoluteFile().getParent()
            });
            
            propertiesAntigo = new File(arquivo.getAbsoluteFile().getParent()+"/temporario.properties");     
            scriptAntigo = new File(arquivo.getAbsoluteFile().getParent()+"/temporario.script");
            propertiesAntigo.delete();
            scriptAntigo.delete();
            retomaConexao();
            return true;
            
        } catch (IllegalStateException|ZipException ex) {
            System.out.println(propertiesAntigo);
            propertiesAntigo.delete();
            scriptAntigo.delete();
            File properties = new File(arquivo.getAbsoluteFile().getParent()+"/temporario.properties");     
            File script = new File(arquivo.getAbsoluteFile().getParent()+"/temporario.script");
            properties.renameTo(new File("farmacia.properties"));
            script.renameTo(new File("farmacia.script"));
            System.out.println(properties);
            properties.delete();
            script.delete();
        }
        
            
        retomaConexao();
        return false;
        
    }
   
}
