package Interfaces;

import DAO.ModalidadeDAO;
import Dominio.Modalidade;
import Utils.ModelTable;
import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class ConfigGerenciarModalidade extends javax.swing.JPanel {
    List<Modalidade> modalidades = new ArrayList<>();
    public ConfigGerenciarModalidade() {
        initComponents();
         iniciarTabelaModalidade();
    }
    public void iniciarTabelaModalidade(){
        tabelaModalidade.removeAll();
        try {
           modalidades = ModalidadeDAO.selectModalidade();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode()+":"+ex.getMessage());
        }
        String[] colunas = {"CÓDIGO","TIPO DE MODALIDADE"};
        ArrayList dados = new ArrayList();
        for(Modalidade modalidade:modalidades){
            dados.add(new Object[]{
                modalidade.getId(),
                modalidade.getModalidade()
            });
        }
        ModelTable model = new ModelTable(dados, colunas);
        tabelaModalidade.setModel(model);
        tabelaModalidade.getColumnModel().getColumn(0).setMaxWidth(100);
        tabelaModalidade.getTableHeader().setResizingAllowed(false);
        tabelaModalidade.getTableHeader().setReorderingAllowed(false);
        tabelaModalidade.getSelectionModel().addListSelectionListener(selecionaLinha());
        tabelaModalidade.repaint();
    }
    private ListSelectionListener selecionaLinha() {
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tabelaModalidade.getSelectedRow() != -1){
                    int cod = (int) tabelaModalidade.getValueAt(tabelaModalidade.getSelectedRow(), 0);
                    try {
                        Modalidade modalidade = ModalidadeDAO.selectModalidadeByID(cod);
                        Object[] options = {"Editar","Excluir"};
                        int resposta = JOptionPane.showOptionDialog(getParent(),
                            "O que deseja fazer?",
                            "Modalidade: "+modalidade.getModalidade(),
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
                    if(resposta == 0)
                       editarInfo(modalidade); 
                    else if(resposta == 1)
                        confirmaExclusao(cod);
                    tabelaModalidade.clearSelection();
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                    
                }
            }
        };
    }
    private void confirmaExclusao(int cod){
       Object[] options = {"Confirmar","Cancelar"};
       int resposta = JOptionPane.showOptionDialog(getParent(), "Deseja realmente excluir?",
               null,
               JOptionPane.DEFAULT_OPTION,
               JOptionPane.QUESTION_MESSAGE,null,options,null);
       if(resposta == 0)
        try {
            ModalidadeDAO.delete(cod);
            revalida();
        } catch (SQLException ex) {
          JOptionPane.showMessageDialog(getParent(), "Não foi possivel excluir o registro",
                    "Erro de exclusão",JOptionPane.WARNING_MESSAGE);
        }
           
    }
    private void editarInfo(Modalidade modalidade){
        String novoNome = null;
        boolean result = false;
        while(novoNome == null || novoNome.equals("") ){
            novoNome =  JOptionPane.showInputDialog(getParent(), "Novo nome para a modalidade:",
                            "Editar Informação da Modalidade: "+modalidade.getModalidade(),
                            JOptionPane.PLAIN_MESSAGE);
            if(novoNome != null)
                if( novoNome.equals("") ){
                    JOptionPane.showMessageDialog(getParent(), "Insira um nome valido",
                            "Erro ao atualizar dados",JOptionPane.ERROR_MESSAGE);
                }else{
                    try{
                        modalidade.setModalidade(novoNome);
                        result =ModalidadeDAO.updateModalidadeByid(modalidade);
                        revalida();
                        JOptionPane.showMessageDialog(getParent(), "Atualização realizada com sucesso",
                        "",JOptionPane.INFORMATION_MESSAGE);
                        break;
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                }
            else
                break;
        }
        
    }
    public void revalida(){
        iniciarTabelaModalidade();
        tabelaModalidade.repaint();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelModalidade = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaModalidade = new javax.swing.JTable();
        btNovaModalidade = new javax.swing.JButton();

        setOpaque(false);

        labelModalidade.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        labelModalidade.setForeground(new java.awt.Color(255, 255, 255));
        labelModalidade.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelModalidade.setText("Modalidades Registradas");

        tabelaModalidade.setBackground(new java.awt.Color(0, 153, 153));
        tabelaModalidade.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tabelaModalidade.setForeground(new java.awt.Color(255, 255, 255));
        tabelaModalidade.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelaModalidade.setToolTipText("");
        jScrollPane1.setViewportView(tabelaModalidade);

        btNovaModalidade.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btNovaModalidade.setText("Nova Modalidade");
        btNovaModalidade.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btNovaModalidade.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btNovaModalidadeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btNovaModalidadeMouseExited(evt);
            }
        });
        btNovaModalidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNovaModalidadeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(165, 165, 165)
                        .addComponent(labelModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btNovaModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btNovaModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btNovaModalidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNovaModalidadeActionPerformed
        String novoNome = null;
        boolean result = false;
        while(novoNome == null || novoNome.equals("") ){
            novoNome =  JOptionPane.showInputDialog(getParent(), "Nome da nova modalidade:",
                "Cadastrar nova modalidade",
                JOptionPane.PLAIN_MESSAGE);
            if(novoNome != null)
                if(novoNome.equals("") ){
                    JOptionPane.showMessageDialog(getParent(), "Insira um nome valido",
                        "Erro ao cadastrar novos dados",JOptionPane.ERROR_MESSAGE);
                }else{
                    try{
                        Modalidade modalidade = new Modalidade(novoNome);
                        result = ModalidadeDAO.insertModalidade(modalidade);
                        revalida();
                        JOptionPane.showMessageDialog(getParent(), "Cadastro realizado com sucesso",
                            "",JOptionPane.INFORMATION_MESSAGE);
                        break;
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                }
            else
                break;
            
        }
        
    }//GEN-LAST:event_btNovaModalidadeActionPerformed

    private void btNovaModalidadeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btNovaModalidadeMouseEntered
        btNovaModalidade.setBackground(new Color(0,153,153));
        btNovaModalidade.setForeground(Color.WHITE);
        
    }//GEN-LAST:event_btNovaModalidadeMouseEntered

    private void btNovaModalidadeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btNovaModalidadeMouseExited
        btNovaModalidade.setBackground(new Color(240,240,240));
        btNovaModalidade.setForeground(Color.BLACK);
    }//GEN-LAST:event_btNovaModalidadeMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btNovaModalidade;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelModalidade;
    private javax.swing.JTable tabelaModalidade;
    // End of variables declaration//GEN-END:variables
}
