package Interfaces;

import DAO.RelatorioDAO;
import Dominio.ItemRelatorioPPS;
import Utils.ModelTable;
import Utils.VariableRowHeightRenderer;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import static javax.swing.text.StyleConstants.ALIGN_CENTER;

public class PPSRelatorio extends javax.swing.JPanel {
    SimpleDateFormat in= new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat out = new SimpleDateFormat("dd/MM/yyyy");
    private ArrayList<ItemRelatorioPPS> movimentacao_pps = new ArrayList();
    private ArrayList dados;
    Calendar calendarHoje = Calendar.getInstance();
    Calendar calendarInicio = Calendar.getInstance();
    
    String dataHoje;
    String dataInicio;
    public PPSRelatorio() {
        initComponents();
        incializaTabelaRelatorio();
        defineData();
        btLimparPeriodo.setVisible(false);
    }
    protected void incializaTabelaRelatorio(){
        tabelaRelatorio.removeAll();
        defineData();
        try{
            movimentacao_pps = RelatorioDAO.selectRelatorioPPS(calendarInicio.getTime(),calendarHoje.getTime());
            constroiTabela(movimentacao_pps);
         } catch (SQLException ex) {
            Logger.getLogger(MedicamentoHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void atualizaTabelaRelatorio(){
        tabelaRelatorio.removeAll();
        capturaData();
        try{
            movimentacao_pps = RelatorioDAO.selectRelatorioPPS(calendarInicio.getTime(),calendarHoje.getTime());
            constroiTabela(movimentacao_pps);
         } catch (SQLException ex) {
            Logger.getLogger(MedicamentoHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void constroiTabela(ArrayList<ItemRelatorioPPS> movimentacao_pps){
        dados = new ArrayList();
        String[] colunas = {"Tipo","Lote","Descricao","Unidade","Modalidade","Quantidade","Data Operacao","Validade"};
        
            for(ItemRelatorioPPS item: movimentacao_pps){
                try {
                    
                dados.add(new Object[]{
                   item.getTipo(),
                   item.getEstoquepps().getnLote(),
                   item.getEstoquepps().getPps().getDescricao(),
                   item.getEstoquepps().getPps().getUnidade(),
                   item.getEstoquepps().getPps().getModalidade(),
                   item.getEntrada()!=null?item.getEntrada().getQuantidade():item.getSaida().getQuantidade(),
                   formatarData(item.getEntrada()!=null?item.getEntrada().getData():item.getSaida().getData()),
                   formatarData(item.getEstoquepps().getDataValidade())
                });
                } catch (ParseException ex) {
                    Logger.getLogger(PPSRelatorio.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            ModelTable model = new ModelTable(dados, colunas);
            tabelaRelatorio.setModel(model);
            tabelaRelatorio.getColumnModel().getColumn(0).setPreferredWidth(250);
            tabelaRelatorio.getColumnModel().getColumn(1).setPreferredWidth(200);
            tabelaRelatorio.getColumnModel().getColumn(2).setPreferredWidth(850);
            tabelaRelatorio.getColumnModel().getColumn(3).setPreferredWidth(300);
            tabelaRelatorio.getColumnModel().getColumn(4).setPreferredWidth(300);
            tabelaRelatorio.getColumnModel().getColumn(5).setPreferredWidth(100);
            tabelaRelatorio.getColumnModel().getColumn(6).setPreferredWidth(400);
            tabelaRelatorio.getColumnModel().getColumn(7).setPreferredWidth(400);
            tabelaRelatorio.getTableHeader().setResizingAllowed(false);
            tabelaRelatorio.getTableHeader().setReorderingAllowed(false);
            tabelaRelatorio.getColumnModel().getColumn(2).setCellRenderer( new VariableRowHeightRenderer(tabelaRelatorio.getColumnModel().getColumn(2).getMinWidth()));
            tabelaRelatorio.repaint();
    }
    
    private String formatarData(Date data) throws ParseException{
        String dataTexto = new SimpleDateFormat("dd/MM/yyyy").format(data);
        return dataTexto;
    }
    private void capturaData(){
        dataHoje = jFormattedDataFim.getText().toString();
        dataInicio = jFormattedDataInicio.getText().toString();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            if(dataInicio.length()>0 && !dataInicio.isEmpty() && !dataInicio.equals("  /  /    ")){
                  this.calendarInicio.setTime(sdf.parse(dataInicio));
            }
            this.calendarHoje.setTime(sdf.parse(dataHoje));
            calendarHoje.set(Calendar.DAY_OF_MONTH,calendarHoje.get(Calendar.DAY_OF_MONTH)+1);
            
        } catch (ParseException ex) {
            System.out.println("Problema ao formatar data//"+calendarHoje+"//"+calendarInicio);
        }
        
    }
    private void defineData(){
        calendarHoje = Calendar.getInstance();
        if(calendarHoje.get(GregorianCalendar.DAY_OF_MONTH)<10)
            dataHoje = "0"+calendarHoje.get(GregorianCalendar.DAY_OF_MONTH);
        else
            dataHoje = ""+calendarHoje.get(GregorianCalendar.DAY_OF_MONTH);
        
        if(calendarHoje.get(GregorianCalendar.MONTH)>=9)
            dataHoje +=(calendarHoje.get(GregorianCalendar.MONTH)+1)+""+calendarHoje.get(GregorianCalendar.YEAR);
        else
            dataHoje +="0"+(calendarHoje.get(GregorianCalendar.MONTH)+1)+""+calendarHoje.get(GregorianCalendar.YEAR);
        jFormattedDataFim.setText(dataHoje);
        calendarInicio.set(2000,0,1);
        calendarHoje.set(Calendar.DAY_OF_MONTH,calendarHoje.get(Calendar.DAY_OF_MONTH)+1);
        jFormattedDataInicio.setText("");
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btPDFpps = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaRelatorio = new javax.swing.JTable();
        jFormattedDataFim = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        btLimparPeriodo = new javax.swing.JButton();
        btBuscar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jFormattedDataInicio = new javax.swing.JFormattedTextField();

        setOpaque(false);

        btPDFpps.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btPDFpps.setText("Exportar Para PDF");
        btPDFpps.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btPDFpps.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btPDFppsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btPDFppsMouseExited(evt);
            }
        });
        btPDFpps.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btPDFppsActionPerformed(evt);
            }
        });

        tabelaRelatorio.setBackground(new java.awt.Color(0, 153, 153));
        tabelaRelatorio.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tabelaRelatorio.setForeground(new java.awt.Color(255, 255, 255));
        tabelaRelatorio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelaRelatorio);

        try {
            jFormattedDataFim.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataFim.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataFim.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel7.setText("Até");

        btLimparPeriodo.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btLimparPeriodo.setText("Limpar Seleção");
        btLimparPeriodo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btLimparPeriodo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btLimparPeriodoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btLimparPeriodoMouseExited(evt);
            }
        });
        btLimparPeriodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimparPeriodoActionPerformed(evt);
            }
        });

        btBuscar.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btBuscar.setText("Buscar");
        btBuscar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btBuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btBuscarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btBuscarMouseExited(evt);
            }
        });
        btBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel6.setText("De");

        try {
            jFormattedDataInicio.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataInicio.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataInicio.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(jFormattedDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jFormattedDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btLimparPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(72, 72, 72)
                .addComponent(btPDFpps, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 730, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btPDFpps, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jFormattedDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btLimparPeriodo)
                        .addComponent(btBuscar))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jFormattedDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 458, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btPDFppsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btPDFppsActionPerformed
        
        Calendar cal = Calendar.getInstance();
        String dataHoje;
        if(cal.get(GregorianCalendar.DAY_OF_MONTH)<10)
            dataHoje = "0"+cal.get(GregorianCalendar.DAY_OF_MONTH);
        else
            dataHoje = ""+cal.get(GregorianCalendar.DAY_OF_MONTH);
        
        if(cal.get(GregorianCalendar.MONTH)>=9)
            dataHoje +=(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        else
            dataHoje +="0"+(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        
        
        
        String caminho = pastaDestino();
        if(caminho != null){try {
                Document documento = new Document();

                PdfWriter.getInstance(documento, new FileOutputStream(caminho+"\\RelatorioPPS-"+dataHoje+".pdf"));
                documento.open();
                documento.setPageSize(PageSize.A4);

                Paragraph paragrafo1;
                paragrafo1 = new Paragraph("");
                documento.add(paragrafo1);

                float[] columnWidths = {3,3,4,4,3,3,3,3};
                PdfPTable tabela1 = new PdfPTable(columnWidths);
                tabela1.setWidthPercentage(100);
                tabela1.getDefaultCell().setUseAscender(true);
                tabela1.getDefaultCell().setUseDescender(true);

                Font f = new Font(Font.FontFamily.COURIER, 6, Font.NORMAL, GrayColor.WHITE);
                PdfPCell cabecalho = new PdfPCell(new Paragraph("RELATÓRIO MOVIMENTAÇÃO PRODUTO PARA SAÚDE",new Font(Font.FontFamily.COURIER, 11, Font.BOLD)));
                cabecalho.setHorizontalAlignment(ALIGN_CENTER);
                cabecalho.setPaddingBottom(10);
                cabecalho.setBorder(PdfPCell.NO_BORDER);
                cabecalho.setColspan(9);
                tabela1.addCell(cabecalho);

                PdfPCell cel1 = new PdfPCell(new Phrase("TIPO",f));
                cel1.setHorizontalAlignment(ALIGN_CENTER);
                cel1.setBackgroundColor(GrayColor.DARK_GRAY);
                cel1.setPaddingTop(5);
                cel1.setPaddingBottom(5);

                PdfPCell cel2 = new PdfPCell(new Phrase("LOTE",f));
                cel2.setHorizontalAlignment(ALIGN_CENTER);
                cel2.setBackgroundColor(GrayColor.DARK_GRAY);
                cel2.setPaddingTop(5);
                cel2.setPaddingBottom(5);

                PdfPCell cel3 = new PdfPCell(new Phrase("DESCRIÇÃO",f));
                cel3.setHorizontalAlignment(ALIGN_CENTER);
                cel3.setBackgroundColor(GrayColor.DARK_GRAY);
                cel3.setPaddingTop(5);
                cel3.setPaddingBottom(5);

                PdfPCell cel4 = new PdfPCell(new Phrase("UNIDADE",f));
                cel4.setHorizontalAlignment(ALIGN_CENTER);
                cel4.setBackgroundColor(GrayColor.DARK_GRAY);
                cel4.setPaddingTop(5);
                cel4.setPaddingBottom(5);

                PdfPCell cel5 = new PdfPCell(new Phrase("MODALIDADE",f));
                cel5.setHorizontalAlignment(ALIGN_CENTER);
                cel5.setBackgroundColor(GrayColor.DARK_GRAY);
                cel5.setPaddingTop(5);
                cel5.setPaddingBottom(5);

                PdfPCell cel6 = new PdfPCell(new Phrase("QUANTIDADE",f));
                cel6.setHorizontalAlignment(ALIGN_CENTER);
                cel6.setBackgroundColor(GrayColor.DARK_GRAY);
                cel6.setPaddingTop(5);
                cel6.setPaddingBottom(5);

                PdfPCell cel7 = new PdfPCell(new Phrase("DATA OPERAÇÃO",f));
                cel7.setHorizontalAlignment(ALIGN_CENTER);
                cel7.setBackgroundColor(GrayColor.DARK_GRAY);
                cel7.setPaddingTop(5);
                cel7.setPaddingBottom(5);

                PdfPCell cel8 = new PdfPCell(new Phrase("VALIDADE",f));
                cel8.setHorizontalAlignment(ALIGN_CENTER);
                cel8.setBackgroundColor(GrayColor.DARK_GRAY);
                cel8.setPaddingTop(5);
                cel8.setPaddingBottom(5);

                tabela1.addCell(cel1);
                tabela1.addCell(cel2);
                tabela1.addCell(cel3);
                tabela1.addCell(cel4);
                tabela1.addCell(cel5);
                tabela1.addCell(cel6);
                tabela1.addCell(cel7);
                tabela1.addCell(cel8);

                List<ItemRelatorioPPS> pps; 
                pps = new ArrayList<>();
                pps = movimentacao_pps;

                tabela1.getDefaultCell().setHorizontalAlignment(ALIGN_CENTER);
                tabela1.getDefaultCell().setPadding(2);
                Font g = new Font(Font.FontFamily.COURIER,8,Font.NORMAL, GrayColor.BLACK);
                try {
                    for(ItemRelatorioPPS PPS:pps){
                        tabela1.addCell(new Phrase(PPS.getTipo(), g));
                        tabela1.addCell(new Phrase(PPS.getEstoquepps().getnLote(), g));
                        tabela1.addCell(new Phrase(PPS.getEstoquepps().getPps().getDescricao(), g));
                        tabela1.addCell(new Phrase(PPS.getEstoquepps().getPps().getUnidade().toString(), g));
                        tabela1.addCell(new Phrase(PPS.getEstoquepps().getPps().getModalidade().toString(), g));
                        tabela1.addCell(new Phrase(Integer.toString( PPS.getEntrada()!=null?PPS.getEntrada().getQuantidade():PPS.getSaida().getQuantidade()), g));
                        tabela1.addCell(new Phrase(formatarData(PPS.getEntrada()!=null?PPS.getEntrada().getData():PPS.getSaida().getData()), g));
                        tabela1.addCell(new Phrase(formatarData(PPS.getEstoquepps().getDataValidade()), g));
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(MedicamentoRelatorio.class.getName()).log(Level.SEVERE, null, ex);
                }

                documento.add(tabela1);

                documento.close();

            } catch (FileNotFoundException ex) {
                Logger.getLogger(PPSRelatorio.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DocumentException ex) {
                Logger.getLogger(PPSRelatorio.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                Desktop.getDesktop().open(new File(caminho+"\\RelatorioPPS-"+dataHoje+".pdf"));
            } catch (IOException ex) {
                Logger.getLogger(MedicamentoRelatorio.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btPDFppsActionPerformed

    public String pastaDestino(){
        
            JFileChooser file = new JFileChooser(); 
            file.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int res = file.showOpenDialog(getParent());
            File caminho;
            if(res == JFileChooser.APPROVE_OPTION){
                caminho = file.getSelectedFile();
                
                System.out.println(caminho.getAbsoluteFile());
                return caminho.getAbsolutePath();
            }else{
               
               return null;
            }
        
        
    }
    private void btPDFppsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btPDFppsMouseEntered
        btPDFpps.setBackground(new Color(0,153,153));
        btPDFpps.setForeground(Color.WHITE);
    }//GEN-LAST:event_btPDFppsMouseEntered

    private void btPDFppsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btPDFppsMouseExited
        btPDFpps.setBackground(new Color(240,240,240));
        btPDFpps.setForeground(Color.BLACK);
    }//GEN-LAST:event_btPDFppsMouseExited

    private void btLimparPeriodoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btLimparPeriodoMouseEntered
        btLimparPeriodo.setBackground(new Color(0,153,153));
        btLimparPeriodo.setForeground(Color.WHITE);
    }//GEN-LAST:event_btLimparPeriodoMouseEntered

    private void btLimparPeriodoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btLimparPeriodoMouseExited
        btLimparPeriodo.setBackground(new Color(240,240,240));
        btLimparPeriodo.setForeground(Color.BLACK);
    }//GEN-LAST:event_btLimparPeriodoMouseExited

    private void btLimparPeriodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimparPeriodoActionPerformed
        defineData();
        incializaTabelaRelatorio();
        btLimparPeriodo.setVisible(false);
    }//GEN-LAST:event_btLimparPeriodoActionPerformed

    private void btBuscarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBuscarMouseEntered
        btBuscar.setBackground(new Color(0,153,153));
        btBuscar.setForeground(Color.WHITE);
    }//GEN-LAST:event_btBuscarMouseEntered

    private void btBuscarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBuscarMouseExited
        btBuscar.setBackground(new Color(240,240,240));
        btBuscar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btBuscarMouseExited

    private void btBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarActionPerformed
        atualizaTabelaRelatorio();
        btLimparPeriodo.setVisible(true);
    }//GEN-LAST:event_btBuscarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBuscar;
    private javax.swing.JButton btLimparPeriodo;
    private javax.swing.JButton btPDFpps;
    private javax.swing.JFormattedTextField jFormattedDataFim;
    private javax.swing.JFormattedTextField jFormattedDataInicio;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaRelatorio;
    // End of variables declaration//GEN-END:variables
}
