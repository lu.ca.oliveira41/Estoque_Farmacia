package Interfaces;

import DAO.UnidadeDAO;
import Dominio.Unidade;
import Utils.ModelTable;
import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ConfigGerenciarUnidade extends javax.swing.JPanel {
    private List<Unidade> unidades = new ArrayList<>();
    public ConfigGerenciarUnidade() {
        initComponents();
        iniciarTabelaUnidade();
    }
    public void iniciarTabelaUnidade(){
        tableUnidade.removeAll();
        try {
           unidades = UnidadeDAO.selectUnidade();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode()+":"+ex.getMessage());
        }
        String[] colunas = {"CÓDIGO","TIPO DE UNIDADE"};
        ArrayList dados = new ArrayList();
        for(Unidade unidade:unidades){
            dados.add(new Object[]{
                unidade.getId(),
                unidade.getUnidade()
            });
        }
        ModelTable model = new ModelTable(dados, colunas);
        tableUnidade.setModel(model);
        tableUnidade.getColumnModel().getColumn(0).setMaxWidth(100);
        tableUnidade.getTableHeader().setResizingAllowed(false);
        tableUnidade.getTableHeader().setReorderingAllowed(false);
        tableUnidade.getSelectionModel().addListSelectionListener(selecionaLinha());
        
    }
    private ListSelectionListener selecionaLinha() {
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tableUnidade.getSelectedRow() != -1){
                    int cod = (int) tableUnidade.getValueAt(tableUnidade.getSelectedRow(), 0);
                    try {
                        Unidade unidade = UnidadeDAO.selectUnidadeByID(cod);
                        Object[] options = {"Editar","Excluir"};
                    int resposta = JOptionPane.showOptionDialog(getParent(),
                        "Unidade: "+unidade.getUnidade(),
                        "O que deseja fazer?",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.QUESTION_MESSAGE,null,options,null);
                    if(resposta == 0)
                       editarInfo(unidade); 
                    else if(resposta == 1)
                        confirmaExclusao(cod);
                    tableUnidade.clearSelection();
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                    
                }
            }
        };
    }
    private void confirmaExclusao(int cod){
       Object[] options = {"Confirmar","Cancelar"};
       int resposta = JOptionPane.showOptionDialog(getParent(), "Deseja realmente excluir?",
               null,
               JOptionPane.DEFAULT_OPTION,
               JOptionPane.QUESTION_MESSAGE,null,options,null);
       if(resposta == 0)
        try {
            UnidadeDAO.delete(cod);
            revalida();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode() +":"+ex.getMessage());
        }
           
    }
    private void editarInfo(Unidade unidade){
        String novoNome = null;
        boolean result = false;
        while(novoNome == null || novoNome.equals("") ){
            novoNome =  JOptionPane.showInputDialog(getParent(), "Novo nome para a unidade:",
                            "Editar Informação da Unidade: "+unidade.getUnidade(),
                            JOptionPane.PLAIN_MESSAGE);
            if(novoNome != null)
                if(novoNome.equals("") ){
                    JOptionPane.showMessageDialog(getParent(), "Insira um nome valido",
                            "Erro ao atualizar dados",JOptionPane.ERROR_MESSAGE);
                }else{
                    try{
                        unidade.setUnidade(novoNome);
                        result = UnidadeDAO.updateUnidadeByid(unidade);
                        revalida();
                        JOptionPane.showMessageDialog(getParent(), "Atualização realizada com sucesso",
                            "",JOptionPane.INFORMATION_MESSAGE);
                        break;
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                }
            else
                break;
        }
        
        
    }
    public void revalida(){
        iniciarTabelaUnidade();
        tableUnidade.repaint();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelUnidade = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableUnidade = new javax.swing.JTable();
        btNovaUnidade = new javax.swing.JButton();

        setOpaque(false);

        labelUnidade.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        labelUnidade.setForeground(new java.awt.Color(255, 255, 255));
        labelUnidade.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelUnidade.setText("Unidades Registradas");

        tableUnidade.setBackground(new java.awt.Color(0, 153, 153));
        tableUnidade.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tableUnidade.setForeground(new java.awt.Color(255, 255, 255));
        tableUnidade.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tableUnidade);

        btNovaUnidade.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btNovaUnidade.setText("Nova Unidade");
        btNovaUnidade.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btNovaUnidade.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btNovaUnidadeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btNovaUnidadeMouseExited(evt);
            }
        });
        btNovaUnidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNovaUnidadeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(labelUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btNovaUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btNovaUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btNovaUnidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNovaUnidadeActionPerformed
        String novoNome = null;
        boolean result = false;
        while( novoNome == null || novoNome.equals("") ){
            novoNome =  JOptionPane.showInputDialog(getParent(), "Nome da nova unidade:",
                           "Cadastrar nova unidade",
                            JOptionPane.PLAIN_MESSAGE);
            if(novoNome!=null)
                if(novoNome == null || novoNome.equals("") ){
                    JOptionPane.showMessageDialog(getParent(), "Insira um nome valido",
                            "Erro ao cadastrar novos dados",JOptionPane.ERROR_MESSAGE);
                }else{
                    try{
                        Unidade unidade = new Unidade(novoNome);
                        result = UnidadeDAO.insertUnidade(unidade);
                        revalida();
                        JOptionPane.showMessageDialog(getParent(), "Cadastro realizado com sucesso",
                            "",JOptionPane.INFORMATION_MESSAGE);
                        break;
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                }
            else
                break;
        }        
    }//GEN-LAST:event_btNovaUnidadeActionPerformed

    private void btNovaUnidadeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btNovaUnidadeMouseEntered
        btNovaUnidade.setBackground(new Color(0,153,153));
        btNovaUnidade.setForeground(Color.WHITE);
    }//GEN-LAST:event_btNovaUnidadeMouseEntered

    private void btNovaUnidadeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btNovaUnidadeMouseExited
        btNovaUnidade.setBackground(new Color(240,240,240));
        btNovaUnidade.setForeground(Color.BLACK);
    }//GEN-LAST:event_btNovaUnidadeMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btNovaUnidade;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelUnidade;
    private javax.swing.JTable tableUnidade;
    // End of variables declaration//GEN-END:variables

    
}
