package Interfaces;

import DAO.ConcentracaoDAO;
import DAO.UnidadeDAO;
import Dominio.Concentracao;
import Dominio.Unidade;
import Utils.ModelTable;
import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ConfigGerenciarConcentracao extends javax.swing.JPanel {
    List<Concentracao> concentracoes = new ArrayList<>();
    public ConfigGerenciarConcentracao() {
        initComponents();
        iniciarTabelaConcentracao();
    }
    public void iniciarTabelaConcentracao(){
        tableConcentracao.removeAll();
        try {
           concentracoes = ConcentracaoDAO.selectConcentracao();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode()+":"+ex.getMessage());
        }
        String[] colunas = {"CÓDIGO","TIPO DE CONCENTRAÇÃO"};
        ArrayList dados = new ArrayList();
        for(Concentracao concentracao:concentracoes){
            dados.add(new Object[]{
                concentracao.getId(),
                concentracao.getConcentracao()
            });
        }
        ModelTable model = new ModelTable(dados, colunas);
        tableConcentracao.setModel(model);
        tableConcentracao.getColumnModel().getColumn(0).setMaxWidth(100);
        tableConcentracao.getTableHeader().setResizingAllowed(false);
        tableConcentracao.getTableHeader().setReorderingAllowed(false);
        tableConcentracao.getSelectionModel().addListSelectionListener(selecionaLinha());
    }
    private ListSelectionListener selecionaLinha() {
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tableConcentracao.getSelectedRow() != -1){
                    int cod = (int) tableConcentracao.getValueAt(tableConcentracao.getSelectedRow(), 0);
                    try {
                        Concentracao concentracao = ConcentracaoDAO.selectConcentracaoByID(cod);
                        Object[] options = {"Editar","Excluir"};
                        int resposta = JOptionPane.showOptionDialog(getParent(),
                            "O que deseja fazer?",
                            "Concentracao: "+concentracao.getConcentracao(),
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
                        if(resposta == 0)
                           editarInfo(concentracao); 
                        else if(resposta == 1)
                            confirmaExclusao(cod);
                        tableConcentracao.clearSelection();
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                    
                }
            }
        };
    }
    private void confirmaExclusao(int cod){
       Object[] options = {"Confirmar","Cancelar"};
       int resposta = JOptionPane.showOptionDialog(getParent(), "Deseja realmente excluir?",
               null,
               JOptionPane.DEFAULT_OPTION,
               JOptionPane.QUESTION_MESSAGE,null,options,null);
       
       if(resposta == 0)
        try {
            ConcentracaoDAO.delete(cod);
            revalidarTabela();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode() +":"+ex.getMessage());
        }
       
           
    }
    private void editarInfo(Concentracao concentracao){
        String novoNome = null;
        boolean result = false;
        while(novoNome == null || novoNome.equals("") ){
            novoNome =  JOptionPane.showInputDialog(getParent(), "Novo nome para a concentração:",
                            "Editar Informação da Concentração: "+concentracao.getConcentracao(),
                            JOptionPane.PLAIN_MESSAGE);
            if(novoNome!=null)
                if(novoNome.equals("") ){
                    JOptionPane.showMessageDialog(getParent(), "Insira um nome valido",
                            "Erro ao atualizar dados",JOptionPane.ERROR_MESSAGE);
                }else{
                    try{
                        concentracao.setConcentracao(novoNome);
                        result =ConcentracaoDAO.updateConcentracaoByid(concentracao);
                        revalidarTabela();
                        JOptionPane.showMessageDialog(getParent(), "Informações Atualizadas",
                            null,JOptionPane.INFORMATION_MESSAGE);
                        break;
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                }
            else
                break;
            
                
        }
        
        
    }
    public void revalidarTabela(){
        iniciarTabelaConcentracao();
        tableConcentracao.repaint();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelConcentracao = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableConcentracao = new javax.swing.JTable();
        btNovaConcentracao = new javax.swing.JButton();

        setOpaque(false);

        labelConcentracao.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        labelConcentracao.setForeground(new java.awt.Color(255, 255, 255));
        labelConcentracao.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelConcentracao.setText("Concentrações Registradas");

        tableConcentracao.setBackground(new java.awt.Color(0, 153, 153));
        tableConcentracao.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tableConcentracao.setForeground(new java.awt.Color(255, 255, 255));
        tableConcentracao.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tableConcentracao.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jScrollPane1.setViewportView(tableConcentracao);

        btNovaConcentracao.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btNovaConcentracao.setText("Nova Concentração");
        btNovaConcentracao.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btNovaConcentracao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btNovaConcentracaoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btNovaConcentracaoMouseExited(evt);
            }
        });
        btNovaConcentracao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNovaConcentracaoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(labelConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btNovaConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btNovaConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btNovaConcentracaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNovaConcentracaoActionPerformed
        String novoNome = null;
        boolean result = false;
        while(novoNome == null || novoNome.equals("") ){
            novoNome =  JOptionPane.showInputDialog(getParent(), "Nome da nova concentracao:",
                "Cadastrar nova concentracao",
                JOptionPane.PLAIN_MESSAGE);
            if(novoNome != null)
                if(novoNome == null || novoNome.equals("") ){
                    JOptionPane.showMessageDialog(getParent(), "Insira um nome valido",
                        "Erro ao cadastrar novos dados",JOptionPane.ERROR_MESSAGE);
                }else{
                    try{
                        Concentracao concentracao = new Concentracao(novoNome);
                        result = ConcentracaoDAO.insertConcentracao(concentracao);
                        revalidarTabela();
                        JOptionPane.showMessageDialog(getParent(), "Cadastro realizado com sucesso",
                                "",JOptionPane.INFORMATION_MESSAGE);
                        break;
                        
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                }
            else
                break;
       
                
        }
       
    }//GEN-LAST:event_btNovaConcentracaoActionPerformed

    private void btNovaConcentracaoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btNovaConcentracaoMouseEntered
        btNovaConcentracao.setBackground(new Color(0,153,153));
        btNovaConcentracao.setForeground(Color.WHITE);
    }//GEN-LAST:event_btNovaConcentracaoMouseEntered

    private void btNovaConcentracaoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btNovaConcentracaoMouseExited
        btNovaConcentracao.setBackground(new Color(240,240,240));
        btNovaConcentracao.setForeground(Color.BLACK);
    }//GEN-LAST:event_btNovaConcentracaoMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btNovaConcentracao;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelConcentracao;
    private javax.swing.JTable tableConcentracao;
    // End of variables declaration//GEN-END:variables
}
