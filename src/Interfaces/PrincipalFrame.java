package Interfaces;

import Conexao.ConexaoBanco;
import DAO.*;
import Principal.EstoqueFarmacia;
import java.awt.CardLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

public class PrincipalFrame extends javax.swing.JFrame {
    private static PrincipalPanel principalPanel = new PrincipalPanel();
    private static NotificacoesPanel notificacoesPanel = new NotificacoesPanel();
    private static ConfiguracoesPanel configPanel = new ConfiguracoesPanel();

    
    public PrincipalFrame() {
        initComponents();
        
        iniciaPanel();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt){
                Object[] options = {"Confirmar","Cancelar"};
                int resposta = JOptionPane.showOptionDialog(getParent(),
                            "Deseja realmente sair?",
                            "AVISO",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
                if(resposta==0){
                    System.exit(0);
                    try {
                        ConexaoBanco.fechaConexao();
                    } catch (SQLException ex) {
                        Logger.getLogger(PrincipalFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }        
        });
    }
    public static void reiniciarConexoes(){
        ApresentacaoDAO.atualizaConexao();
        ConcentracaoDAO.atualizaConexao();
        EstoqueDAO.atualizaConexao();
        EntradaDAO.atualizaConexao();
        EstoqueMedicamentoDAO.atualizaConexao();
        EstoquePPSDAO.atualizaConexao();
        MedicamentoDAO.atualizaConexao();
        ModalidadeDAO.atualizaConexao();
        ProdutoParaSaudeDAO.atualizaConexao();
        RelatorioDAO.atualizaConexao();
        SaidaDAO.atualizaConexao();
        UnidadeDAO.atualizaConexao();
        UsuarioDAO.atualizaConexao();
    }
    public static void reiniciaSistema(){
        ApresentacaoDAO.atualizaConexao();
        ConcentracaoDAO.atualizaConexao();
        EstoqueDAO.atualizaConexao();
        EntradaDAO.atualizaConexao();
        EstoqueMedicamentoDAO.atualizaConexao();
        EstoquePPSDAO.atualizaConexao();
        MedicamentoDAO.atualizaConexao();
        ModalidadeDAO.atualizaConexao();
        ProdutoParaSaudeDAO.atualizaConexao();
        RelatorioDAO.atualizaConexao();
        SaidaDAO.atualizaConexao();
        UnidadeDAO.atualizaConexao();
        UsuarioDAO.atualizaConexao();
        principalPanel.panelMedicamentoCadastro.atualizaJBox();
        principalPanel.panelMedicamentoEntradaTable.inicializaTabelaMedicamento();
        principalPanel.panelMedicamentoHome.inicializaTabelaMedicamento();
        principalPanel.panelMedicamentoRelatorio.incializaTabelaRelatorio();
        
        principalPanel.panelEntradaTable.inicializaTabelaPPS();
        principalPanel.panelPPSCadastro.atualizaJBox();
        principalPanel.panelPPSHome.inicializaTabelaPPS();
        principalPanel.panelPPSRelatorio.incializaTabelaRelatorio();
        
        notificacoesPanel.iniciaTabelaNotificacoes(90);
        configPanel.panelConfigGerenciarApresentacao.iniciarTabelaApresentacao();
        configPanel.panelConfigGerenciarConcentracao.iniciarTabelaConcentracao();
        configPanel.panelConfigGerenciarModalidade.iniciarTabelaModalidade();
        configPanel.panelConfigGerenciarUnidade.iniciarTabelaUnidade();
        
        principalPanel.atualizaNumNoficacoes();
        configPanel.panelConfigInfoAcesso.atualizaDados();
    }
    
    private static void iniciaPanel(){
        cardpanelPrincipal.add(principalPanel,"panel home");
        cardpanelPrincipal.add(notificacoesPanel,"panel notificacao");
        cardpanelPrincipal.add(configPanel,"panel config");
        
        CardLayout card = (CardLayout) cardpanelPrincipal.getLayout();
        card.show(cardpanelPrincipal, "panel home");
    }
    public static void showPanelPrincipal(){
        CardLayout card = (CardLayout) cardpanelPrincipal.getLayout();
        card.show(cardpanelPrincipal, "panel home");
    }
    public static void showPanelNotificacoes(){
        CardLayout card = (CardLayout) cardpanelPrincipal.getLayout();
        card.show(cardpanelPrincipal, "panel notificacao");
    }
    public static void showPanelConfiguracao(){
        CardLayout card = (CardLayout) cardpanelPrincipal.getLayout();
        card.show(cardpanelPrincipal, "panel config");
    }
    public static void atualizaNotificacoes(){
        notificacoesPanel.iniciaTabelaNotificacoes(90);
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cardpanelPrincipal = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        cardpanelPrincipal.setPreferredSize(new java.awt.Dimension(1021, 665));
        cardpanelPrincipal.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cardpanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 1067, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cardpanelPrincipal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PrincipalFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PrincipalFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PrincipalFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PrincipalFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PrincipalFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.JPanel cardpanelPrincipal;
    // End of variables declaration//GEN-END:variables
}
