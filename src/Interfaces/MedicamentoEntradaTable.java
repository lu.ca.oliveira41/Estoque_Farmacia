
package Interfaces;

import DAO.EntradaDAO;
import DAO.EstoqueMedicamentoDAO;
import DAO.MedicamentoDAO;
import DAO.SaidaDAO;
import Dominio.Entrada;
import Dominio.EstoqueMedicamento;
import Dominio.Medicamento;
import Dominio.Saida;
import Utils.ModelTable;
import Utils.VariableRowHeightRenderer;
import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MedicamentoEntradaTable extends javax.swing.JPanel {
    private String[] colunas = {"ID","DENOMINAÇÃO \nCOMUM BRASILEIRA","APRESENTAÇÃO","CONCENTRAÇÃO","MODALIDADE"};
    private ArrayList dados;
    private List<Medicamento> medicamentos = new ArrayList<>();
    
    public MedicamentoEntradaTable() {
        initComponents();
        inicializaTabelaMedicamento();
    }
    protected void inicializaTabelaMedicamento(){
        tableMedicamento.removeAll();
        medicamentos.clear();
        try {
            medicamentos = MedicamentoDAO.selectMedicamento();
            constroiTabela(medicamentos);
        } catch (SQLException ex) {
            Logger.getLogger(MedicamentoHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void atualizaTabelaMedicamento(String query){
        try {
            medicamentos = MedicamentoDAO.buscaEstoqueMedicamentoByDCB(query);
            constroiTabela(medicamentos);
        }catch (SQLException ex) {
            Logger.getLogger(MedicamentoHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void constroiTabela(List<Medicamento> estoque_medicamentos){
        dados  = new ArrayList();
            for(Medicamento medicamento:estoque_medicamentos){
                dados.add(new Object[]{
                   medicamento.getId(),
                    medicamento.getDenominacao(),
                   medicamento.getApresentacao(),
                   medicamento.getConcentracao(),
                   medicamento.getModalidade()
                });
            }
            ModelTable model = new ModelTable(dados, colunas);
            tableMedicamento.setModel(model);
            tableMedicamento.getColumnModel().getColumn(0).setPreferredWidth(100);
            tableMedicamento.getColumnModel().getColumn(1).setPreferredWidth(800);
            tableMedicamento.getColumnModel().getColumn(2).setPreferredWidth(400);
            tableMedicamento.getColumnModel().getColumn(3).setPreferredWidth(400);
            tableMedicamento.getColumnModel().getColumn(4).setPreferredWidth(400);
            tableMedicamento.setAutoResizeMode(tableMedicamento.AUTO_RESIZE_ALL_COLUMNS);
            tableMedicamento.getTableHeader().setResizingAllowed(false);
            tableMedicamento.getTableHeader().setReorderingAllowed(false);
            tableMedicamento.getSelectionModel().addListSelectionListener(selecionarLinha());
            tableMedicamento.getColumnModel().getColumn(1).setCellRenderer( new VariableRowHeightRenderer(tableMedicamento.getColumnModel().getColumn(1).getMinWidth()));
            tableMedicamento.repaint();
        
    }
    private ListSelectionListener selecionarLinha(){
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tableMedicamento.getSelectedRow() != -1){
                    int cod = (int) tableMedicamento.getValueAt(tableMedicamento.getSelectedRow(), 0);
                    try {
                        Medicamento medicamento = MedicamentoDAO.selectMedicamentoByID(cod);
                        Object[] options = {"Editar","Excluir","Realizar Entrada"};
                        int resposta = JOptionPane.showOptionDialog(getParent(),
                            "Medicamento: "+medicamento.getDenominacao(),
                            "O que deseja fazer?",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
                    if(resposta == 0)
                       PrincipalPanel.showMedEdit(medicamento); 
                    else if(resposta==1)
                        excluirMed(medicamento);
                    else if(resposta==2)
                       PrincipalPanel.showMedPrimeiraEntrada(medicamento);  
                    tableMedicamento.clearSelection();
                        
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                    
                }
            }
        };
    }
    private void excluirMed(Medicamento med){
        
        Object[] options = {"Confirmar","Cancelar"};
        int resposta = JOptionPane.showOptionDialog(getParent(),
                            "Medicamento: "+med.getDenominacao(),
                            "Você tem certeza que deseja excluir?",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
        if(resposta==0){
            try{
                MedicamentoDAO.deleteMedicamento(med);
                JOptionPane.showMessageDialog(getParent(),"Exlusão realizada com sucesso","AVISO",JOptionPane.WARNING_MESSAGE );
                PrincipalPanel.showMedTabelaEntrada();
            }catch (SQLException ex) {
                    JOptionPane.showMessageDialog(getParent(),"Não foi possivel excluir o registro","AVISO",JOptionPane.WARNING_MESSAGE );
            }
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfBusca = new javax.swing.JTextField();
        btBusca = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableMedicamento = new javax.swing.JTable();
        btLimpaBusca = new javax.swing.JButton();

        setOpaque(false);

        tfBusca.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        btBusca.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btBusca.setText("Pesquisar");
        btBusca.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btBusca.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btBuscaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btBuscaMouseExited(evt);
            }
        });
        btBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscaActionPerformed(evt);
            }
        });

        tableMedicamento.setBackground(new java.awt.Color(0, 153, 153));
        tableMedicamento.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tableMedicamento.setForeground(new java.awt.Color(255, 255, 255));
        tableMedicamento.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tableMedicamento);

        btLimpaBusca.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btLimpaBusca.setText("Limpar Busca");
        btLimpaBusca.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btLimpaBusca.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btLimpaBuscaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btLimpaBuscaMouseExited(evt);
            }
        });
        btLimpaBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimpaBuscaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 695, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tfBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btLimpaBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(tfBusca, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                    .addComponent(btBusca, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btLimpaBusca, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btBuscaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBuscaMouseEntered
        btBusca.setBackground(new Color(0,153,153));
        btBusca.setForeground(Color.WHITE);
    }//GEN-LAST:event_btBuscaMouseEntered

    private void btBuscaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBuscaMouseExited
        btBusca.setBackground(new Color(240,240,240));
        btBusca.setForeground(Color.BLACK);
    }//GEN-LAST:event_btBuscaMouseExited

    private void btBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscaActionPerformed
        String query = tfBusca.getText().toString().trim();
        if(query.isEmpty() || query.equals("")){
            JOptionPane.showMessageDialog(getParent(), "Preencha o campo corretamente","ALERTA",JOptionPane.ERROR_MESSAGE);
        }else{
            atualizaTabelaMedicamento(query);
            btLimpaBusca.setVisible(true);
        }
    }//GEN-LAST:event_btBuscaActionPerformed

    private void btLimpaBuscaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btLimpaBuscaMouseEntered
        btLimpaBusca.setBackground(new Color(0,153,153));
        btLimpaBusca.setForeground(Color.WHITE);
    }//GEN-LAST:event_btLimpaBuscaMouseEntered

    private void btLimpaBuscaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btLimpaBuscaMouseExited
        btLimpaBusca.setBackground(new Color(240,240,240));
        btLimpaBusca.setForeground(Color.BLACK);
    }//GEN-LAST:event_btLimpaBuscaMouseExited

    private void btLimpaBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimpaBuscaActionPerformed
        inicializaTabelaMedicamento();
        tfBusca.setText("");
        btLimpaBusca.setVisible(false);
    }//GEN-LAST:event_btLimpaBuscaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBusca;
    private javax.swing.JButton btLimpaBusca;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableMedicamento;
    private javax.swing.JTextField tfBusca;
    // End of variables declaration//GEN-END:variables

    
}
