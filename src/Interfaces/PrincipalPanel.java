package Interfaces;

import DAO.EstoqueDAO;
import Dominio.Entrada;
import Dominio.EstoqueMedicamento;
import Dominio.EstoquePPS;
import Dominio.Medicamento;
import Dominio.ProdutoParaSaude;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import Dominio.EstoqueMedicamento;
import Dominio.EstoquePPS;
import Dominio.Medicamento;
import Dominio.ProdutoParaSaude;
import Dominio.Saida;
import java.awt.BorderLayout;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class PrincipalPanel extends javax.swing.JPanel{
    public static PPSHome panelPPSHome = new PPSHome();
    public static PPSRelatorio panelPPSRelatorio = new PPSRelatorio();
    public static PPSCadastro panelPPSCadastro = new PPSCadastro();
    public static PPSEntradaTable panelEntradaTable = new PPSEntradaTable();
    
    public static MedicamentoHome panelMedicamentoHome = new MedicamentoHome();
    public static MedicamentoRelatorio panelMedicamentoRelatorio = new MedicamentoRelatorio();
    public static MedicamentoCadastro panelMedicamentoCadastro = new MedicamentoCadastro();
    public static MedicamentoEntradaTable panelMedicamentoEntradaTable = new MedicamentoEntradaTable();

    
    
    public PrincipalPanel() {
        initComponents();
        inicializaPanelMedicamento();
        inicializaPanelPPS();

        btNotificacoes.setText(EstoqueDAO.getEstoqueVencidoSize()+"");
        
        BackgroundPrincipal bg = new BackgroundPrincipal();
        this.add(bg, BorderLayout.CENTER);
        
        Transparencia t = new Transparencia();
        t.aplicarTransparencia(panelMedicamentoView);
        t.aplicarTransparencia(panelMenuLateralMedicamento);
        t.aplicarTransparencia(cardPanelMedicamentoPrincipal);
        t.aplicarTransparencia(panelPPSView);
        t.aplicarTransparencia(panelMenuLateralPPS);
        
    }
    private void inicializaPanelMedicamento(){

        cardPanelMedicamentoPrincipal.add(panelMedicamentoHome,"panel medHome");
        cardPanelMedicamentoPrincipal.add(panelMedicamentoRelatorio,"panel medRelatorio");
        cardPanelMedicamentoPrincipal.add(panelMedicamentoCadastro,"panel medCadastro");
        cardPanelMedicamentoPrincipal.add(panelMedicamentoEntradaTable,"panel medEntradaTable");
        
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medHome");
    }
    private void inicializaPanelPPS(){
        cardpanelPPSPrincipal.add(panelPPSHome,"panel ppsHome");
        cardpanelPPSPrincipal.add(panelPPSRelatorio,"panel ppsRelatorio");
        cardpanelPPSPrincipal.add(panelPPSCadastro,"panel ppsCadastro");
        cardpanelPPSPrincipal.add(panelEntradaTable,"panel ppsEntradaTable");
        
        CardLayout card = (CardLayout) cardpanelPPSPrincipal.getLayout();
        card.show(cardpanelPPSPrincipal, "panel ppsHome");
    }
    
    public static void showMedHome(){
        PrincipalFrame.atualizaNotificacoes();
        btNotificacoes.setText(EstoqueDAO.getEstoqueVencidoSize()+"");
        panelMedicamentoRelatorio.incializaTabelaRelatorio();
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medHome");
        panelMedicamentoHome.inicializaTabelaMedicamento();
        
    }
    public static void showMedEntrada(EstoqueMedicamento medicamento){
        MedicamentoEntrada panelMedicamentoEntrada = new MedicamentoEntrada(medicamento);
        cardPanelMedicamentoPrincipal.add(panelMedicamentoEntrada,"panel medEntrada");
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medEntrada");
    }
    public static void showMedPrimeiraEntrada(Medicamento medicamento){
        MedicamentoEntrada panelMedicamentoEntrada = new MedicamentoEntrada(medicamento);
        cardPanelMedicamentoPrincipal.add(panelMedicamentoEntrada,"panel medEntrada");
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medEntrada");
    }
    public static void showMedSaida(EstoqueMedicamento medicamento){
        MedicamentoSaida panelMedicamentoSaida = new MedicamentoSaida(medicamento);
        cardPanelMedicamentoPrincipal.add(panelMedicamentoSaida,"panel medSaida");
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medSaida");
    }
    public static void showMedTabelaEntrada(){
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medEntradaTable");
        panelMedicamentoEntradaTable.inicializaTabelaMedicamento();
        
    }
    static void showMedEdit(Medicamento medicamento) {
        MedicamentoEdicao panelmedicamentoedit = new MedicamentoEdicao(medicamento);
        cardPanelMedicamentoPrincipal.add(panelmedicamentoedit,"panel medEdit");
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medEdit");
    }
    public static void showMedEditOp(Entrada entrada){
        MedicamentoEditOp edit = new MedicamentoEditOp(entrada);
        cardPanelMedicamentoPrincipal.add(edit,"panel medEditOp");
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medEditOp");
        
    }
    public static void showMedEditOp(Saida saida){
        MedicamentoEditOp edit = new MedicamentoEditOp(saida);
        cardPanelMedicamentoPrincipal.add(edit,"panel medEditOp");
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medEditOp");
    }
    public static void showMedRelatorio(){
        panelMedicamentoRelatorio.incializaTabelaRelatorio();
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medRelatorio");
    }
    
    public static void showPPSHome(){
        PrincipalFrame.atualizaNotificacoes();
        btNotificacoes.setText(EstoqueDAO.getEstoqueVencidoSize()+"");
        panelPPSRelatorio.incializaTabelaRelatorio();
        CardLayout card = (CardLayout) cardpanelPPSPrincipal.getLayout();
        card.show(cardpanelPPSPrincipal, "panel ppsHome");
        panelPPSHome.inicializaTabelaPPS();
    }
    public static void showPPSPrimeiraEntrada(ProdutoParaSaude pps){
        PPSEntrada panelPPSEntrada = new PPSEntrada(pps);
        cardpanelPPSPrincipal.add(panelPPSEntrada,"panel ppsPrimeiraEntrada");
        CardLayout card = (CardLayout) cardpanelPPSPrincipal.getLayout();
        card.show(cardpanelPPSPrincipal, "panel ppsPrimeiraEntrada");
        
    }
    public static void showPPSEntrada(EstoquePPS pps){
        PPSEntrada panelPPSEntrada = new PPSEntrada(pps);
        cardpanelPPSPrincipal.add(panelPPSEntrada,"panel ppsEntrada");
        CardLayout card = (CardLayout) cardpanelPPSPrincipal.getLayout();
        card.show(cardpanelPPSPrincipal, "panel ppsEntrada");
    }
    public static void showPPSSaida(EstoquePPS pps){
        PPSSaida panelPPSSaida = new PPSSaida(pps);
        cardpanelPPSPrincipal.add(panelPPSSaida,"panel ppsSaida");
        CardLayout card = (CardLayout) cardpanelPPSPrincipal.getLayout();
        card.show(cardpanelPPSPrincipal, "panel ppsSaida");
    }
    public static void showPPSTabelaEntrada(){
        CardLayout card = (CardLayout) cardpanelPPSPrincipal.getLayout();
        card.show(cardpanelPPSPrincipal, "panel ppsEntradaTable");
        panelEntradaTable.inicializaTabelaPPS();
        
    }
    static void showPPSEdit(ProdutoParaSaude produto) {
        PPSEdicao panelPpsedit = new PPSEdicao(produto);
        cardpanelPPSPrincipal.add(panelPpsedit,"panel ppsEdit");
        CardLayout card = (CardLayout) cardpanelPPSPrincipal.getLayout();
        card.show(cardpanelPPSPrincipal, "panel ppsEdit");
    }
    public static void atualizaNumNoficacoes(){
        btNotificacoes.setText(EstoqueDAO.getEstoqueVencidoSize()+"");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedPanelPrincipal = new javax.swing.JTabbedPane();
        panelMedicamentoView = new javax.swing.JPanel();
        cardPanelMedicamentoPrincipal = new javax.swing.JPanel();
        panelMenuLateralMedicamento = new javax.swing.JPanel();
        btHomeMedicamento = new javax.swing.JButton();
        btCadastroMedicamento = new javax.swing.JButton();
        btRelatorioMedicamento = new javax.swing.JButton();
        btEntradaMedicamento = new javax.swing.JButton();
        panelPPSView = new javax.swing.JPanel();
        panelMenuLateralPPS = new javax.swing.JPanel();
        btHomePPS = new javax.swing.JButton();
        btCadastroPPS = new javax.swing.JButton();
        btRelatorioPPS = new javax.swing.JButton();
        btEntrada = new javax.swing.JButton();
        cardpanelPPSPrincipal = new javax.swing.JPanel();
        btSair = new javax.swing.JButton();
        btConfiguração = new javax.swing.JButton();
        btNotificacoes = new javax.swing.JButton();

        tabbedPanelPrincipal.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N

        panelMedicamentoView.setOpaque(false);

        cardPanelMedicamentoPrincipal.setOpaque(false);
        cardPanelMedicamentoPrincipal.setLayout(new java.awt.CardLayout());

        btHomeMedicamento.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btHomeMedicamento.setText("Home");
        btHomeMedicamento.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btHomeMedicamento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btHomeMedicamento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btHomeMedicamentoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btHomeMedicamentoMouseExited(evt);
            }
        });
        btHomeMedicamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btHomeMedicamentoActionPerformed(evt);
            }
        });

        btCadastroMedicamento.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btCadastroMedicamento.setText("Cadastrar Medicamento");
        btCadastroMedicamento.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btCadastroMedicamento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btCadastroMedicamento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btCadastroMedicamentoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btCadastroMedicamentoMouseExited(evt);
            }
        });
        btCadastroMedicamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCadastroMedicamentoActionPerformed(evt);
            }
        });

        btRelatorioMedicamento.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btRelatorioMedicamento.setText("Relatorio");
        btRelatorioMedicamento.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRelatorioMedicamento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btRelatorioMedicamento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btRelatorioMedicamentoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btRelatorioMedicamentoMouseExited(evt);
            }
        });
        btRelatorioMedicamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRelatorioMedicamentoActionPerformed(evt);
            }
        });

        btEntradaMedicamento.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btEntradaMedicamento.setText("Entrada");
        btEntradaMedicamento.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btEntradaMedicamento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btEntradaMedicamento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btEntradaMedicamentoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btEntradaMedicamentoMouseExited(evt);
            }
        });
        btEntradaMedicamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEntradaMedicamentoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMenuLateralMedicamentoLayout = new javax.swing.GroupLayout(panelMenuLateralMedicamento);
        panelMenuLateralMedicamento.setLayout(panelMenuLateralMedicamentoLayout);
        panelMenuLateralMedicamentoLayout.setHorizontalGroup(
            panelMenuLateralMedicamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuLateralMedicamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMenuLateralMedicamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btRelatorioMedicamento, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btEntradaMedicamento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btCadastroMedicamento, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                    .addComponent(btHomeMedicamento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelMenuLateralMedicamentoLayout.setVerticalGroup(
            panelMenuLateralMedicamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuLateralMedicamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btHomeMedicamento, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btCadastroMedicamento, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btEntradaMedicamento, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btRelatorioMedicamento, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(353, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelMedicamentoViewLayout = new javax.swing.GroupLayout(panelMedicamentoView);
        panelMedicamentoView.setLayout(panelMedicamentoViewLayout);
        panelMedicamentoViewLayout.setHorizontalGroup(
            panelMedicamentoViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMedicamentoViewLayout.createSequentialGroup()
                .addComponent(panelMenuLateralMedicamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(cardPanelMedicamentoPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 780, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panelMedicamentoViewLayout.setVerticalGroup(
            panelMedicamentoViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMedicamentoViewLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMedicamentoViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelMenuLateralMedicamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cardPanelMedicamentoPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        tabbedPanelPrincipal.addTab("Medicamento", panelMedicamentoView);

        panelPPSView.setOpaque(false);

        btHomePPS.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btHomePPS.setText("Home");
        btHomePPS.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btHomePPS.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btHomePPS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btHomePPSMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btHomePPSMouseExited(evt);
            }
        });
        btHomePPS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btHomePPSActionPerformed(evt);
            }
        });

        btCadastroPPS.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btCadastroPPS.setText("Cadastrar PPS");
        btCadastroPPS.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btCadastroPPS.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btCadastroPPS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btCadastroPPSMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btCadastroPPSMouseExited(evt);
            }
        });
        btCadastroPPS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCadastroPPSActionPerformed(evt);
            }
        });

        btRelatorioPPS.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btRelatorioPPS.setText("Relatorio");
        btRelatorioPPS.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRelatorioPPS.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btRelatorioPPS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btRelatorioPPSMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btRelatorioPPSMouseExited(evt);
            }
        });
        btRelatorioPPS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRelatorioPPSActionPerformed(evt);
            }
        });

        btEntrada.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btEntrada.setText("Entrada");
        btEntrada.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btEntrada.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btEntrada.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btEntradaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btEntradaMouseExited(evt);
            }
        });
        btEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEntradaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMenuLateralPPSLayout = new javax.swing.GroupLayout(panelMenuLateralPPS);
        panelMenuLateralPPS.setLayout(panelMenuLateralPPSLayout);
        panelMenuLateralPPSLayout.setHorizontalGroup(
            panelMenuLateralPPSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuLateralPPSLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMenuLateralPPSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btRelatorioPPS, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btCadastroPPS, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btEntrada, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelMenuLateralPPSLayout.createSequentialGroup()
                        .addComponent(btHomePPS, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelMenuLateralPPSLayout.setVerticalGroup(
            panelMenuLateralPPSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuLateralPPSLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btHomePPS, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btCadastroPPS, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btRelatorioPPS, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(353, Short.MAX_VALUE))
        );

        cardpanelPPSPrincipal.setOpaque(false);
        cardpanelPPSPrincipal.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout panelPPSViewLayout = new javax.swing.GroupLayout(panelPPSView);
        panelPPSView.setLayout(panelPPSViewLayout);
        panelPPSViewLayout.setHorizontalGroup(
            panelPPSViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPPSViewLayout.createSequentialGroup()
                .addComponent(panelMenuLateralPPS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(cardpanelPPSPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 773, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        panelPPSViewLayout.setVerticalGroup(
            panelPPSViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPPSViewLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPPSViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelMenuLateralPPS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelPPSViewLayout.createSequentialGroup()
                        .addComponent(cardpanelPPSPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        tabbedPanelPrincipal.addTab("Produto Para Saude", panelPPSView);

        btSair.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        btSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/sair.png"))); // NOI18N
        btSair.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSair.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btSair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btSairMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btSairMouseExited(evt);
            }
        });
        btSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSairActionPerformed(evt);
            }
        });

        btConfiguração.setForeground(java.awt.SystemColor.activeCaption);
        btConfiguração.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/settings.png"))); // NOI18N
        btConfiguração.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btConfiguração.setBorderPainted(false);
        btConfiguração.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btConfiguração.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btConfiguraçãoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btConfiguraçãoMouseExited(evt);
            }
        });
        btConfiguração.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btConfiguraçãoActionPerformed(evt);
            }
        });

        btNotificacoes.setForeground(java.awt.SystemColor.activeCaption);
        btNotificacoes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/notificacoes.png"))); // NOI18N
        btNotificacoes.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btNotificacoes.setBorderPainted(false);
        btNotificacoes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btNotificacoes.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        btNotificacoes.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btNotificacoes.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btNotificacoes.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btNotificacoes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btNotificacoesMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btNotificacoesMouseExited(evt);
            }
        });
        btNotificacoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNotificacoesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(846, 846, 846)
                .addComponent(btNotificacoes)
                .addGap(18, 18, 18)
                .addComponent(btConfiguração)
                .addGap(18, 18, 18)
                .addComponent(btSair))
            .addComponent(tabbedPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 1067, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btConfiguração, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btSair, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(btNotificacoes))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabbedPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 619, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btHomeMedicamentoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btHomeMedicamentoMouseEntered
        btHomeMedicamento.setBackground(new Color(0,153,153));
        btHomeMedicamento.setForeground(Color.WHITE);
    }//GEN-LAST:event_btHomeMedicamentoMouseEntered

    private void btHomeMedicamentoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btHomeMedicamentoMouseExited
        btHomeMedicamento.setBackground(new Color(240,240,240));
        btHomeMedicamento.setForeground(Color.BLACK);
    }//GEN-LAST:event_btHomeMedicamentoMouseExited

    private void btHomeMedicamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btHomeMedicamentoActionPerformed
        showMedHome();
    }//GEN-LAST:event_btHomeMedicamentoActionPerformed

    private void btCadastroMedicamentoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btCadastroMedicamentoMouseEntered
        btCadastroMedicamento.setBackground(new Color(0,153,153));
        btCadastroMedicamento.setForeground(Color.WHITE);
    }//GEN-LAST:event_btCadastroMedicamentoMouseEntered

    private void btCadastroMedicamentoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btCadastroMedicamentoMouseExited
        btCadastroMedicamento.setBackground(new Color(240,240,240));
        btCadastroMedicamento.setForeground(Color.BLACK);
    }//GEN-LAST:event_btCadastroMedicamentoMouseExited

    private void btCadastroMedicamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCadastroMedicamentoActionPerformed
        panelMedicamentoCadastro.iniciarComboBoxApresentacao();
        panelMedicamentoCadastro.iniciarComboBoxConcentracao();
        panelMedicamentoCadastro.iniciarComboBoxModalidade();
        panelMedicamentoCadastro.limpaForm();
        CardLayout card = (CardLayout) cardPanelMedicamentoPrincipal.getLayout();
        card.show(cardPanelMedicamentoPrincipal, "panel medCadastro");
    }//GEN-LAST:event_btCadastroMedicamentoActionPerformed

    private void btRelatorioMedicamentoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btRelatorioMedicamentoMouseEntered
        btRelatorioMedicamento.setBackground(new Color(0,153,153));
        btRelatorioMedicamento.setForeground(Color.WHITE);
    }//GEN-LAST:event_btRelatorioMedicamentoMouseEntered

    private void btRelatorioMedicamentoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btRelatorioMedicamentoMouseExited
        btRelatorioMedicamento.setBackground(new Color(240,240,240));
        btRelatorioMedicamento.setForeground(Color.BLACK);
    }//GEN-LAST:event_btRelatorioMedicamentoMouseExited

    private void btRelatorioMedicamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRelatorioMedicamentoActionPerformed
        showMedRelatorio();
    }//GEN-LAST:event_btRelatorioMedicamentoActionPerformed

    private void btEntradaMedicamentoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btEntradaMedicamentoMouseEntered
        btEntradaMedicamento.setBackground(new Color(0,153,153));
        btEntradaMedicamento.setForeground(Color.WHITE);
    }//GEN-LAST:event_btEntradaMedicamentoMouseEntered

    private void btEntradaMedicamentoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btEntradaMedicamentoMouseExited
        btEntradaMedicamento.setBackground(new Color(240,240,240));
        btEntradaMedicamento.setForeground(Color.BLACK);
    }//GEN-LAST:event_btEntradaMedicamentoMouseExited

    private void btEntradaMedicamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEntradaMedicamentoActionPerformed
        showMedTabelaEntrada();
    }//GEN-LAST:event_btEntradaMedicamentoActionPerformed

    private void btHomePPSMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btHomePPSMouseEntered
        btHomePPS.setBackground(new Color(0,153,153));
        btHomePPS.setForeground(Color.WHITE);
    }//GEN-LAST:event_btHomePPSMouseEntered

    private void btHomePPSMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btHomePPSMouseExited
        btHomePPS.setBackground(new Color(240,240,240));
        btHomePPS.setForeground(Color.BLACK);
    }//GEN-LAST:event_btHomePPSMouseExited

    private void btHomePPSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btHomePPSActionPerformed
        showPPSHome();
    }//GEN-LAST:event_btHomePPSActionPerformed

    private void btCadastroPPSMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btCadastroPPSMouseEntered
        btCadastroPPS.setBackground(new Color(0,153,153));
        btCadastroPPS.setForeground(Color.WHITE);
    }//GEN-LAST:event_btCadastroPPSMouseEntered

    private void btCadastroPPSMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btCadastroPPSMouseExited
        btCadastroPPS.setBackground(new Color(240,240,240));
        btCadastroPPS.setForeground(Color.BLACK);
    }//GEN-LAST:event_btCadastroPPSMouseExited

    private void btCadastroPPSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCadastroPPSActionPerformed
        panelPPSCadastro.inicializaComboBoxModalidade();
        panelPPSCadastro.inicializaComboBoxUnidade();
        panelPPSCadastro.limpaForm();
        CardLayout card = (CardLayout) cardpanelPPSPrincipal.getLayout();
        card.show(cardpanelPPSPrincipal, "panel ppsCadastro");
    }//GEN-LAST:event_btCadastroPPSActionPerformed

    private void btRelatorioPPSMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btRelatorioPPSMouseEntered
        btRelatorioPPS.setBackground(new Color(0,153,153));
        btRelatorioPPS.setForeground(Color.WHITE);
    }//GEN-LAST:event_btRelatorioPPSMouseEntered

    private void btRelatorioPPSMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btRelatorioPPSMouseExited
        btRelatorioPPS.setBackground(new Color(240,240,240));
        btRelatorioPPS.setForeground(Color.BLACK);
    }//GEN-LAST:event_btRelatorioPPSMouseExited

    private void btRelatorioPPSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRelatorioPPSActionPerformed
        CardLayout card = (CardLayout) cardpanelPPSPrincipal.getLayout();
        card.show(cardpanelPPSPrincipal, "panel ppsRelatorio");
    }//GEN-LAST:event_btRelatorioPPSActionPerformed

    private void btEntradaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btEntradaMouseEntered
        btEntrada.setBackground(new Color(0,153,153));
        btEntrada.setForeground(Color.WHITE);
    }//GEN-LAST:event_btEntradaMouseEntered

    private void btEntradaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btEntradaMouseExited
        btEntrada.setBackground(new Color(240,240,240));
        btEntrada.setForeground(Color.BLACK);
    }//GEN-LAST:event_btEntradaMouseExited

    private void btEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEntradaActionPerformed
        showPPSTabelaEntrada();
    }//GEN-LAST:event_btEntradaActionPerformed

    private void btSairMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btSairMouseEntered
        btSair.setBackground(new Color(0,153,153));
        btSair.setForeground(Color.WHITE);
    }//GEN-LAST:event_btSairMouseEntered

    private void btSairMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btSairMouseExited
        btSair.setBackground(new Color(240,240,240));
        btSair.setForeground(Color.BLACK);
    }//GEN-LAST:event_btSairMouseExited

    private void btSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSairActionPerformed
        Object[] options = {"Confirmar","Cancelar"};
        int resposta = JOptionPane.showOptionDialog(getParent(),
            "Deseja realmente encerrar a sessão?",
            "Aviso",
            JOptionPane.DEFAULT_OPTION,
            JOptionPane.QUESTION_MESSAGE,null,options,null);
        if(resposta == 0){
            LoginView login = new LoginView();
            JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
            topFrame.dispose();
            login.setVisible(true);
        }
    }//GEN-LAST:event_btSairActionPerformed

    private void btConfiguraçãoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btConfiguraçãoMouseEntered
        btConfiguração.setBackground(new Color(0,153,153));
        btConfiguração.setForeground(Color.WHITE);
    }//GEN-LAST:event_btConfiguraçãoMouseEntered

    private void btConfiguraçãoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btConfiguraçãoMouseExited
        btConfiguração.setBackground(new Color(240,240,240));
        btConfiguração.setForeground(Color.BLACK);
    }//GEN-LAST:event_btConfiguraçãoMouseExited

    private void btConfiguraçãoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btConfiguraçãoActionPerformed
        showMedHome();
        showPPSHome();
        PrincipalFrame.showPanelConfiguracao();
    }//GEN-LAST:event_btConfiguraçãoActionPerformed

    private void btNotificacoesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btNotificacoesMouseEntered
        btNotificacoes.setBackground(new Color(0,153,153));
        btNotificacoes.setForeground(Color.WHITE);
    }//GEN-LAST:event_btNotificacoesMouseEntered

    private void btNotificacoesMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btNotificacoesMouseExited
        btNotificacoes.setBackground(new Color(240,240,240));
        btNotificacoes.setForeground(Color.BLACK);
    }//GEN-LAST:event_btNotificacoesMouseExited

    private void btNotificacoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNotificacoesActionPerformed
        showMedHome();
        showPPSHome();
        PrincipalFrame.showPanelNotificacoes();
    }//GEN-LAST:event_btNotificacoesActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCadastroMedicamento;
    private javax.swing.JButton btCadastroPPS;
    private javax.swing.JButton btConfiguração;
    private javax.swing.JButton btEntrada;
    private javax.swing.JButton btEntradaMedicamento;
    private javax.swing.JButton btHomeMedicamento;
    private javax.swing.JButton btHomePPS;
    private static javax.swing.JButton btNotificacoes;
    private javax.swing.JButton btRelatorioMedicamento;
    private javax.swing.JButton btRelatorioPPS;
    private javax.swing.JButton btSair;
    private static javax.swing.JPanel cardPanelMedicamentoPrincipal;
    private static javax.swing.JPanel cardpanelPPSPrincipal;
    private javax.swing.JPanel panelMedicamentoView;
    private javax.swing.JPanel panelMenuLateralMedicamento;
    private javax.swing.JPanel panelMenuLateralPPS;
    private javax.swing.JPanel panelPPSView;
    private javax.swing.JTabbedPane tabbedPanelPrincipal;
    // End of variables declaration//GEN-END:variables
}
