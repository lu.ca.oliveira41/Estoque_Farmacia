package Interfaces;

import DAO.EstoqueDAO;
import DAO.EstoqueMedicamentoDAO;
import DAO.EstoquePPSDAO;
import Dominio.Estoque;
import Dominio.EstoqueMedicamento;
import Dominio.EstoquePPS;
import Dominio.ItemNotificacao;
import Utils.ModelTable;
import Utils.VariableRowHeightRenderer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NotificacoesPanel extends javax.swing.JPanel {
    SimpleDateFormat in= new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat out = new SimpleDateFormat("dd/MM/yyyy");
    private List<Estoque> estoques = new ArrayList<>();
    private ArrayList<ItemNotificacao> alertas = null;
    public NotificacoesPanel(){
        initComponents();
        iniciaTabelaNotificacoes(90);
        buttonGroup1.setSelected(radio90.getModel(), true);
        BackgroundPrincipal bg = new BackgroundPrincipal();
        this.add(bg, BorderLayout.CENTER);
        
    }   
    protected void iniciaTabelaNotificacoes(int dias){
        tabelaAlertas.removeAll();
        alertas = new ArrayList<>();
        ArrayList<Object> avisos = new ArrayList<>();
        String[] colunas = {"TIPO","LOTE","VALIDADE","NOMECLATURA"};
        try {
            estoques = EstoqueDAO.selectAlertas(dias);
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode()+":"+ex.getMessage());
        }
        for(Estoque estoque:estoques){
            try {
                EstoqueMedicamento med = EstoqueMedicamentoDAO.buscaEstoqueMedicamentoByIDEstoque(estoque.getId());
                if(med!=null){
                        alertas.add(new ItemNotificacao(
                                "Medicamento",
                                med.getnLote(),
                                med.getMedicamento().getDenominacao(),
                                med.getDataValidade()
                        ));
                    
                }else{
                EstoquePPS pps = EstoquePPSDAO.buscaEstoquePPSByIDEstoque(estoque.getId());
                if(pps!=null)
                    alertas.add(new ItemNotificacao(
                                  "PPS",
                                  pps.getnLote(),
                                  pps.getPps().getDescricao(),
                                  pps.getDataValidade()
                          ));
                
                }
            } catch (SQLException ex) {
                Logger.getLogger(NotificacoesPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Collections.sort(alertas);
        for(ItemNotificacao item:alertas){
            try {
                avisos.add(new Object[]{
                    item.getTipo(),
                    item.getLote(),
                    formataData(item.getValidade()),
                    item.getNome()
                });
            } catch (ParseException ex) {
                Logger.getLogger(NotificacoesPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ModelTable model = new ModelTable(avisos, colunas);
        tabelaAlertas.setModel(model);
        tabelaAlertas.getColumnModel().getColumn(0).setPreferredWidth(200);
        tabelaAlertas.getColumnModel().getColumn(1).setPreferredWidth(400);
        tabelaAlertas.getColumnModel().getColumn(2).setPreferredWidth(400);
        tabelaAlertas.getColumnModel().getColumn(3).setPreferredWidth(800);
        tabelaAlertas.getTableHeader().setResizingAllowed(false);
        tabelaAlertas.getTableHeader().setReorderingAllowed(false);
        tabelaAlertas.getColumnModel().getColumn(3).setCellRenderer( new VariableRowHeightRenderer(tabelaAlertas.getColumnModel().getColumn(3).getMinWidth()));
        tabelaAlertas.setAutoResizeMode(tabelaAlertas.AUTO_RESIZE_ALL_COLUMNS);
        tabelaAlertas.repaint();
    }
    private String formataData(Date data) throws ParseException{
        String dataFormatada = out.format(data);
    
        return dataFormatada;
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        btVoltar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaAlertas = new javax.swing.JTable();
        labelAvisodeVencimento = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        radio60 = new javax.swing.JRadioButton();
        radio90 = new javax.swing.JRadioButton();

        btVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/voltar.png"))); // NOI18N
        btVoltar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btVoltar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btVoltarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btVoltarMouseExited(evt);
            }
        });
        btVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVoltarActionPerformed(evt);
            }
        });

        tabelaAlertas.setBackground(new java.awt.Color(0, 153, 153));
        tabelaAlertas.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tabelaAlertas.setForeground(new java.awt.Color(255, 255, 255));
        tabelaAlertas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelaAlertas);

        labelAvisodeVencimento.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        labelAvisodeVencimento.setForeground(new java.awt.Color(255, 255, 255));
        labelAvisodeVencimento.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelAvisodeVencimento.setText("AVISO DE VENCIMENTO");

        jLabel1.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Prazo de vencimento:");

        buttonGroup1.add(radio60);
        radio60.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        radio60.setForeground(new java.awt.Color(255, 255, 255));
        radio60.setText("60");
        radio60.setOpaque(false);
        radio60.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radio60ActionPerformed(evt);
            }
        });

        buttonGroup1.add(radio90);
        radio90.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        radio90.setForeground(new java.awt.Color(255, 255, 255));
        radio90.setText("90");
        radio90.setOpaque(false);
        radio90.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radio90ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 628, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btVoltar)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(labelAvisodeVencimento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1)
                                .addGap(7, 7, 7)
                                .addComponent(radio90)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(radio60)
                                .addGap(13, 13, 13)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(btVoltar))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelAvisodeVencimento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(radio60)
                            .addComponent(jLabel1)
                            .addComponent(radio90))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btVoltarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btVoltarMouseEntered
        btVoltar.setBackground(new Color(0,153,153));
    }//GEN-LAST:event_btVoltarMouseEntered

    private void btVoltarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btVoltarMouseExited
        btVoltar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_btVoltarMouseExited

    private void btVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVoltarActionPerformed
        PrincipalFrame.showPanelPrincipal();
    }//GEN-LAST:event_btVoltarActionPerformed

    private void radio60ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radio60ActionPerformed
       iniciaTabelaNotificacoes(60);
    }//GEN-LAST:event_radio60ActionPerformed

    private void radio90ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radio90ActionPerformed
        iniciaTabelaNotificacoes(90);
    }//GEN-LAST:event_radio90ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btVoltar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelAvisodeVencimento;
    private javax.swing.JRadioButton radio60;
    private javax.swing.JRadioButton radio90;
    private javax.swing.JTable tabelaAlertas;
    // End of variables declaration//GEN-END:variables
}
