
package Interfaces;

import DAO.EntradaDAO;
import DAO.EstoquePPSDAO;
import DAO.ProdutoParaSaudeDAO;
import DAO.SaidaDAO;
import Dominio.Entrada;
import Dominio.EstoquePPS;
import Dominio.ProdutoParaSaude;
import Dominio.Saida;
import Utils.ModelTable;
import Utils.VariableRowHeightRenderer;
import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PPSEntradaTable extends javax.swing.JPanel {
    private List<ProdutoParaSaude> estoque_pps = new ArrayList<>();
    private String[] colunas = {"ID","DESCRIÇÃO","UNIDADE","MODALIDADE"};
    private ArrayList dados;
    public PPSEntradaTable() {
        initComponents();
    }
    protected void inicializaTabelaPPS(){
        try{
            estoque_pps = ProdutoParaSaudeDAO.selectPPS();
            constroiTabela(estoque_pps);
        } catch (SQLException ex) {
            Logger.getLogger(PPSHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected void atualizaTabelaPPS(String query){
        try{
            estoque_pps = ProdutoParaSaudeDAO.selectPPSByDescricao(query);
            constroiTabela(estoque_pps);
        } catch (SQLException ex) {
            Logger.getLogger(PPSHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void constroiTabela(List<ProdutoParaSaude> estoque_pps){
        dados = new ArrayList();
            for(ProdutoParaSaude estoque:estoque_pps){
                dados.add(new Object[]{
                        estoque.getId(),
                        estoque.getDescricao(),
                        estoque.getUnidade().getUnidade(),
                        estoque.getModalidade().getModalidade()
                });
            }
            
            ModelTable model = new ModelTable(dados, colunas);
            tabelaPPS.setModel(model);
            tabelaPPS.getColumnModel().getColumn(0).setPreferredWidth(100);
            tabelaPPS.getColumnModel().getColumn(1).setPreferredWidth(800);
            tabelaPPS.getColumnModel().getColumn(2).setPreferredWidth(400);
            tabelaPPS.getColumnModel().getColumn(3).setPreferredWidth(400);
            tabelaPPS.getTableHeader().setResizingAllowed(false);
            tabelaPPS.getTableHeader().setReorderingAllowed(false);
            
            tabelaPPS.getSelectionModel().addListSelectionListener(selecionarLinha());
            tabelaPPS.getColumnModel().getColumn(1).setCellRenderer( new VariableRowHeightRenderer(tabelaPPS.getColumnModel().getColumn(1).getMinWidth()));
            tabelaPPS.repaint();  
    }
    private ListSelectionListener selecionarLinha(){
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tabelaPPS.getSelectedRow() != -1){
                    int cod = (int) tabelaPPS.getValueAt(tabelaPPS.getSelectedRow(), 0);
                    try {
                        ProdutoParaSaude pps = ProdutoParaSaudeDAO.selectPPSByID(cod);
                        Object[] options = {"Editar","Excluir","Realizar Entrada"};
                        int resposta = JOptionPane.showOptionDialog(getParent(),
                            "Produto: "+pps.getDescricao(),
                            "Deseja fazer a entrada?",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
                   if(resposta == 0)
                       PrincipalPanel.showPPSEdit(pps); 
                    else if(resposta==1)
                        excluirMed(pps);
                    else if(resposta==2)
                       PrincipalPanel.showPPSPrimeiraEntrada(pps);  
                   
                    tabelaPPS.clearSelection();
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                    
                }
            }
        };
    }
    private void excluirMed(ProdutoParaSaude pps){
        
        Object[] options = {"Confirmar","Cancelar"};
        int resposta = JOptionPane.showOptionDialog(getParent(),
                            "Produto: "+pps.getDescricao(),
                            "Você tem certeza que deseja excluir?",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
        if(resposta==0){
            try{
                ProdutoParaSaudeDAO.deletePPS(pps);
                JOptionPane.showMessageDialog(getParent(),"Exlusão realizada com sucesso","AVISO",JOptionPane.WARNING_MESSAGE );
                PrincipalPanel.showPPSTabelaEntrada();
            }catch (SQLException ex) {
                    JOptionPane.showMessageDialog(getParent(),"Não foi possivel excluir o registro","AVISO",JOptionPane.WARNING_MESSAGE );
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btLimpaBusca = new javax.swing.JButton();
        tfBusca = new javax.swing.JTextField();
        btBusca = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaPPS = new javax.swing.JTable();

        setOpaque(false);

        btLimpaBusca.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btLimpaBusca.setText("Limpar Busca");
        btLimpaBusca.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btLimpaBusca.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btLimpaBuscaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btLimpaBuscaMouseExited(evt);
            }
        });
        btLimpaBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimpaBuscaActionPerformed(evt);
            }
        });

        tfBusca.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N

        btBusca.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btBusca.setText("Pesquisar");
        btBusca.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btBusca.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btBuscaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btBuscaMouseExited(evt);
            }
        });
        btBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscaActionPerformed(evt);
            }
        });

        tabelaPPS.setBackground(new java.awt.Color(0, 153, 153));
        tabelaPPS.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tabelaPPS.setForeground(new java.awt.Color(255, 255, 255));
        tabelaPPS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelaPPS);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 662, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tfBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btLimpaBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tfBusca, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(btBusca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btLimpaBusca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 472, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btLimpaBuscaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btLimpaBuscaMouseEntered
        btLimpaBusca.setBackground(new Color(0,153,153));
        btLimpaBusca.setForeground(Color.WHITE);
    }//GEN-LAST:event_btLimpaBuscaMouseEntered

    private void btLimpaBuscaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btLimpaBuscaMouseExited
        btLimpaBusca.setBackground(new Color(240,240,240));
        btLimpaBusca.setForeground(Color.BLACK);
    }//GEN-LAST:event_btLimpaBuscaMouseExited

    private void btLimpaBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimpaBuscaActionPerformed
        inicializaTabelaPPS();
        btLimpaBusca.setVisible(false);
        tfBusca.setText("");
    }//GEN-LAST:event_btLimpaBuscaActionPerformed

    private void btBuscaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBuscaMouseEntered
        btBusca.setBackground(new Color(0,153,153));
        btBusca.setForeground(Color.WHITE);
    }//GEN-LAST:event_btBuscaMouseEntered

    private void btBuscaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBuscaMouseExited
        btBusca.setBackground(new Color(240,240,240));
        btBusca.setForeground(Color.BLACK);
    }//GEN-LAST:event_btBuscaMouseExited

    private void btBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscaActionPerformed
        String query = tfBusca.getText().toString().trim();
        if(query.isEmpty() || query.equals("")){
            JOptionPane.showMessageDialog(getParent(), "Preencha o campo corretamente","ALERTA",JOptionPane.ERROR_MESSAGE);
        }else{
            atualizaTabelaPPS(query);
            btLimpaBusca.setVisible(true);
        }
    }//GEN-LAST:event_btBuscaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBusca;
    private javax.swing.JButton btLimpaBusca;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaPPS;
    private javax.swing.JTextField tfBusca;
    // End of variables declaration//GEN-END:variables
}
