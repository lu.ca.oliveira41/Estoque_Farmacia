package Interfaces;

import DAO.EntradaDAO;
import DAO.EstoqueDAO;
import DAO.EstoqueMedicamentoDAO;
import DAO.SaidaDAO;
import Dominio.Entrada;
import Dominio.Estoque;
import Dominio.EstoqueMedicamento;
import Dominio.Medicamento;
import Dominio.Saida;
import java.awt.Color;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class MedicamentoEditOp extends javax.swing.JPanel {
    private Calendar cal = Calendar.getInstance();
    private Entrada entrada;
    private Saida saida;
    private EstoqueMedicamento estMed;
    private String data;
    private boolean flag;
    private int qtdOriginal;
    SimpleDateFormat in= new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat out = new SimpleDateFormat("dd/MM/yyyy");
    public MedicamentoEditOp(Entrada entrada) {
        config(entrada.getEstoque().getId());
        flag=true;
        this.entrada = entrada;
        jSpinnerQuantidade.getModel().setValue(entrada.getQuantidade());
        labelDataValidade.setText("Data de Entrada");
    }
    public MedicamentoEditOp(Saida saida) {
        config(saida.getEstoque().getId()); 
        flag=false;
        this.saida = saida;
        jSpinnerQuantidade.getModel().setValue(saida.getQuantidade());
        labelDataValidade.setText("Data de Saida");
    }
    public void config(int cod){
        initComponents();
        jTextDenominacao.setEditable(false);
        jTextDenominacao.setLineWrap(true);
        jTextDenominacao.setWrapStyleWord(true);
        jTextApresentacao.setEditable(false);
        jTextConcentracao.setEditable(false);
        jTextModalidade.setEditable(false);
        jTextLote.setEditable(false);
        
        try {
            estMed = EstoqueMedicamentoDAO.buscaEstoqueMedicamentoByIDEstoque(cod);
            qtdOriginal = estMed.getQuantidade();
            data = out.format(in.parse(estMed.getDataValidade().toString()));
            jFormattedData.setText(data);
            
            jTextDenominacao.setText(estMed.getMedicamento().getDenominacao());
            jTextApresentacao.setText(estMed.getMedicamento().getApresentacao().toString());
            jTextConcentracao.setText(estMed.getMedicamento().getConcentracao().toString());
            jTextModalidade.setText(estMed.getMedicamento().getModalidade().toString());
            jTextLote.setText(estMed.getnLote());
        } catch (SQLException ex) {
            Logger.getLogger(MedicamentoEditOp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(MedicamentoEditOp.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextApresentacao = new javax.swing.JTextField();
        jTextConcentracao = new javax.swing.JTextField();
        jTextModalidade = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextLote = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jSpinnerQuantidade = new javax.swing.JSpinner();
        jButtonCancelar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        labelDataValidade = new javax.swing.JLabel();
        jFormattedData = new javax.swing.JFormattedTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextDenominacao = new javax.swing.JTextArea();

        setOpaque(false);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("BAIXA EM ESTOQUE");

        jLabel2.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel2.setText("Denominação comum brasileira");

        jLabel3.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel3.setText("Apresentação");

        jLabel4.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel4.setText("Concentração");

        jLabel5.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel5.setText("Modalidade");

        jTextApresentacao.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jTextConcentracao.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jTextModalidade.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jLabel6.setText("Lote");

        jTextLote.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel7.setText("Quantidade");

        jSpinnerQuantidade.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jButtonCancelar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jButtonCancelar.setText("CANCELAR");
        jButtonCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseExited(evt);
            }
        });
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jButtonSalvar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jButtonSalvar.setText("SALVAR");
        jButtonSalvar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonSalvarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonSalvarMouseExited(evt);
            }
        });
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        labelDataValidade.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        labelDataValidade.setText("Data Saida");

        try {
            jFormattedData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedData.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedData.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jTextDenominacao.setColumns(20);
        jTextDenominacao.setRows(5);
        jScrollPane1.setViewportView(jTextDenominacao);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jButtonCancelar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27))
                            .addComponent(jTextLote, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextModalidade, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextConcentracao, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextApresentacao)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jSpinnerQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelDataValidade)
                                .addGap(18, 18, 18)
                                .addComponent(jFormattedData, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)))
                .addContainerGap(93, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextLote, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jFormattedData, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelDataValidade))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jSpinnerQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(57, 57, 57))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        jButtonCancelar.setEnabled(false);
        PrincipalPanel.showMedRelatorio();
        jButtonCancelar.setEnabled(true);
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        jButtonSalvar.setEnabled(false);
        int qtd = Integer.parseInt(jSpinnerQuantidade.getValue().toString());
        data = jFormattedData.getText().toString();
        Calendar calendar = Calendar.getInstance();
        Calendar horario = Calendar.getInstance();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            calendar.setTime(sdf.parse(data));
            calendar.set(Calendar.HOUR_OF_DAY,horario.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE,horario.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND,horario.get(Calendar.SECOND));
        } catch (ParseException ex) {
           System.out.println("Problema ao formatar data");
        }
        if(qtd < 0){
            JOptionPane.showMessageDialog(getParent(),"Quantidade inválida!","AVISO",JOptionPane.WARNING_MESSAGE);
            jButtonSalvar.setEnabled(true);
        }else{
            try {
                if(flag){
                    //atualiza entrada
                    if(qtd>=qtdOriginal){
                        entrada.setQtd(qtd);
                        estMed.setQuantidade(qtd);
                        EstoqueDAO.updateEstoque(estMed);
                        EntradaDAO.AtualizaEntrada(entrada);
                    }else if(qtdOriginal-qtd<=0){
                        JOptionPane.showMessageDialog(getParent(),"Quantidade inválida!","AVISO",JOptionPane.WARNING_MESSAGE);
                        jButtonSalvar.setEnabled(true);
                    }
                    
                }else{
                    //atualiza saida
                }
                PrincipalPanel.showMedHome();
                jButtonSalvar.setEnabled(true);
            
            } catch (SQLException ex) {
                Logger.getLogger(MedicamentoEntrada.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        jButtonSalvar.setEnabled(true);
        
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseEntered
        jButtonCancelar.setBackground(Color.RED);
        jButtonCancelar.setForeground(Color.WHITE);
    }//GEN-LAST:event_jButtonCancelarMouseEntered

    private void jButtonCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseExited
        jButtonCancelar.setBackground(new Color(240,240,240));
        jButtonCancelar.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonCancelarMouseExited

    private void jButtonSalvarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSalvarMouseEntered
        jButtonSalvar.setBackground(new Color(0,153,204));
        jButtonSalvar.setForeground(Color.WHITE);
    }//GEN-LAST:event_jButtonSalvarMouseEntered

    private void jButtonSalvarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSalvarMouseExited
        jButtonSalvar.setBackground(new Color(240,240,240));
        jButtonSalvar.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonSalvarMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JFormattedTextField jFormattedData;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jSpinnerQuantidade;
    private javax.swing.JTextField jTextApresentacao;
    private javax.swing.JTextField jTextConcentracao;
    private javax.swing.JTextArea jTextDenominacao;
    private javax.swing.JTextField jTextLote;
    private javax.swing.JTextField jTextModalidade;
    private javax.swing.JLabel labelDataValidade;
    // End of variables declaration//GEN-END:variables
}
