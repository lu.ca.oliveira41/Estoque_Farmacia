package Interfaces;

import DAO.ApresentacaoDAO;
import DAO.ModalidadeDAO;
import DAO.ProdutoParaSaudeDAO;
import DAO.UnidadeDAO;
import Dominio.Apresentacao;
import Dominio.Modalidade;
import Dominio.ProdutoParaSaude;
import Dominio.Unidade;
import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
public class PPSEdicao extends javax.swing.JPanel {
    private List<Modalidade> modalidades = new ArrayList<>();
    private List<Unidade> unidades = new ArrayList<>();
    private ProdutoParaSaude produto;
    public PPSEdicao(ProdutoParaSaude produto) {
        initComponents();
        this.produto=produto;
        inicializaComboBoxUnidade(produto.getUnidade().getId());
        inicializaComboBoxModalidade(produto.getModalidade().getId());
        tAPPS.setLineWrap(true);
        tAPPS.setWrapStyleWord(true);
        tAPPS.setText(produto.getDescricao());
         
    }
    protected void inicializaComboBoxUnidade(int cod){
        comboBoxUnidade.removeAllItems();
        try {
            unidades = UnidadeDAO.selectUnidade();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode() +":"+ex.getMessage());
        }
        for(Unidade unidade:unidades){
            comboBoxUnidade.addItem(unidade);
        }
        comboBoxUnidade.setSelectedIndex(cod);
    }
    protected void inicializaComboBoxModalidade(int cod){
        comboBoxModalidade.removeAllItems();
        try {
            modalidades = ModalidadeDAO.selectModalidade();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode() +":"+ex.getMessage());
        }
        for(Modalidade modalidade:modalidades){
            comboBoxModalidade.addItem(modalidade);
        }
        comboBoxModalidade.setSelectedIndex(cod);
    }
    public void limpaForm(){
        tAPPS.setText("");
        comboBoxUnidade.setSelectedIndex(-1);
        
        comboBoxModalidade.setSelectedIndex(-1);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelCadastroPPS = new javax.swing.JLabel();
        labelConcentracao = new javax.swing.JLabel();
        labelDescricao = new javax.swing.JLabel();
        labelApresentacao = new javax.swing.JLabel();
        comboBoxModalidade = new javax.swing.JComboBox<>();
        comboBoxUnidade = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tAPPS = new javax.swing.JTextArea();
        btSalvar = new javax.swing.JButton();
        btCancelar = new javax.swing.JButton();

        setOpaque(false);

        labelCadastroPPS.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        labelCadastroPPS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelCadastroPPS.setText("CADASTRO DE PPS");

        labelConcentracao.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        labelConcentracao.setText("Modalidade");

        labelDescricao.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        labelDescricao.setText("Descricao");

        labelApresentacao.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        labelApresentacao.setText("Unidade");

        comboBoxModalidade.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        comboBoxUnidade.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        tAPPS.setColumns(20);
        tAPPS.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        tAPPS.setRows(5);
        tAPPS.setAutoscrolls(false);
        jScrollPane1.setViewportView(tAPPS);

        btSalvar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btSalvar.setText("SALVAR");
        btSalvar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSalvar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btSalvarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btSalvarMouseExited(evt);
            }
        });
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });

        btCancelar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btCancelar.setText("CANCELAR");
        btCancelar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btCancelarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btCancelarMouseExited(evt);
            }
        });
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(100, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                        .addComponent(btSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1)
                    .addComponent(comboBoxModalidade, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboBoxUnidade, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(100, Short.MAX_VALUE))
            .addComponent(labelCadastroPPS, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(labelCadastroPPS, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(77, 77, 77))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        btSalvar.setEnabled(false);
        if(tAPPS.getText().isEmpty() || tAPPS.getText().length()<=0){
            JOptionPane.showMessageDialog(getParent(),"Preencha os campos corretamente","AVISO",JOptionPane.WARNING_MESSAGE );
            btSalvar.setEnabled(true);
        }
        if(comboBoxUnidade.getSelectedIndex() == -1 ||
            comboBoxModalidade.getSelectedIndex() == -1 ){
            JOptionPane.showMessageDialog(getParent(),"Selecione as informações corretamente","AVISO",JOptionPane.WARNING_MESSAGE );
            btSalvar.setEnabled(true);
        }else{
            String descricao = tAPPS.getText().toString().trim();
            Unidade unidade = (Unidade) comboBoxUnidade.getSelectedItem();
            Modalidade modalidade = (Modalidade) comboBoxModalidade.getSelectedItem();
            produto.setDescricao(descricao);
            produto.setModalidade(modalidade);
            produto.setUnidade(unidade);
            try {
                
                Object[] options = {"Confirmar"};
                         int resposta = JOptionPane.showOptionDialog(getParent(),
                             "Você tem certeza das alterações?",
                             "AVISO?",
                             JOptionPane.DEFAULT_OPTION,
                             JOptionPane.QUESTION_MESSAGE,null,options,null);
                if(resposta == 0){
                    boolean atz = ProdutoParaSaudeDAO.updatePPS(produto);
                    if(!atz){
                        JOptionPane.showMessageDialog(getParent(),"Produto Atualizado","AVISO",JOptionPane.INFORMATION_MESSAGE );
                        PrincipalPanel.showPPSTabelaEntrada();
                    }else{
                        JOptionPane.showMessageDialog(getParent(),"Algo de errado ocorreu","AVISO",JOptionPane.INFORMATION_MESSAGE );
                        btSalvar.setEnabled(false);
                    }   
                }
            } catch (SQLException ex) {
                System.out.println(ex.getErrorCode() +":"+ex.getMessage());
            }
            
        }
        btSalvar.setEnabled(true);
    }//GEN-LAST:event_btSalvarActionPerformed

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        btCancelar.setEnabled(false);
        PrincipalPanel.showPPSHome();
        btCancelar.setEnabled(true);
    }//GEN-LAST:event_btCancelarActionPerformed

    private void btCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btCancelarMouseEntered
        btCancelar.setBackground(Color.RED);
        btCancelar.setForeground(Color.WHITE);
    }//GEN-LAST:event_btCancelarMouseEntered

    private void btCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btCancelarMouseExited
        btCancelar.setBackground(new Color(240,240,240));
        btCancelar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btCancelarMouseExited

    private void btSalvarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btSalvarMouseEntered
        btSalvar.setBackground(new Color(0,153,153));
        btSalvar.setForeground(Color.WHITE);
    }//GEN-LAST:event_btSalvarMouseEntered

    private void btSalvarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btSalvarMouseExited
        btSalvar.setBackground(new Color(240,240,240));
        btSalvar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btSalvarMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCancelar;
    private javax.swing.JButton btSalvar;
    private javax.swing.JComboBox<Modalidade> comboBoxModalidade;
    private javax.swing.JComboBox<Unidade> comboBoxUnidade;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelApresentacao;
    private javax.swing.JLabel labelCadastroPPS;
    private javax.swing.JLabel labelConcentracao;
    private javax.swing.JLabel labelDescricao;
    private javax.swing.JTextArea tAPPS;
    // End of variables declaration//GEN-END:variables

    void atualizaJBox() {
        inicializaComboBoxModalidade(-1);
        inicializaComboBoxUnidade(-1);
    }
}
