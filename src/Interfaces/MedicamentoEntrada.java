 package Interfaces;

import DAO.EntradaDAO;
import DAO.EstoqueDAO;
import DAO.EstoqueMedicamentoDAO;
import Dominio.Entrada;
import Dominio.Estoque;
import Dominio.EstoqueMedicamento;
import Dominio.Medicamento;
import java.awt.Color;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class MedicamentoEntrada extends javax.swing.JPanel {
    private Medicamento medicamento;
    private EstoqueMedicamento estoqueMed =null;
    private boolean flagPrimeiraEntrada;
    private Calendar cal = Calendar.getInstance();
    private Calendar calVencimento = Calendar.getInstance();
    
    public MedicamentoEntrada(EstoqueMedicamento medicamento) {//false
        initComponents();
        this.flagPrimeiraEntrada = false;
        this.estoqueMed = medicamento;
        jTextDenominacao.setLineWrap(true);
        jTextDenominacao.setWrapStyleWord(true);
        jTextDenominacao.setText(medicamento.getMedicamento().getDenominacao());
        
        jTextApresentacao.setText(medicamento.getMedicamento().getApresentacao().toString());
        jTextConcentracao.setText(medicamento.getMedicamento().getConcentracao().toString());
        jTextModalidade.setText(medicamento.getMedicamento().getModalidade().toString());
        calVencimento.setTime(medicamento.getDataValidade());
        jTextLote.setText(medicamento.getnLote());
        jFormattedDataValidade.setEditable(false);
        jTextDenominacao.setEditable(false);
        jTextApresentacao.setEditable(false);
        jTextConcentracao.setEditable(false);
        jTextModalidade.setEditable(false);
        jTextLote.setEditable(false);
        
        String dataEntrada;
        String dataValidade;
        
        
        if(cal.get(GregorianCalendar.DAY_OF_MONTH)<10)
            dataEntrada = "0"+cal.get(GregorianCalendar.DAY_OF_MONTH);
        else
            dataEntrada = ""+cal.get(GregorianCalendar.DAY_OF_MONTH);
        
        if(cal.get(GregorianCalendar.MONTH)>=9)
            dataEntrada +=(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        else
            dataEntrada +="0"+(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        
        jFormattedDataEntrada.setText(dataEntrada);
        
        if(calVencimento.get(GregorianCalendar.DAY_OF_MONTH)<10)
            dataValidade = "0"+calVencimento.get(GregorianCalendar.DAY_OF_MONTH);
        else
            dataValidade = ""+calVencimento.get(GregorianCalendar.DAY_OF_MONTH);
        
        if(calVencimento.get(GregorianCalendar.MONTH)>=9)
            dataValidade +=(calVencimento.get(GregorianCalendar.MONTH)+1)+""+calVencimento.get(GregorianCalendar.YEAR);
        else
            dataValidade +="0"+(calVencimento.get(GregorianCalendar.MONTH)+1)+""+calVencimento.get(GregorianCalendar.YEAR);
        
        jFormattedDataValidade.setText(dataValidade);
    }
    
    public MedicamentoEntrada(Medicamento medicamento){//verdadeiro
        initComponents();
        this.medicamento = medicamento;
        this.flagPrimeiraEntrada = true;
        jTextDenominacao.setLineWrap(true);
        jTextDenominacao.setWrapStyleWord(true);
        jTextDenominacao.setText(medicamento.getDenominacao());
        jTextApresentacao.setText(medicamento.getApresentacao().toString());
        jTextConcentracao.setText(medicamento.getConcentracao().toString());
        jTextModalidade.setText(medicamento.getModalidade().toString());
        jTextDenominacao.setEditable(false);
        jTextApresentacao.setEditable(false);
        jTextConcentracao.setEditable(false);
        jTextModalidade.setEditable(false);
         String dataEntrada;
        
        
        if(cal.get(GregorianCalendar.DAY_OF_MONTH)<10)
            dataEntrada = "0"+cal.get(GregorianCalendar.DAY_OF_MONTH);
        else
            dataEntrada = ""+cal.get(GregorianCalendar.DAY_OF_MONTH);
        
        if(cal.get(GregorianCalendar.MONTH)>=9)
            dataEntrada +=(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        else
            dataEntrada +="0"+(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        
        jFormattedDataEntrada.setText(dataEntrada);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextApresentacao = new javax.swing.JTextField();
        jTextConcentracao = new javax.swing.JTextField();
        jTextModalidade = new javax.swing.JTextField();
        jSpinnerQtdEntrada = new javax.swing.JSpinner();
        jFormattedDataValidade = new javax.swing.JFormattedTextField();
        jTextLote = new javax.swing.JTextField();
        jButtonCancelar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jFormattedDataEntrada = new javax.swing.JFormattedTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextDenominacao = new javax.swing.JTextArea();

        setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("ENTRADA EM ESTOQUE");

        jLabel2.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel2.setText("Denominação comum brasileira");

        jLabel3.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel3.setText("Apresentação");

        jLabel4.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel4.setText("Concentração");

        jLabel5.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel5.setText("Modalidade");

        jLabel6.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel6.setText("Quantidade de entrada");

        jLabel7.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel7.setText("Data de Validade");

        jLabel8.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel8.setText("Lote");

        jTextApresentacao.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jTextConcentracao.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jTextModalidade.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jSpinnerQtdEntrada.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        try {
            jFormattedDataValidade.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataValidade.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataValidade.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jTextLote.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jButtonCancelar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jButtonCancelar.setText("CANCELAR");
        jButtonCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseExited(evt);
            }
        });
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jButtonSalvar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jButtonSalvar.setText("SALVAR");
        jButtonSalvar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonSalvarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonSalvarMouseExited(evt);
            }
        });
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel9.setText("Data de Entrada");

        try {
            jFormattedDataEntrada.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataEntrada.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataEntrada.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jTextDenominacao.setColumns(20);
        jTextDenominacao.setRows(5);
        jTextDenominacao.setTabSize(1);
        jScrollPane1.setViewportView(jTextDenominacao);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 126, Short.MAX_VALUE)
                                .addComponent(jTextConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jSpinnerQtdEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(jFormattedDataValidade, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jTextLote, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel9)
                            .addGap(18, 18, 18)
                            .addComponent(jFormattedDataEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(88, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jButtonCancelar)
                .addGap(79, 79, 79)
                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(127, 127, 127))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(81, 81, 81))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jTextModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jFormattedDataValidade, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)
                            .addComponent(jSpinnerQtdEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jFormattedDataEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextLote, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        jButtonCancelar.setEnabled(false);
        PrincipalPanel.showMedHome();
        jButtonCancelar.setEnabled(true);
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        jButtonSalvar.setEnabled(false);
        int qtd = Integer.parseInt(jSpinnerQtdEntrada.getValue().toString());
        String lote = jTextLote.getText().toString().trim();
        String dataValidade = jFormattedDataValidade.getText().toString();
        String dataEntrada = jFormattedDataEntrada.getText().toString();
        Calendar calendarEntrada = Calendar.getInstance();
        Calendar calendarValidade = Calendar.getInstance();
       
        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Calendar horario = Calendar.getInstance();
            calendarEntrada.setTime(sdf.parse(dataEntrada));
            calendarEntrada.set(Calendar.HOUR_OF_DAY,horario.get(Calendar.HOUR_OF_DAY));
            calendarEntrada.set(Calendar.MINUTE,horario.get(Calendar.MINUTE));
            calendarEntrada.set(Calendar.SECOND,horario.get(Calendar.SECOND));
            System.out.println(calendarEntrada.getTime());
            calendarValidade.setTime(sdf.parse(dataValidade));
        } catch (ParseException ex) {
           System.out.println("Problema ao formatar data");
        }
        if(qtd <= 0 || lote.equals("") || lote.length()<=0 ||lote.isEmpty()){
            JOptionPane.showMessageDialog(getParent(),"Preencha os Campos Corretamente!","ERRO",JOptionPane.WARNING_MESSAGE);
            jButtonSalvar.setEnabled(true);
            
        }else{
            try {
                if(flagPrimeiraEntrada && EstoqueMedicamentoDAO.selectMedicamentoByLote(lote)==null){
                    try {
                        Estoque estoque = new Estoque(0, calendarValidade.getTime());
                        int idEstoque = EstoqueDAO.insertEstoque(estoque);
                        estoque.setIdEstoque(idEstoque);
                        
                        Entrada novaEntrada = new Entrada(qtd, calendarEntrada.getTime(), estoque);
                        EntradaDAO.insertEntrada(novaEntrada);
                        
                        EstoqueMedicamento novoEstoqueMed = new EstoqueMedicamento(lote, medicamento, estoque);
                        EstoqueMedicamentoDAO.insertEstoqueMedicamento(novoEstoqueMed);
                        JOptionPane.showMessageDialog(getParent(), "Entrada registrada com sucesso",null,JOptionPane.INFORMATION_MESSAGE);
                        PrincipalPanel.showMedHome();
                        jButtonSalvar.setEnabled(true);
                    } catch (SQLException ex) {
                        Logger.getLogger(MedicamentoEntrada.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    try {
                        if(estoqueMed==null)
                            estoqueMed=EstoqueMedicamentoDAO.selectMedicamentoByLote(lote);
                        Estoque estoque = new Estoque(estoqueMed.getId(),estoqueMed.getQuantidade(),estoqueMed.getDataValidade());
                        Entrada novaEntrada = new Entrada(qtd, calendarEntrada.getTime(),estoque );
                        EntradaDAO.insertEntrada(novaEntrada);
                        JOptionPane.showMessageDialog(getParent(), "Entrada registrada com sucesso",null,JOptionPane.INFORMATION_MESSAGE);
                        PrincipalPanel.showMedHome();
                        jButtonSalvar.setEnabled(true);
                        
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(getParent(), "Ocorreu um erro na solicitacao","ERRO",JOptionPane.WARNING_MESSAGE);
                        Logger.getLogger(MedicamentoEntrada.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(MedicamentoEntrada.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        jButtonSalvar.setEnabled(true);
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseEntered
        jButtonCancelar.setBackground(Color.RED);
        jButtonCancelar.setForeground(Color.WHITE);
    }//GEN-LAST:event_jButtonCancelarMouseEntered

    private void jButtonCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseExited
        jButtonCancelar.setBackground(new Color(240,240,240));
        jButtonCancelar.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonCancelarMouseExited

    private void jButtonSalvarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSalvarMouseEntered
        jButtonSalvar.setBackground(new Color(0,153,153));
        jButtonSalvar.setForeground(Color.WHITE);
    }//GEN-LAST:event_jButtonSalvarMouseEntered

    private void jButtonSalvarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSalvarMouseExited
        jButtonSalvar.setBackground(new Color(240,240,240));
        jButtonSalvar.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonSalvarMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JFormattedTextField jFormattedDataEntrada;
    private javax.swing.JFormattedTextField jFormattedDataValidade;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jSpinnerQtdEntrada;
    private javax.swing.JTextField jTextApresentacao;
    private javax.swing.JTextField jTextConcentracao;
    private javax.swing.JTextArea jTextDenominacao;
    private javax.swing.JTextField jTextLote;
    private javax.swing.JTextField jTextModalidade;
    // End of variables declaration//GEN-END:variables
}
