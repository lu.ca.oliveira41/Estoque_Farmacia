package Interfaces;

import DAO.ApresentacaoDAO;
import DAO.ConcentracaoDAO;
import DAO.MedicamentoDAO;
import DAO.ModalidadeDAO;
import Dominio.Apresentacao;
import Dominio.Concentracao;
import Dominio.Medicamento;
import Dominio.Modalidade;
import java.awt.CardLayout;
import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class MedicamentoCadastro extends javax.swing.JPanel {
    private List<Apresentacao> apresentacoes = new ArrayList<>();
    private List<Modalidade> modalidades = new ArrayList<>();
    private List<Concentracao> concentracoes = new ArrayList<>();
    
    public MedicamentoCadastro() {
        initComponents();
        iniciarComboBoxApresentacao();
        iniciarComboBoxModalidade();
        iniciarComboBoxConcentracao();
        tfDCB.setLineWrap(true);
        tfDCB.setWrapStyleWord(true);
    }
    
    public void iniciarComboBoxApresentacao() {
        comboBoxApresentacao.removeAllItems();
        try {
            apresentacoes = ApresentacaoDAO.selectApresentacao();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode() +":"+ex.getMessage());
        }
        for(Apresentacao apresentacao:apresentacoes){
            comboBoxApresentacao.addItem(apresentacao);
        }
        comboBoxApresentacao.setSelectedIndex(-1);
        
        
    }
    public void iniciarComboBoxModalidade() {
        comboBoxModalidade.removeAllItems();
        try {
            modalidades = ModalidadeDAO.selectModalidade();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode() +":"+ex.getMessage());
        }
        for(Modalidade modalidade:modalidades){
            comboBoxModalidade.addItem(modalidade);
        }
        comboBoxModalidade.setSelectedIndex(-1);
    }
    public void iniciarComboBoxConcentracao() {
        comboBoxConcentracao.removeAllItems();
        try {
            concentracoes = ConcentracaoDAO.selectConcentracao();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode() +":"+ex.getMessage());
        }
        for(Concentracao concentracao:concentracoes){
            comboBoxConcentracao.addItem(concentracao);
        }        
         comboBoxConcentracao.setSelectedIndex(-1);
    }
    public void limpaForm(){
        tfDCB.setText("");
        comboBoxApresentacao.setSelectedIndex(-1);
        comboBoxConcentracao.setSelectedIndex(-1);
        comboBoxModalidade.setSelectedIndex(-1);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelCadastroMedicamento = new javax.swing.JLabel();
        labelConcentracao = new javax.swing.JLabel();
        labelDCB2 = new javax.swing.JLabel();
        labelModalidade = new javax.swing.JLabel();
        labelApresentacao = new javax.swing.JLabel();
        comboBoxConcentracao = new javax.swing.JComboBox<>();
        comboBoxModalidade = new javax.swing.JComboBox<>();
        comboBoxApresentacao = new javax.swing.JComboBox<>();
        btSalvar = new javax.swing.JButton();
        btCancelar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tfDCB = new javax.swing.JTextArea();

        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(750, 550));

        labelCadastroMedicamento.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        labelCadastroMedicamento.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelCadastroMedicamento.setText("CADASTRO DE MEDICAMENTO");

        labelConcentracao.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        labelConcentracao.setText("Concentração");

        labelDCB2.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        labelDCB2.setText("Denominação Comum Brasileira");

        labelModalidade.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        labelModalidade.setText("Modalidade");

        labelApresentacao.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        labelApresentacao.setText("Apresentação");

        comboBoxConcentracao.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        comboBoxConcentracao.setSelectedItem("Selecione uma concentração");

        comboBoxModalidade.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        comboBoxModalidade.setSelectedItem("Selecione uma modalidade");

        comboBoxApresentacao.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        comboBoxApresentacao.setSelectedItem("Selecione uma Apresentação");

        btSalvar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btSalvar.setText("SALVAR");
        btSalvar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSalvar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btSalvarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btSalvarMouseExited(evt);
            }
        });
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });

        btCancelar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btCancelar.setText("CANCELAR");
        btCancelar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btCancelarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btCancelarMouseExited(evt);
            }
        });
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });

        tfDCB.setColumns(20);
        tfDCB.setRows(5);
        jScrollPane1.setViewportView(tfDCB);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(42, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDCB2, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(comboBoxApresentacao, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboBoxModalidade, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(73, 73, 73)
                        .addComponent(btSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(comboBoxConcentracao, 0, 314, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addGap(157, 157, 157))
            .addComponent(labelCadastroMedicamento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(labelCadastroMedicamento, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelDCB2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        btCancelar.setEnabled(false);
        PrincipalPanel.showMedHome();
        btCancelar.setEnabled(true);
    }//GEN-LAST:event_btCancelarActionPerformed
    
    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
       btSalvar.setEnabled(false);
        if(tfDCB.getText().isEmpty() || tfDCB.getText().length()<=0 ||
               comboBoxApresentacao.getSelectedIndex() == -1 ||
                comboBoxModalidade.getSelectedIndex() == -1 ||
               comboBoxConcentracao.getSelectedIndex() == -1){
           JOptionPane.showMessageDialog(getParent(),"Preencha os campos corretamente","AVISO",JOptionPane.WARNING_MESSAGE );
           btSalvar.setEnabled(true);
       
       }else{
            
            String denominacaoComumBrasileira = tfDCB.getText().toString().trim();
            Modalidade modalidade = (Modalidade) comboBoxModalidade.getSelectedItem();
            Apresentacao apresentacao = (Apresentacao) comboBoxApresentacao.getSelectedItem();
            Concentracao concetracao = (Concentracao) comboBoxConcentracao.getSelectedItem();
            
      
           try {
                Medicamento novoMedicamento = new Medicamento(denominacaoComumBrasileira,apresentacao,concetracao,modalidade);
                int id =MedicamentoDAO.insertMedicamento(novoMedicamento);
                novoMedicamento.setId(id);
                JOptionPane.showMessageDialog(getParent(),"Medicamento Cadastrado","AVISO",JOptionPane.INFORMATION_MESSAGE );
                tfDCB.setText("");
                comboBoxApresentacao.setSelectedIndex(-1);
                comboBoxConcentracao.setSelectedIndex(-1);
                comboBoxModalidade.setSelectedIndex(-1);
               
                Object[] options = {"Retornar","Realizar Entrada"};
                         int resposta = JOptionPane.showOptionDialog(getParent(),
                             "Deseja retornar para a lista de medicamento ou realizar entrada neste?",
                             "O que deseja fazer?",
                             JOptionPane.DEFAULT_OPTION,
                             JOptionPane.QUESTION_MESSAGE,null,options,null);
                if(resposta == 0)
                    PrincipalPanel.showMedTabelaEntrada();
                else if(resposta ==1)
                    PrincipalPanel.showMedPrimeiraEntrada(novoMedicamento); 
                btSalvar.setEnabled(true);
           } catch (SQLException ex) {
               System.out.println(ex.getErrorCode() +":"+ex.getMessage());
           }
       
       }
       btSalvar.setEnabled(true);
       
    }//GEN-LAST:event_btSalvarActionPerformed

    private void btCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btCancelarMouseEntered
        btCancelar.setBackground(Color.RED);
        btCancelar.setForeground(Color.WHITE);
    }//GEN-LAST:event_btCancelarMouseEntered

    private void btCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btCancelarMouseExited
        btCancelar.setBackground(new Color(240,240,240));
        btCancelar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btCancelarMouseExited

    private void btSalvarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btSalvarMouseEntered
        btSalvar.setBackground(new Color(0,153,153));
        btSalvar.setForeground(Color.WHITE);
    }//GEN-LAST:event_btSalvarMouseEntered

    private void btSalvarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btSalvarMouseExited
        btSalvar.setBackground(new Color(240,240,240));
        btSalvar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btSalvarMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCancelar;
    private javax.swing.JButton btSalvar;
    private javax.swing.JComboBox<Apresentacao> comboBoxApresentacao;
    private javax.swing.JComboBox<Concentracao> comboBoxConcentracao;
    private javax.swing.JComboBox<Modalidade> comboBoxModalidade;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelApresentacao;
    private javax.swing.JLabel labelCadastroMedicamento;
    private javax.swing.JLabel labelConcentracao;
    private javax.swing.JLabel labelDCB2;
    private javax.swing.JLabel labelModalidade;
    private javax.swing.JTextArea tfDCB;
    // End of variables declaration//GEN-END:variables

    void atualizaJBox() {
        iniciarComboBoxApresentacao();
        iniciarComboBoxConcentracao();
        iniciarComboBoxModalidade();
    }

    
}
