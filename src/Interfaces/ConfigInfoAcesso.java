package Interfaces;

import DAO.UsuarioDAO;
import Dominio.Usuario;
import java.awt.Color;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ConfigInfoAcesso extends javax.swing.JPanel {
    private Usuario user;
    
    public ConfigInfoAcesso() {
        initComponents();
        btSalvar.setVisible(false);
        btCancelar.setVisible(false);
        labelConfirmaSenha.setVisible(false);
        pfConfirmarSenha.setVisible(false);
        pfSenha.setEditable(false);
        tfLogin.setEditable(false);
        
        try{
            user = UsuarioDAO.selectUsuario();
            tfLogin.setText(user.getNome());
            pfSenha.setText(user.getSenha());
        } catch (SQLException ex) {
            System.out.println(ex.getErrorCode()+":"+ex.getMessage());
        }
    }
    protected void atualizaDados(){
        try{
            user = UsuarioDAO.selectUsuario();
            tfLogin.setText(user.getNome());
            pfSenha.setText(user.getSenha());
        } catch (SQLException ex) {
            System.out.println(ex.getErrorCode()+":"+ex.getMessage());
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelInfoAcesso = new javax.swing.JLabel();
        labelLogin = new javax.swing.JLabel();
        labelSenha = new javax.swing.JLabel();
        tfLogin = new javax.swing.JTextField();
        btEditar = new javax.swing.JButton();
        btSalvar = new javax.swing.JButton();
        pfSenha = new javax.swing.JPasswordField();
        pfConfirmarSenha = new javax.swing.JPasswordField();
        labelConfirmaSenha = new javax.swing.JLabel();
        btCancelar = new javax.swing.JButton();

        setOpaque(false);

        labelInfoAcesso.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        labelInfoAcesso.setForeground(new java.awt.Color(255, 255, 255));
        labelInfoAcesso.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelInfoAcesso.setText("Informações de Acesso");

        labelLogin.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        labelLogin.setForeground(new java.awt.Color(255, 255, 255));
        labelLogin.setText("Login");

        labelSenha.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        labelSenha.setForeground(new java.awt.Color(255, 255, 255));
        labelSenha.setText("Senha");

        tfLogin.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        btEditar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btEditar.setText("EDITAR");
        btEditar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btEditar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btEditarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btEditarMouseExited(evt);
            }
        });
        btEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEditarActionPerformed(evt);
            }
        });

        btSalvar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btSalvar.setText("SALVAR");
        btSalvar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSalvar.setEnabled(false);
        btSalvar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btSalvarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btSalvarMouseExited(evt);
            }
        });
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });

        pfSenha.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        pfConfirmarSenha.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        labelConfirmaSenha.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        labelConfirmaSenha.setForeground(new java.awt.Color(255, 255, 255));
        labelConfirmaSenha.setText("Confirmar Senha");

        btCancelar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btCancelar.setText("CANCELAR");
        btCancelar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btCancelarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btCancelarMouseExited(evt);
            }
        });
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelInfoAcesso, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(labelSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(labelConfirmaSenha)
                                .addComponent(labelLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(tfLogin, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                                .addComponent(pfSenha)
                                .addComponent(pfConfirmarSenha, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(btCancelar)
                                    .addGap(42, 42, 42)
                                    .addComponent(btEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(134, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(labelInfoAcesso)
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(labelLogin, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                    .addComponent(tfLogin))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pfSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pfConfirmarSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelConfirmaSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(169, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEditarActionPerformed
        btSalvar.setVisible(true);
        btSalvar.setEnabled(true);
        btEditar.setVisible(false);
        btCancelar.setVisible(true);
        labelConfirmaSenha.setVisible(true);
        pfConfirmarSenha.setVisible(true);
        pfConfirmarSenha.setText("");
        pfSenha.setEditable(true);
        pfSenha.setText("");
        tfLogin.setEditable(true);
        
    }//GEN-LAST:event_btEditarActionPerformed

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        btEditar.setVisible(true);
        btSalvar.setVisible(false);
        btCancelar.setVisible(false);
        labelConfirmaSenha.setVisible(false);
        pfConfirmarSenha.setVisible(false);
        pfSenha.setEditable(false);
        tfLogin.setEditable(false);
        tfLogin.setText(user.getNome());
        pfSenha.setText(user.getSenha());
        pfConfirmarSenha.setText("");
    }//GEN-LAST:event_btCancelarActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        String senha = new String(pfSenha.getPassword());
        String confirmaSenha = new String(pfConfirmarSenha.getPassword());
        if(tfLogin.getText().isEmpty() || pfSenha.getPassword().toString().isEmpty() || pfConfirmarSenha.getPassword().toString().isEmpty()){
            JOptionPane.showMessageDialog(getParent(),"Preencha os campos corretamente","AVISO",JOptionPane.WARNING_MESSAGE );
        }else if(!(senha.equals(confirmaSenha))){
            JOptionPane.showMessageDialog(getParent(),"Senhas nao coincidem","AVISO",JOptionPane.WARNING_MESSAGE );
        }else{
           System.out.println(senha);
           user.setNome(tfLogin.getText().trim());
           user.setSenha(senha);
           JOptionPane.showMessageDialog(getParent(),"Informações atualizadas","AVISO",JOptionPane.INFORMATION_MESSAGE );
           
            btCancelarActionPerformed(evt);
            try {
                UsuarioDAO.updateInfo(user);
            } catch (SQLException ex) {
                System.out.println(ex.getErrorCode()+":"+ex.getMessage());
            }
        }
    }//GEN-LAST:event_btSalvarActionPerformed

    private void btCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btCancelarMouseEntered
        btCancelar.setBackground(Color.RED);
        btCancelar.setForeground(Color.WHITE);
    }//GEN-LAST:event_btCancelarMouseEntered

    private void btCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btCancelarMouseExited
        btCancelar.setBackground(new Color(240,240,240));
        btCancelar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btCancelarMouseExited

    private void btEditarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btEditarMouseEntered
        btEditar.setBackground(new Color(204,204,0));
        btEditar.setForeground(Color.WHITE);
    }//GEN-LAST:event_btEditarMouseEntered

    private void btEditarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btEditarMouseExited
        btEditar.setBackground(new Color(240,240,240));
        btEditar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btEditarMouseExited

    private void btSalvarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btSalvarMouseEntered
        btSalvar.setBackground(new Color(0,153,153));
        btSalvar.setForeground(Color.WHITE);
    }//GEN-LAST:event_btSalvarMouseEntered

    private void btSalvarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btSalvarMouseExited
        btSalvar.setBackground(new Color(240,240,240));
        btSalvar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btSalvarMouseExited

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCancelar;
    private javax.swing.JButton btEditar;
    private javax.swing.JButton btSalvar;
    private javax.swing.JLabel labelConfirmaSenha;
    private javax.swing.JLabel labelInfoAcesso;
    private javax.swing.JLabel labelLogin;
    private javax.swing.JLabel labelSenha;
    private javax.swing.JPasswordField pfConfirmarSenha;
    private javax.swing.JPasswordField pfSenha;
    private javax.swing.JTextField tfLogin;
    // End of variables declaration//GEN-END:variables
}
