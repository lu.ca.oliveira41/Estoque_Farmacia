package Interfaces;

import DAO.EntradaDAO;
import DAO.EstoqueDAO;
import DAO.EstoquePPSDAO;
import Dominio.Entrada;
import Dominio.Estoque;
import Dominio.EstoquePPS;
import Dominio.ProdutoParaSaude;

import java.awt.Color;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class PPSEntrada extends javax.swing.JPanel {
    private ProdutoParaSaude pps;
    private EstoquePPS estoquePPS=null;
    private boolean flagPrimeiraEntrada;
    private Calendar cal = Calendar.getInstance();
    private Calendar calVencimento = Calendar.getInstance();
    public PPSEntrada(EstoquePPS pps) {//false
        initComponents();
        this.flagPrimeiraEntrada = false;
        this.estoquePPS = pps;
        jTextDescricao.setEditable(false);
        jTextDescricao.setLineWrap(true);
        jTextDescricao.setWrapStyleWord(true);
        jTextUnidade.setEditable(false);
        jTextModalidade.setEditable(false);
        jTextLote.setEditable(false);
        calVencimento.setTime(pps.getDataValidade());
        jTextDescricao.setText(pps.getPps().getDescricao());
        jTextUnidade.setText(pps.getPps().getUnidade().toString());
        jTextModalidade.setText(pps.getPps().getModalidade().toString());
        jTextLote.setText(pps.getnLote());
        jFormattedDataValidade.setEditable(false);
        String dataEntrada;
        String dataValidade;
        if(cal.get(GregorianCalendar.DAY_OF_MONTH)<10)
            dataEntrada = "0"+cal.get(GregorianCalendar.DAY_OF_MONTH);
        else
            dataEntrada = ""+cal.get(GregorianCalendar.DAY_OF_MONTH);
        
        if(cal.get(GregorianCalendar.MONTH)>=9)
            dataEntrada +=(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        else
            dataEntrada +="0"+(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        
        jFormattedDataEntrada.setText(dataEntrada);
        
        if(calVencimento.get(GregorianCalendar.DAY_OF_MONTH)<10)
            dataValidade = "0"+calVencimento.get(GregorianCalendar.DAY_OF_MONTH);
        else
            dataValidade = ""+calVencimento.get(GregorianCalendar.DAY_OF_MONTH);
        
        if(calVencimento.get(GregorianCalendar.MONTH)>=9)
            dataValidade +=(calVencimento.get(GregorianCalendar.MONTH)+1)+""+calVencimento.get(GregorianCalendar.YEAR);
        else
            dataValidade +="0"+(calVencimento.get(GregorianCalendar.MONTH)+1)+""+calVencimento.get(GregorianCalendar.YEAR);
        
        jFormattedDataValidade.setText(dataValidade);
    }
    public PPSEntrada(ProdutoParaSaude produtoParaSaude){//true
        initComponents();
        this.flagPrimeiraEntrada = true;
        this.pps = produtoParaSaude;
        jTextDescricao.setEditable(false);
        jTextDescricao.setLineWrap(true);
        jTextDescricao.setWrapStyleWord(true);
        jTextUnidade.setEditable(false);
        jTextModalidade.setEditable(false);
        
        jTextDescricao.setText(produtoParaSaude.getDescricao());
        jTextUnidade.setText(produtoParaSaude.getUnidade().toString());
        jTextModalidade.setText(produtoParaSaude.getModalidade().toString());
        String dataEntrada;
        if(cal.get(GregorianCalendar.DAY_OF_MONTH)<10)
            dataEntrada = "0"+cal.get(GregorianCalendar.DAY_OF_MONTH);
        else
            dataEntrada = ""+cal.get(GregorianCalendar.DAY_OF_MONTH);
        
        if(cal.get(GregorianCalendar.MONTH)>=9)
            dataEntrada +=(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        else
            dataEntrada +="0"+(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        
        jFormattedDataEntrada.setText(dataEntrada);
       
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSpinnerQtd = new javax.swing.JSpinner();
        jTextLote = new javax.swing.JTextField();
        jButtonCancelar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jTextUnidade = new javax.swing.JTextField();
        jTextModalidade = new javax.swing.JTextField();
        jFormattedDataValidade = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        jFormattedDataEntrada = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextDescricao = new javax.swing.JTextArea();

        setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("ENTRADA EM ESTOQUE");

        jLabel2.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel2.setText("Descrição");

        jLabel3.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel3.setText("Unidade");

        jLabel4.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel4.setText("Modalidade");

        jLabel5.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel5.setText("Quantidade");

        jLabel6.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel6.setText("Lote");

        jSpinnerQtd.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jTextLote.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jButtonCancelar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jButtonCancelar.setText("CANCELAR");
        jButtonCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseExited(evt);
            }
        });
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jButtonSalvar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jButtonSalvar.setText("SALVAR");
        jButtonSalvar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonSalvarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonSalvarMouseExited(evt);
            }
        });
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jTextUnidade.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jTextModalidade.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        try {
            jFormattedDataValidade.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataValidade.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataValidade.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel7.setText("Data de Validade");

        try {
            jFormattedDataEntrada.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataEntrada.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataEntrada.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel8.setText("Data de Entrada");

        jTextDescricao.setColumns(20);
        jTextDescricao.setRows(5);
        jScrollPane1.setViewportView(jTextDescricao);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(jButtonCancelar)
                            .addGap(98, 98, 98)
                            .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(30, 30, 30))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addComponent(jFormattedDataEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6))
                                .addGap(28, 28, 28)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane1)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jSpinnerQtd, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel7)
                                            .addGap(18, 18, 18)
                                            .addComponent(jFormattedDataValidade, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jTextLote)
                                        .addComponent(jTextUnidade)
                                        .addComponent(jTextModalidade))))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(43, 43, 43)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(117, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextLote, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jFormattedDataValidade, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jSpinnerQtd, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jFormattedDataEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        jButtonCancelar.setEnabled(false);
        PrincipalPanel.showPPSHome();
        jButtonCancelar.setEnabled(true);
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        jButtonSalvar.setEnabled(false);
        int qtd = Integer.parseInt(jSpinnerQtd.getValue().toString());
        String lote = jTextLote.getText().toString().trim();
        String dataValidade = jFormattedDataValidade.getText().toString();
        String dataEntrada = jFormattedDataEntrada.getText().toString();
        Calendar calendarEntrada = Calendar.getInstance();
        Calendar calendarValidade = Calendar.getInstance();
        Calendar horario = Calendar.getInstance();
        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            calendarEntrada.setTime(sdf.parse(dataEntrada));
            calendarEntrada.set(Calendar.HOUR_OF_DAY,horario.get(Calendar.HOUR_OF_DAY));
            calendarEntrada.set(Calendar.MINUTE,horario.get(Calendar.MINUTE));
            calendarEntrada.set(Calendar.SECOND,horario.get(Calendar.SECOND));
            calendarValidade.setTime(sdf.parse(dataValidade));
        } catch (ParseException ex) {
           System.out.println("Problema ao formatar data");
        }
        if(qtd <= 0 || lote.equals("") || lote.length()<=0 ||lote.isEmpty()){
            JOptionPane.showMessageDialog(getParent(),"Preencha os Campos Corretamente!","ERRO",JOptionPane.WARNING_MESSAGE);  
            jButtonSalvar.setEnabled(true);
        } else{
            try {
                if(flagPrimeiraEntrada && EstoquePPSDAO.selectEstoquePPSByLote(lote)==null){
                    try {
                        Estoque estoque = new Estoque(0, calendarValidade.getTime());
                        int idEstoque = EstoqueDAO.insertEstoque(estoque);
                        estoque.setIdEstoque(idEstoque);
                        
                        Entrada novaEntrada = new Entrada(qtd, calendarEntrada.getTime(), estoque);
                        EntradaDAO.insertEntrada(novaEntrada);
                        
                        EstoquePPS estoquePPS = new EstoquePPS(lote, pps, estoque);
                        EstoquePPSDAO.insertEstoquePPS(estoquePPS);
                        JOptionPane.showMessageDialog(getParent(), "Entrada registrada com sucesso",null,JOptionPane.INFORMATION_MESSAGE);
                        PrincipalPanel.showPPSHome();
                        jButtonSalvar.setEnabled(true);
                    } catch (SQLException ex) {
                        Logger.getLogger(PPSEntrada.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    try{
                        if(estoquePPS==null)
                            estoquePPS=EstoquePPSDAO.selectEstoquePPSByLote(lote);
                        Estoque estoque = new Estoque(estoquePPS.getId(),estoquePPS.getQuantidade(),estoquePPS.getDataValidade());
                        Entrada novaEntrada = new Entrada(qtd, calendarEntrada.getTime(), estoque);
                        EntradaDAO.insertEntrada(novaEntrada);
                        JOptionPane.showMessageDialog(getParent(), "Entrada registrada com sucesso",null,JOptionPane.INFORMATION_MESSAGE);
                        PrincipalPanel.showPPSHome();
                        jButtonSalvar.setEnabled(true);
                    }catch (SQLException ex) {
                        JOptionPane.showMessageDialog(getParent(), "Ocorreu um erro na solicitacao","ERRO",JOptionPane.WARNING_MESSAGE);
                        Logger.getLogger(PPSEntrada.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(PPSEntrada.class.getName()).log(Level.SEVERE, null, ex);
            }
            jButtonSalvar.setEnabled(true);
            
        }
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseEntered
        jButtonCancelar.setBackground(Color.RED);
        jButtonCancelar.setForeground(Color.WHITE);
    }//GEN-LAST:event_jButtonCancelarMouseEntered

    private void jButtonCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseExited
        jButtonCancelar.setBackground(new Color(240,240,240));
        jButtonCancelar.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonCancelarMouseExited

    private void jButtonSalvarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSalvarMouseEntered
        jButtonSalvar.setBackground(new Color(0,153,153));
        jButtonSalvar.setForeground(Color.WHITE);
    }//GEN-LAST:event_jButtonSalvarMouseEntered

    private void jButtonSalvarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSalvarMouseExited
        jButtonSalvar.setBackground(new Color(240,240,240));
        jButtonSalvar.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonSalvarMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JFormattedTextField jFormattedDataEntrada;
    private javax.swing.JFormattedTextField jFormattedDataValidade;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jSpinnerQtd;
    private javax.swing.JTextArea jTextDescricao;
    private javax.swing.JTextField jTextLote;
    private javax.swing.JTextField jTextModalidade;
    private javax.swing.JTextField jTextUnidade;
    // End of variables declaration//GEN-END:variables
}
