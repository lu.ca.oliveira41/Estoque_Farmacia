package Interfaces;

import DAO.ApresentacaoDAO;
import Dominio.Apresentacao;
import Utils.ModelTable;
import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ConfigGerenciarApresentacao extends javax.swing.JPanel {
    List<Apresentacao> apresentacoes = new ArrayList<>();
    public ConfigGerenciarApresentacao() {
        initComponents();
        iniciarTabelaApresentacao();
    }
    public void iniciarTabelaApresentacao(){
        tabelaApresentacoes.removeAll();
        try {
           apresentacoes = ApresentacaoDAO.selectApresentacao();
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode()+":"+ex.getMessage());
        }
        String[] colunas = {"CÓDIGO","TIPO DE APRESENTAÇÃO"};
        ArrayList dados = new ArrayList();
        for(Apresentacao apresentacao:apresentacoes){
            dados.add(new Object[]{
                apresentacao.getId(),
                apresentacao.getApresentacao()
            });
        }
        ModelTable model = new ModelTable(dados, colunas);
        tabelaApresentacoes.setModel(model);
        tabelaApresentacoes.getColumnModel().getColumn(0).setMaxWidth(100);
        tabelaApresentacoes.getTableHeader().setResizingAllowed(false);
        tabelaApresentacoes.getTableHeader().setReorderingAllowed(false);

        tabelaApresentacoes.getSelectionModel().addListSelectionListener(selecionaLinha());
    }
    private ListSelectionListener selecionaLinha() {
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tabelaApresentacoes.getSelectedRow() != -1){
                    int cod = (int) tabelaApresentacoes.getValueAt(tabelaApresentacoes.getSelectedRow(), 0);
                    try {
                        Apresentacao apresentacao = ApresentacaoDAO.selectApresentacaoByID(cod);
                        Object[] options = {"Editar","Excluir"};
                        int resposta = JOptionPane.showOptionDialog(getParent(),
                            "Apresentacao: "+apresentacao.getApresentacao(),
                            "O que deseja fazer?",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
                    if(resposta == 0)
                       editarInfo(apresentacao); 
                    else if(resposta == 1)
                        confirmaExclusao(cod);
                    tabelaApresentacoes.clearSelection();
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                    
                }
            }
        };
    }
    private void confirmaExclusao(int cod){
       Object[] options = {"Confirmar","Cancelar"};
       int resposta = JOptionPane.showOptionDialog(getParent(), "Deseja realmente excluir?",
               null,
               JOptionPane.DEFAULT_OPTION,
               JOptionPane.QUESTION_MESSAGE,null,options,null);
       if(resposta == 0)
        try {
            ApresentacaoDAO.delete(cod);
            revalida();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(getParent(), "Não é possivel excluir o registro ",
                    "Erro de exclusão",JOptionPane.WARNING_MESSAGE);
        }
           
    }
    private void editarInfo(Apresentacao apresentacao){
        String novoNome = null;
        boolean result = false;
        while(novoNome == null || novoNome.equals("") ){
            novoNome =  JOptionPane.showInputDialog(getParent(), 
                            "Editar Informação da Apresentacao: "+apresentacao.getApresentacao(),
                            "Novo nome para a apresentacao:",
                            JOptionPane.PLAIN_MESSAGE);
            if(novoNome != null )
                if(novoNome.equals("") ){
                    JOptionPane.showMessageDialog(getParent(), "Insira um nome valido",
                            "Erro ao atualizar dados",JOptionPane.ERROR_MESSAGE);
                }else{
                    try{
                        apresentacao.setApresentacao(novoNome);
                        result =ApresentacaoDAO.updateApresentacaoByid(apresentacao);
                        revalida();
                        JOptionPane.showMessageDialog(getParent(), "Atualização realizada com sucesso",
                            "",JOptionPane.INFORMATION_MESSAGE);
                        break;
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                }
            else
                break;
        }
        
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelApresentacao = new javax.swing.JLabel();
        btNovaApresentacao = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaApresentacoes = new javax.swing.JTable();

        setOpaque(false);

        labelApresentacao.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        labelApresentacao.setForeground(new java.awt.Color(255, 255, 255));
        labelApresentacao.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelApresentacao.setText("Apresentações Registradas");

        btNovaApresentacao.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btNovaApresentacao.setText("Nova Apresentação");
        btNovaApresentacao.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btNovaApresentacao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btNovaApresentacaoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btNovaApresentacaoMouseExited(evt);
            }
        });
        btNovaApresentacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNovaApresentacaoActionPerformed(evt);
            }
        });

        tabelaApresentacoes.setBackground(new java.awt.Color(0, 153, 153));
        tabelaApresentacoes.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tabelaApresentacoes.setForeground(new java.awt.Color(255, 255, 255));
        tabelaApresentacoes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelaApresentacoes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jScrollPane1.setViewportView(tabelaApresentacoes);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(158, 158, 158)
                .addComponent(labelApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(btNovaApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btNovaApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btNovaApresentacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNovaApresentacaoActionPerformed
        String novoNome = null;
        boolean result = false;
        while(novoNome == null || novoNome.equals("") ){
            novoNome =  JOptionPane.showInputDialog(getParent(), "Nome da nova Apresentacao:",
                "Cadastrar nova apresentacao",
                JOptionPane.PLAIN_MESSAGE);

            if(novoNome != null)
                if(novoNome.equals("") ){

                    JOptionPane.showMessageDialog(getParent(), "Insira um nome valido",
                        "Erro ao cadastrar novos dados",JOptionPane.ERROR_MESSAGE);
                }else{
                    try{
                        Apresentacao apresentacao = new Apresentacao(novoNome);
                        result = ApresentacaoDAO.insertApresentacao(apresentacao);revalida();
                        JOptionPane.showMessageDialog(getParent(), "Cadastro realizado com sucesso",
                            "",JOptionPane.INFORMATION_MESSAGE);
                        break;
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                }
            else{
                break;
            }
        }
       
    }//GEN-LAST:event_btNovaApresentacaoActionPerformed

    private void btNovaApresentacaoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btNovaApresentacaoMouseEntered
        btNovaApresentacao.setBackground(new Color(0,153,153));
        btNovaApresentacao.setForeground(Color.WHITE);
    }//GEN-LAST:event_btNovaApresentacaoMouseEntered

    private void btNovaApresentacaoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btNovaApresentacaoMouseExited
        btNovaApresentacao.setBackground(new Color(240,240,240));
        btNovaApresentacao.setForeground(Color.BLACK);
    }//GEN-LAST:event_btNovaApresentacaoMouseExited
    public void revalida(){
        iniciarTabelaApresentacao();
        tabelaApresentacoes.repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btNovaApresentacao;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelApresentacao;
    private javax.swing.JTable tabelaApresentacoes;
    // End of variables declaration//GEN-END:variables
}
