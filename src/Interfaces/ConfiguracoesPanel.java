package Interfaces;

import Conexao.ConexaoBanco;
import DAO.UsuarioDAO;
import Dominio.Usuario;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.hsqldb.lib.tar.TarMalformatException;

public class ConfiguracoesPanel extends javax.swing.JPanel {
    public ConfigGerenciarUnidade panelConfigGerenciarUnidade = new ConfigGerenciarUnidade();
    public ConfigGerenciarConcentracao panelConfigGerenciarConcentracao = new ConfigGerenciarConcentracao();
    public ConfigGerenciarModalidade panelConfigGerenciarModalidade = new ConfigGerenciarModalidade();
    public ConfigGerenciarApresentacao panelConfigGerenciarApresentacao = new ConfigGerenciarApresentacao();
    public ConfigInfoAcesso panelConfigInfoAcesso = new ConfigInfoAcesso();
    public ConfiguracoesPanel() {
        initComponents();
        inicializaPanelConfig();
        BackgroundPrincipal bg = new BackgroundPrincipal();
        this.add(bg, BorderLayout.CENTER);
    }
    private void inicializaPanelConfig(){
        
        
        panelConteudo.add(panelConfigInfoAcesso,"panel infoAcesso");
        panelConteudo.add(panelConfigGerenciarUnidade,"panel gerenciaUnidade");
        panelConteudo.add(panelConfigGerenciarConcentracao,"panel gerenciaConcentracao");
        panelConteudo.add(panelConfigGerenciarModalidade,"panel gerenciaModalidade");
        panelConteudo.add(panelConfigGerenciarApresentacao,"panel gerenciaApresentacao");
        CardLayout card = (CardLayout) panelConteudo.getLayout();
        card.show(panelConteudo, "panel infoAcesso");
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btVoltar = new javax.swing.JButton();
        btInfoAcesso = new javax.swing.JButton();
        btUnidade = new javax.swing.JButton();
        btConcentracao = new javax.swing.JButton();
        btModalidade = new javax.swing.JButton();
        btApresentacao = new javax.swing.JButton();
        panelConteudo = new javax.swing.JPanel();
        btBackUp = new javax.swing.JButton();
        btRestauraBackUp = new javax.swing.JButton();

        btVoltar.setForeground(java.awt.SystemColor.window);
        btVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/voltar.png"))); // NOI18N
        btVoltar.setBorder(null);
        btVoltar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btVoltarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btVoltarMouseExited(evt);
            }
        });
        btVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVoltarActionPerformed(evt);
            }
        });

        btInfoAcesso.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btInfoAcesso.setText("Informações de Acesso");
        btInfoAcesso.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btInfoAcesso.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btInfoAcessoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btInfoAcessoMouseExited(evt);
            }
        });
        btInfoAcesso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btInfoAcessoActionPerformed(evt);
            }
        });

        btUnidade.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btUnidade.setText("Gerenciar Unidade");
        btUnidade.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btUnidade.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btUnidadeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btUnidadeMouseExited(evt);
            }
        });
        btUnidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUnidadeActionPerformed(evt);
            }
        });

        btConcentracao.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btConcentracao.setText("Gerenciar Concentracão");
        btConcentracao.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btConcentracao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btConcentracaoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btConcentracaoMouseExited(evt);
            }
        });
        btConcentracao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btConcentracaoActionPerformed(evt);
            }
        });

        btModalidade.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btModalidade.setText("Gerenciar Modalidade");
        btModalidade.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btModalidade.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btModalidadeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btModalidadeMouseExited(evt);
            }
        });
        btModalidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btModalidadeActionPerformed(evt);
            }
        });

        btApresentacao.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btApresentacao.setText("Gerenciar Apresentacao");
        btApresentacao.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btApresentacao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btApresentacaoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btApresentacaoMouseExited(evt);
            }
        });
        btApresentacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btApresentacaoActionPerformed(evt);
            }
        });

        panelConteudo.setOpaque(false);
        panelConteudo.setLayout(new java.awt.CardLayout());

        btBackUp.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btBackUp.setText("Realizar BackUp");
        btBackUp.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btBackUp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btBackUpMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btBackUpMouseExited(evt);
            }
        });
        btBackUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBackUpActionPerformed(evt);
            }
        });

        btRestauraBackUp.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btRestauraBackUp.setText("Restaurar via BackUp");
        btRestauraBackUp.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRestauraBackUp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btRestauraBackUpMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btRestauraBackUpMouseExited(evt);
            }
        });
        btRestauraBackUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRestauraBackUpActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btVoltar)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btInfoAcesso, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btBackUp, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btRestauraBackUp, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(50, 50, 50)
                        .addComponent(panelConteudo, javax.swing.GroupLayout.PREFERRED_SIZE, 680, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(btVoltar)
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btInfoAcesso, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btConcentracao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btBackUp, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btRestauraBackUp, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelConteudo, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btVoltarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btVoltarMouseEntered
        btVoltar.setBackground(new Color(0,153,153));
    }//GEN-LAST:event_btVoltarMouseEntered

    private void btVoltarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btVoltarMouseExited
        btVoltar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_btVoltarMouseExited

    private void btVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVoltarActionPerformed
        PrincipalFrame.showPanelPrincipal();
    }//GEN-LAST:event_btVoltarActionPerformed

    private void btInfoAcessoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btInfoAcessoMouseEntered
        btInfoAcesso.setBackground(new Color(0,153,153));
        btInfoAcesso.setForeground(Color.WHITE);
    }//GEN-LAST:event_btInfoAcessoMouseEntered

    private void btInfoAcessoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btInfoAcessoMouseExited
        btInfoAcesso.setBackground(new Color(240,240,240));
        btInfoAcesso.setForeground(Color.BLACK);
    }//GEN-LAST:event_btInfoAcessoMouseExited

    private void btInfoAcessoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btInfoAcessoActionPerformed
        CardLayout card = (CardLayout) panelConteudo.getLayout();
        card.show(panelConteudo, "panel infoAcesso");
    }//GEN-LAST:event_btInfoAcessoActionPerformed

    private void btUnidadeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btUnidadeMouseEntered
        btUnidade.setBackground(new Color(0,153,153));
        btUnidade.setForeground(Color.WHITE);
    }//GEN-LAST:event_btUnidadeMouseEntered

    private void btUnidadeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btUnidadeMouseExited
        btUnidade.setBackground(new Color(240,240,240));
        btUnidade.setForeground(Color.BLACK);
    }//GEN-LAST:event_btUnidadeMouseExited

    private void btUnidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUnidadeActionPerformed
        CardLayout card = (CardLayout) panelConteudo.getLayout();
        card.show(panelConteudo, "panel gerenciaUnidade");
    }//GEN-LAST:event_btUnidadeActionPerformed

    private void btConcentracaoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btConcentracaoMouseEntered
        btConcentracao.setBackground(new Color(0,153,153));
        btConcentracao.setForeground(Color.WHITE);
    }//GEN-LAST:event_btConcentracaoMouseEntered

    private void btConcentracaoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btConcentracaoMouseExited
        btConcentracao.setBackground(new Color(240,240,240));
        btConcentracao.setForeground(Color.BLACK);
    }//GEN-LAST:event_btConcentracaoMouseExited

    private void btConcentracaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btConcentracaoActionPerformed
        CardLayout card = (CardLayout) panelConteudo.getLayout();
        card.show(panelConteudo, "panel gerenciaConcentracao");
    }//GEN-LAST:event_btConcentracaoActionPerformed

    private void btModalidadeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btModalidadeMouseEntered
        btModalidade.setBackground(new Color(0,153,153));
        btModalidade.setForeground(Color.WHITE);
    }//GEN-LAST:event_btModalidadeMouseEntered

    private void btModalidadeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btModalidadeMouseExited
        btModalidade.setBackground(new Color(240,240,240));
        btModalidade.setForeground(Color.BLACK);
    }//GEN-LAST:event_btModalidadeMouseExited

    private void btModalidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btModalidadeActionPerformed
        CardLayout card = (CardLayout) panelConteudo.getLayout();
        card.show(panelConteudo, "panel gerenciaModalidade");
    }//GEN-LAST:event_btModalidadeActionPerformed

    private void btApresentacaoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btApresentacaoMouseEntered
        btApresentacao.setBackground(new Color(0,153,153));
        btApresentacao.setForeground(Color.WHITE);
    }//GEN-LAST:event_btApresentacaoMouseEntered

    private void btApresentacaoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btApresentacaoMouseExited
        btApresentacao.setBackground(new Color(240,240,240));
        btApresentacao.setForeground(Color.BLACK);
    }//GEN-LAST:event_btApresentacaoMouseExited

    private void btApresentacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btApresentacaoActionPerformed
        CardLayout card = (CardLayout) panelConteudo.getLayout();
        card.show(panelConteudo, "panel gerenciaApresentacao");
    }//GEN-LAST:event_btApresentacaoActionPerformed

    private void btBackUpMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBackUpMouseEntered
        btBackUp.setBackground(new Color(0,153,153));
        btBackUp.setForeground(Color.WHITE);
    }//GEN-LAST:event_btBackUpMouseEntered

    private void btBackUpMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBackUpMouseExited
        btBackUp.setBackground(new Color(240,240,240));
        btBackUp.setForeground(Color.BLACK);
    }//GEN-LAST:event_btBackUpMouseExited

    private void btBackUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBackUpActionPerformed
        JFileChooser file = new JFileChooser(); 
        file.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        file.setDialogTitle("Selecione onde armazenar o arquivo de backup");
        int res = file.showOpenDialog(getParent());
        File caminho;
        if(res == JFileChooser.APPROVE_OPTION){
            caminho = file.getSelectedFile();
            ConexaoBanco.backupBanco(caminho.getAbsolutePath());
        }
        
        
    }//GEN-LAST:event_btBackUpActionPerformed

    private void btRestauraBackUpMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btRestauraBackUpMouseEntered
        btRestauraBackUp.setBackground(new Color(0,153,153));
        btRestauraBackUp.setForeground(Color.WHITE);
    }//GEN-LAST:event_btRestauraBackUpMouseEntered

    private void btRestauraBackUpMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btRestauraBackUpMouseExited
        btRestauraBackUp.setBackground(new Color(240,240,240));
        btRestauraBackUp.setForeground(Color.BLACK);
    }//GEN-LAST:event_btRestauraBackUpMouseExited

    private void btRestauraBackUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRestauraBackUpActionPerformed
        boolean flag = true;
        do{
            JFileChooser file = new JFileChooser();
            file.setDialogTitle("Selecione arquivo de backup");
            file.setFileFilter(new javax.swing.filechooser.FileFilter(){
                public boolean accept(File f){
                     return (f.getName().endsWith(".tar.gz"));
                }
                public String getDescription(){
                    return "";
                }
            });
            int res = file.showOpenDialog(getParent());
            boolean respostaBackUp;
            File caminho;
            if(res == JFileChooser.APPROVE_OPTION){
                Object[] options= {"Confirma","Cancela"};
                int resposta = JOptionPane.showOptionDialog(getParent(),
                        "<html>Uma vez restaurando, dados posteriores ao BackUp serão perdidos",
                        "O que deseja fazer?",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.QUESTION_MESSAGE,null,options,null);
                if(resposta == 1)
                    return;
                caminho = file.getSelectedFile();
                try {
                    respostaBackUp=ConexaoBanco.restaurarBackUp(caminho);

                    if(respostaBackUp){
                        JOptionPane.showMessageDialog(getParent(), 
                                "Dados restaurados"
                                ,"AVISO",JOptionPane.INFORMATION_MESSAGE);
                        PrincipalFrame.reiniciaSistema();
                        flag=false;
                    }else{
                        JOptionPane.showMessageDialog(getParent(), 
                                "Selecione um arquivo valido"
                                ,"AVISO",JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ConfiguracoesPanel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ConfiguracoesPanel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ConfiguracoesPanel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (TarMalformatException ex) {
                    Logger.getLogger(ConfiguracoesPanel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(ConfiguracoesPanel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(ConfiguracoesPanel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ConfiguracoesPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }else{
                flag=false;
                PrincipalFrame.reiniciarConexoes();
                            }
        }while(flag);
    }//GEN-LAST:event_btRestauraBackUpActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btApresentacao;
    private javax.swing.JButton btBackUp;
    private javax.swing.JButton btConcentracao;
    private javax.swing.JButton btInfoAcesso;
    private javax.swing.JButton btModalidade;
    private javax.swing.JButton btRestauraBackUp;
    private javax.swing.JButton btUnidade;
    private javax.swing.JButton btVoltar;
    private javax.swing.JPanel panelConteudo;
    // End of variables declaration//GEN-END:variables
}
