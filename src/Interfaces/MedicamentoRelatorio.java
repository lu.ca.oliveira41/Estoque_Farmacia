package Interfaces;

import DAO.EntradaDAO;
import DAO.EstoqueMedicamentoDAO;
import DAO.RelatorioDAO;
import DAO.SaidaDAO;
import Dominio.Entrada;
import Dominio.EstoqueMedicamento;
import Dominio.ItemRelatorioMed;
import Dominio.Saida;
import Interfaces.PrincipalPanel;
import Utils.ModelTable;
import Utils.VariableRowHeightRenderer;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import static javax.swing.text.StyleConstants.ALIGN_CENTER;


public class MedicamentoRelatorio extends javax.swing.JPanel {
    SimpleDateFormat in= new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat out = new SimpleDateFormat("dd/MM/yyyy");
    private ArrayList<ItemRelatorioMed> movimentacao_medicamentos = new ArrayList();
    private ArrayList dados ;
    Calendar calendarHoje = Calendar.getInstance();
    Calendar calendarInicio = Calendar.getInstance();
    
    String dataHoje;
    String dataInicio;
    public MedicamentoRelatorio() {
        initComponents();
        incializaTabelaRelatorio();
        
        defineData();
        btLimparPeriodo.setVisible(false);
    }
    protected void incializaTabelaRelatorio(){
        tabelaRelatorio.removeAll();
        defineData();
        try{
            movimentacao_medicamentos = RelatorioDAO.selectRelatorioMed(calendarInicio.getTime(),calendarHoje.getTime());
            constroiTabela(movimentacao_medicamentos);
        } catch (SQLException ex) {
            Logger.getLogger(MedicamentoHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    protected void atualizaTabelaRelatorio(){
        tabelaRelatorio.removeAll();
        capturaData();
        try{
            movimentacao_medicamentos = RelatorioDAO.selectRelatorioMed(calendarInicio.getTime(),calendarHoje.getTime());
            constroiTabela(movimentacao_medicamentos);
        } catch (SQLException ex) {
            Logger.getLogger(MedicamentoHome.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    protected void constroiTabela(ArrayList<ItemRelatorioMed> movimentacao_medicamentos){
        dados = new ArrayList();
        String[] colunas = {"","Tipo","Lote","Denominação Comum Brasileira","Apresentação","Concentração","Modalidade","Qtd","Data Operacao","Validade"};
        for(ItemRelatorioMed item: movimentacao_medicamentos){
                try {
                    String data = out.format(in.parse(item.getEntrada()!=null?
                            item.getEntrada().getData().toString():
                            item.getSaida().getData().toString()));
                    dados.add(new Object[]{
                        item.getEntrada()!=null?item.getEntrada().getCodEntrada():item.getSaida().getCodSaida(),
                        item.getTipo(),
                        item.getEstoquemed().getnLote(),
                        item.getEstoquemed().getMedicamento().getDenominacao(),
                        item.getEstoquemed().getMedicamento().getApresentacao(),
                        item.getEstoquemed().getMedicamento().getConcentracao(),
                        item.getEstoquemed().getMedicamento().getModalidade(),
                        item.getEntrada()!=null?item.getEntrada().getQuantidade():item.getSaida().getQuantidade(),
                        formatarData(item.getEntrada()!=null?item.getEntrada().getData():item.getSaida().getData()),
                        formatarData(item.getEstoquemed().getDataValidade())
                    });
                } catch (ParseException ex) {
                    Logger.getLogger(MedicamentoRelatorio.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            ModelTable model = new ModelTable(dados, colunas);
            tabelaRelatorio.setModel(model);
            
            tabelaRelatorio.getColumnModel().getColumn(0).setPreferredWidth(0);
            tabelaRelatorio.getColumnModel().getColumn(0).setMinWidth(0);
            tabelaRelatorio.getColumnModel().getColumn(0).setMaxWidth(0);
            
            tabelaRelatorio.getColumnModel().getColumn(1).setPreferredWidth(250);
            tabelaRelatorio.getColumnModel().getColumn(2).setPreferredWidth(200);
            tabelaRelatorio.getColumnModel().getColumn(3).setPreferredWidth(700);
            tabelaRelatorio.getColumnModel().getColumn(4).setPreferredWidth(400);
            tabelaRelatorio.getColumnModel().getColumn(5).setPreferredWidth(400);
            tabelaRelatorio.getColumnModel().getColumn(6).setPreferredWidth(400);
            tabelaRelatorio.getColumnModel().getColumn(7).setPreferredWidth(100);
            tabelaRelatorio.getColumnModel().getColumn(8).setPreferredWidth(400);
            tabelaRelatorio.getColumnModel().getColumn(9).setPreferredWidth(400);
            tabelaRelatorio.getColumnModel().getColumn(3).setCellRenderer( new VariableRowHeightRenderer(tabelaRelatorio.getColumnModel().getColumn(3).getMinWidth()));
            tabelaRelatorio.getTableHeader().setResizingAllowed(false);
            tabelaRelatorio.getTableHeader().setReorderingAllowed(false);
            
            //tabelaRelatorio.getSelectionModel().addListSelectionListener(selecionarLinha());
            tabelaRelatorio.repaint();
    }
    private ListSelectionListener selecionarLinha(){
       
        
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tabelaRelatorio.getSelectedRow() != -1){
                    int cod =  (int) tabelaRelatorio.getValueAt(tabelaRelatorio.getSelectedRow(), 0);
                    String tipo = (String) tabelaRelatorio.getValueAt(tabelaRelatorio.getSelectedRow(), 1);
                    try{
                        Entrada  entrada;
                        Saida saida;
                        
                        Object[] options = {"Editar","Cancelar"};
                            int resposta = JOptionPane.showOptionDialog(getParent(),
                            null,
                            "O que deseja fazer?",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
                        if(resposta == 0){
                            if(tipo == "Entrada"){
                                entrada =  EntradaDAO.selectEntradaByID(cod);
                                PrincipalPanel.showMedEditOp(entrada);
                            }
                            else{
                                saida = SaidaDAO.selectSaidaByID(cod);
                                PrincipalPanel.showMedEditOp(saida);
                            }
                            
                        }                      
                    }catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                    tabelaRelatorio.clearSelection();
                   
                    
                }
            }
        };
    }
    private String formatarData(Date data) throws ParseException{
        String dataTexto = new SimpleDateFormat("dd/MM/yyyy").format(data);
        return dataTexto;
        
    }
    private void capturaData(){
        dataHoje = jFormattedDataFim.getText().toString();
        dataInicio = jFormattedDataInicio.getText().toString();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            if(dataInicio.length()>0 && !dataInicio.isEmpty() && !dataInicio.equals("  /  /    ")){
                  this.calendarInicio.setTime(sdf.parse(dataInicio));
            }
            this.calendarHoje.setTime(sdf.parse(dataHoje));
            calendarHoje.set(Calendar.DAY_OF_MONTH,calendarHoje.get(Calendar.DAY_OF_MONTH)+1);
            
        } catch (ParseException ex) {
            System.out.println("Problema ao formatar data//"+calendarHoje+"//"+calendarInicio);
        }
        
    }
    private void defineData(){
        calendarHoje = Calendar.getInstance();
        if(calendarHoje.get(GregorianCalendar.DAY_OF_MONTH)<10)
            dataHoje = "0"+calendarHoje.get(GregorianCalendar.DAY_OF_MONTH);
        else
            dataHoje = ""+calendarHoje.get(GregorianCalendar.DAY_OF_MONTH);
        
        if(calendarHoje.get(GregorianCalendar.MONTH)>=9)
            dataHoje +=(calendarHoje.get(GregorianCalendar.MONTH)+1)+""+calendarHoje.get(GregorianCalendar.YEAR);
        else
            dataHoje +="0"+(calendarHoje.get(GregorianCalendar.MONTH)+1)+""+calendarHoje.get(GregorianCalendar.YEAR);
        jFormattedDataFim.setText(dataHoje);
        calendarHoje.set(Calendar.DAY_OF_MONTH,calendarHoje.get(Calendar.DAY_OF_MONTH)+1);
        calendarInicio.set(2000,0,1);
        jFormattedDataInicio.setText("");
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        btPDFmedicamento = new javax.swing.JButton();
        jFormattedDataInicio = new javax.swing.JFormattedTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaRelatorio = new javax.swing.JTable();
        jFormattedDataFim = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        btLimparPeriodo = new javax.swing.JButton();
        btBuscar = new javax.swing.JButton();

        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(750, 550));

        jPanel1.setOpaque(false);
        jPanel1.setPreferredSize(new java.awt.Dimension(750, 550));

        jLabel6.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel6.setText("De");

        btPDFmedicamento.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btPDFmedicamento.setText("Exportar Para PDF");
        btPDFmedicamento.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btPDFmedicamento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btPDFmedicamentoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btPDFmedicamentoMouseExited(evt);
            }
        });
        btPDFmedicamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btPDFmedicamentoActionPerformed(evt);
            }
        });

        try {
            jFormattedDataInicio.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataInicio.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataInicio.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        tabelaRelatorio.setBackground(new java.awt.Color(0, 153, 153));
        tabelaRelatorio.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tabelaRelatorio.setForeground(new java.awt.Color(255, 255, 255));
        tabelaRelatorio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelaRelatorio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jScrollPane1.setViewportView(tabelaRelatorio);

        try {
            jFormattedDataFim.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataFim.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataFim.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel7.setText("Até");

        btLimparPeriodo.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btLimparPeriodo.setText("Limpar Seleção");
        btLimparPeriodo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btLimparPeriodo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btLimparPeriodoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btLimparPeriodoMouseExited(evt);
            }
        });
        btLimparPeriodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimparPeriodoActionPerformed(evt);
            }
        });

        btBuscar.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btBuscar.setText("Buscar");
        btBuscar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btBuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btBuscarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btBuscarMouseExited(evt);
            }
        });
        btBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(jFormattedDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jFormattedDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btLimparPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 74, Short.MAX_VALUE)
                        .addComponent(btPDFmedicamento, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 730, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jFormattedDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btLimparPeriodo)
                        .addComponent(btBuscar))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btPDFmedicamento)
                        .addComponent(jFormattedDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 458, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btPDFmedicamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btPDFmedicamentoActionPerformed
        Calendar cal = Calendar.getInstance();
        String dataHoje;
        if(cal.get(GregorianCalendar.DAY_OF_MONTH)<10)
            dataHoje = "0"+cal.get(GregorianCalendar.DAY_OF_MONTH);
        else
            dataHoje = ""+cal.get(GregorianCalendar.DAY_OF_MONTH);
        
        if(cal.get(GregorianCalendar.MONTH)>=9)
            dataHoje +=(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        else
            dataHoje +="0"+(cal.get(GregorianCalendar.MONTH)+1)+""+cal.get(GregorianCalendar.YEAR);
        
        
        
            String caminho = pastaDestino();
            if(caminho != null){
                try {
                Document document = new Document();

                PdfWriter.getInstance(document, new FileOutputStream(caminho+"\\RelatorioMedicamento-"+dataHoje+".pdf"));
                document.open();
                document.setPageSize(PageSize.A4);

                Paragraph paragrafo1;
                paragrafo1 = new Paragraph("");
                document.add(paragrafo1);

                float[] columnWidths = {3,3,4,4,3,4,3,3,3};
                PdfPTable tabela1 = new PdfPTable(columnWidths);
                tabela1.setWidthPercentage(100);
                tabela1.getDefaultCell().setUseAscender(true);
                tabela1.getDefaultCell().setUseDescender(true);

                Font f = new Font(Font.FontFamily.COURIER, 6, Font.NORMAL, GrayColor.WHITE);
                PdfPCell cabecalho = new PdfPCell(new Paragraph("RELATÓRIO DE MOVIMENTAÇÃO DE MEDICAMENTOS",new Font(Font.FontFamily.COURIER, 11, Font.BOLD)));
                cabecalho.setHorizontalAlignment(ALIGN_CENTER);
                cabecalho.setPaddingBottom(10);
                cabecalho.setBorder(PdfPCell.NO_BORDER);
                cabecalho.setColspan(9);
                tabela1.addCell(cabecalho);

                PdfPCell cel1 = new PdfPCell(new Phrase("TIPO",f));
                cel1.setHorizontalAlignment(ALIGN_CENTER);
                cel1.setBackgroundColor(GrayColor.DARK_GRAY);
                cel1.setPaddingTop(5);
                cel1.setPaddingBottom(5);

                PdfPCell cel2 = new PdfPCell(new Phrase("LOTE",f));
                cel2.setHorizontalAlignment(ALIGN_CENTER);
                cel2.setBackgroundColor(GrayColor.DARK_GRAY);
                cel2.setPaddingTop(5);
                cel2.setPaddingBottom(5);

                PdfPCell cel3 = new PdfPCell(new Phrase("DENOMINAÇÃO",f));
                cel3.setHorizontalAlignment(ALIGN_CENTER);
                cel3.setBackgroundColor(GrayColor.DARK_GRAY);
                cel3.setPaddingTop(5);
                cel3.setPaddingBottom(5);

                PdfPCell cel4 = new PdfPCell(new Phrase("APRESENTAÇÃO",f));
                cel4.setHorizontalAlignment(ALIGN_CENTER);
                cel4.setBackgroundColor(GrayColor.DARK_GRAY);
                cel4.setPaddingTop(5);
                cel4.setPaddingBottom(5);

                PdfPCell cel5 = new PdfPCell(new Phrase("CONCENTRAÇÃO",f));
                cel5.setHorizontalAlignment(ALIGN_CENTER);
                cel5.setBackgroundColor(GrayColor.DARK_GRAY);
                cel5.setPaddingTop(5);
                cel5.setPaddingBottom(5);

                PdfPCell cel6 = new PdfPCell(new Phrase("MODALIDADE",f));
                cel6.setHorizontalAlignment(ALIGN_CENTER);
                cel6.setBackgroundColor(GrayColor.DARK_GRAY);
                cel6.setPaddingTop(5);
                cel6.setPaddingBottom(5);

                PdfPCell cel7 = new PdfPCell(new Phrase("QUANTIDADE",f));
                cel7.setHorizontalAlignment(ALIGN_CENTER);
                cel7.setBackgroundColor(GrayColor.DARK_GRAY);
                cel7.setPaddingTop(5);
                cel7.setPaddingBottom(5);

                PdfPCell cel8 = new PdfPCell(new Phrase("DATA OPERAÇÃO",f));
                cel8.setHorizontalAlignment(ALIGN_CENTER);
                cel8.setBackgroundColor(GrayColor.DARK_GRAY);
                cel8.setPaddingTop(5);
                cel8.setPaddingBottom(5);

                PdfPCell cel9 = new PdfPCell(new Phrase("VALIDADE",f));
                cel9.setHorizontalAlignment(ALIGN_CENTER);
                cel9.setBackgroundColor(GrayColor.DARK_GRAY);
                cel9.setPaddingTop(5);
                cel9.setPaddingBottom(5);

                tabela1.addCell(cel1);
                tabela1.addCell(cel2);
                tabela1.addCell(cel3);
                tabela1.addCell(cel4);
                tabela1.addCell(cel5);
                tabela1.addCell(cel6);
                tabela1.addCell(cel7);
                tabela1.addCell(cel8);
                tabela1.addCell(cel9);

                List<ItemRelatorioMed> med; 
                med = new ArrayList<>();
                med = movimentacao_medicamentos;

                tabela1.getDefaultCell().setHorizontalAlignment(ALIGN_CENTER);
                tabela1.getDefaultCell().setPadding(2);
                Font g = new Font(Font.FontFamily.COURIER,8,Font.NORMAL, GrayColor.BLACK);
                try {
                    for(ItemRelatorioMed medic:med){
                        tabela1.addCell(new Phrase(medic.getTipo(), g));
                        tabela1.addCell(new Phrase(medic.getEstoquemed().getnLote(), g));
                        tabela1.addCell(new Phrase(medic.getEstoquemed().getMedicamento().getDenominacao(), g));
                        tabela1.addCell(new Phrase(medic.getEstoquemed().getMedicamento().getApresentacao().getApresentacao(), g));
                        tabela1.addCell(new Phrase(medic.getEstoquemed().getMedicamento().getConcentracao().getConcentracao(), g));
                        tabela1.addCell(new Phrase(medic.getEstoquemed().getMedicamento().getModalidade().getModalidade(), g));
                        tabela1.addCell(new Phrase(Integer.toString(medic.getEntrada()!=null?medic.getEntrada().getQuantidade():medic.getSaida().getQuantidade()), g));
                        tabela1.addCell(new Phrase(formatarData(medic.getEntrada()!=null?medic.getEntrada().getData():medic.getSaida().getData()), g));

                        tabela1.addCell(new Phrase(formatarData(medic.getEstoquemed().getDataValidade()), g));
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(MedicamentoRelatorio.class.getName()).log(Level.SEVERE, null, ex);
                }
                document.add(tabela1);
                document.close();
            } catch (DocumentException ex) {
                Logger.getLogger(MedicamentoRelatorio.class.getName()).log(Level.SEVERE, null, ex);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MedicamentoRelatorio.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                Desktop.getDesktop().open(new File(caminho+"\\RelatorioMedicamento-"+dataHoje+".pdf"));
            } catch (IOException ex) {
                Logger.getLogger(MedicamentoRelatorio.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btPDFmedicamentoActionPerformed

    public String pastaDestino(){
        
            JFileChooser file = new JFileChooser(); 
            file.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int res = file.showOpenDialog(getParent());
            File caminho;
            if(res == JFileChooser.APPROVE_OPTION){
                caminho = file.getSelectedFile();
                
                System.out.println(caminho.getAbsoluteFile());
                return caminho.getAbsolutePath();
            }else{
               
               return null;
            }
        
        
    }
    private void btPDFmedicamentoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btPDFmedicamentoMouseExited
        btPDFmedicamento.setBackground(new Color(240,240,240));
        btPDFmedicamento.setForeground(Color.BLACK);
    }//GEN-LAST:event_btPDFmedicamentoMouseExited

    private void btPDFmedicamentoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btPDFmedicamentoMouseEntered
        btPDFmedicamento.setBackground(new Color(0,153,153));
        btPDFmedicamento.setForeground(Color.WHITE);
    }//GEN-LAST:event_btPDFmedicamentoMouseEntered

    private void btLimparPeriodoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btLimparPeriodoMouseEntered
        btLimparPeriodo.setBackground(new Color(0,153,153));
        btLimparPeriodo.setForeground(Color.WHITE);
    }//GEN-LAST:event_btLimparPeriodoMouseEntered

    private void btLimparPeriodoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btLimparPeriodoMouseExited
        btLimparPeriodo.setBackground(new Color(240,240,240));
        btLimparPeriodo.setForeground(Color.BLACK);
    }//GEN-LAST:event_btLimparPeriodoMouseExited

    private void btLimparPeriodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimparPeriodoActionPerformed
        defineData();
        incializaTabelaRelatorio();
        btLimparPeriodo.setVisible(false);
    }//GEN-LAST:event_btLimparPeriodoActionPerformed

    private void btBuscarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBuscarMouseEntered
        btBuscar.setBackground(new Color(0,153,153));
        btBuscar.setForeground(Color.WHITE);
    }//GEN-LAST:event_btBuscarMouseEntered

    private void btBuscarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBuscarMouseExited
        btBuscar.setBackground(new Color(240,240,240));
        btBuscar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btBuscarMouseExited

    private void btBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarActionPerformed
        atualizaTabelaRelatorio();
        btLimparPeriodo.setVisible(true);
    }//GEN-LAST:event_btBuscarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBuscar;
    private javax.swing.JButton btLimparPeriodo;
    private javax.swing.JButton btPDFmedicamento;
    private javax.swing.JFormattedTextField jFormattedDataFim;
    private javax.swing.JFormattedTextField jFormattedDataInicio;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaRelatorio;
    // End of variables declaration//GEN-END:variables
}
