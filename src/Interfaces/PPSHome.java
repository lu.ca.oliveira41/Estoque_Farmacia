
package Interfaces;

import DAO.EntradaDAO;
import DAO.EstoquePPSDAO;
import DAO.SaidaDAO;
import Dominio.Entrada;
import Dominio.EstoquePPS;
import Dominio.Saida;
import Utils.ModelTable;
import Utils.VariableRowHeightRenderer;
import java.awt.Color;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PPSHome extends javax.swing.JPanel {
    SimpleDateFormat in= new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat out = new SimpleDateFormat("dd/MM/yyyy");
    private List<EstoquePPS> estoque_pps = new ArrayList<>();
    private List<Entrada> entradas= new ArrayList<>();
    private List<Saida> saida= new ArrayList<>();
    private String[] colunas = {"ID","DESCRIÇÃO","UNIDADE","MODALIDADE","VALIDADE"/*,"ENTRADA","SAIDA"*/,"ESTOQUE ATUAL"};
    private ArrayList dados;
    
    public PPSHome() {
        initComponents();
        inicializaTabelaPPS();
        btLimpaBusca.setVisible(false);
    }
    public PPSHome(String query){
        initComponents();
        atualizaTabelaPPS(query);
        btLimpaBusca.setVisible(false);
    }
    protected void inicializaTabelaPPS(){
        try{
            estoque_pps = EstoquePPSDAO.selectEstoquePPS();
            constroiTabela(estoque_pps);
        } catch (SQLException ex) {
            Logger.getLogger(PPSHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected void atualizaTabelaPPS(String query){
        try{
            estoque_pps = EstoquePPSDAO.buscaEstoquePPSByDescricao(query);
            constroiTabela(estoque_pps);
        } catch (SQLException ex) {
            Logger.getLogger(PPSHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void constroiTabela(List<EstoquePPS> estoque_pps){
        dados = new ArrayList();
        try{
            for(EstoquePPS estoque:estoque_pps){
                if(estoque.getQuantidade()<=0)
                    continue;
                Entrada entrada = EntradaDAO.selectUltimaEntradaByIDEstoque(estoque.getId());
                Saida saida = SaidaDAO.selectUltimaSaidaByIDEstoque(estoque.getId());
                dados.add(new Object[]{
                        estoque.getnLote(),
                        estoque.getPps().getDescricao(),
                        estoque.getPps().getUnidade().getUnidade(),
                        estoque.getPps().getModalidade().getModalidade(),
                        formatarData(estoque.getDataValidade()),
                       // 0,
                        //entrada!=null?entrada.getQuantidade():0,
                        //saida!=null?saida.getQuantidade():0,
                        estoque.getQuantidade()
                });
            }
            
            ModelTable model = new ModelTable(dados, colunas);
            tabelaPPS.setModel(model);
            tabelaPPS.getColumnModel().getColumn(0).setPreferredWidth(200);
            tabelaPPS.getColumnModel().getColumn(1).setPreferredWidth(750);
            tabelaPPS.getColumnModel().getColumn(2).setPreferredWidth(400);
            tabelaPPS.getColumnModel().getColumn(3).setPreferredWidth(400);
            tabelaPPS.getColumnModel().getColumn(4).setPreferredWidth(400);
            tabelaPPS.getColumnModel().getColumn(5).setPreferredWidth(400);
            tabelaPPS.getColumnModel().getColumn(1).setCellRenderer( new VariableRowHeightRenderer(tabelaPPS.getColumnModel().getColumn(1).getMinWidth()));
            tabelaPPS.getTableHeader().setResizingAllowed(false);
            tabelaPPS.getTableHeader().setReorderingAllowed(false);
            tabelaPPS.getSelectionModel().addListSelectionListener(selecionarLinha());
            tabelaPPS.repaint();
        } catch (SQLException ex) {
            Logger.getLogger(PPSHome.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(PPSHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private ListSelectionListener selecionarLinha(){
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tabelaPPS.getSelectedRow() != -1){
                    String cod = (String) tabelaPPS.getValueAt(tabelaPPS.getSelectedRow(), 0);
                    try {
                        EstoquePPS pps = EstoquePPSDAO.selectEstoquePPSByLote(cod);
                        Object[] options = {"Entrada","Saida"};
                        int resposta = JOptionPane.showOptionDialog(getParent(),
                            "Produto: "+pps.getPps().getDescricao()+"\nLote: "+ pps.getnLote(),
                            "O que deseja fazer?",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
                        if(resposta == 0)
                           PrincipalPanel.showPPSEntrada(pps); 
                        else if(resposta == 1)
                            PrincipalPanel.showPPSSaida(pps);
                    tabelaPPS.clearSelection();
                    } catch (SQLException ex) {
                        System.out.println(ex.getErrorCode()+":"+ex.getMessage());
                    }
                    
                }
            }
        };
    }
    private String formatarData(Date data) throws ParseException{
        return out.format(in.parse(data.toString()));
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfBusca = new javax.swing.JTextField();
        btBusca = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaPPS = new javax.swing.JTable();
        btLimpaBusca = new javax.swing.JButton();

        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(750, 550));

        tfBusca.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N

        btBusca.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btBusca.setText("Pesquisar");
        btBusca.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btBusca.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btBuscaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btBuscaMouseExited(evt);
            }
        });
        btBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscaActionPerformed(evt);
            }
        });

        tabelaPPS.setBackground(new java.awt.Color(0, 153, 153));
        tabelaPPS.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tabelaPPS.setForeground(new java.awt.Color(255, 255, 255));
        tabelaPPS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelaPPS);

        btLimpaBusca.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        btLimpaBusca.setText("Limpar Busca");
        btLimpaBusca.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btLimpaBusca.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btLimpaBuscaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btLimpaBuscaMouseExited(evt);
            }
        });
        btLimpaBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimpaBuscaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 726, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tfBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btLimpaBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tfBusca, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(btBusca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btLimpaBusca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 472, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscaActionPerformed
        btBusca.setEnabled(false);
        String query = tfBusca.getText().toString().trim();
        if(query.isEmpty() || query.equals("")){
            JOptionPane.showMessageDialog(getParent(), "Preencha o campo corretamente","ALERTA",JOptionPane.ERROR_MESSAGE);
            btBusca.setEnabled(true);
        }else{
           atualizaTabelaPPS(query);
           btLimpaBusca.setVisible(true);
           btBusca.setEnabled(true);
        }
    }//GEN-LAST:event_btBuscaActionPerformed

    private void btLimpaBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimpaBuscaActionPerformed
        inicializaTabelaPPS();
        btLimpaBusca.setVisible(false);
        tfBusca.setText("");
    }//GEN-LAST:event_btLimpaBuscaActionPerformed

    private void btBuscaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBuscaMouseEntered
        btBusca.setBackground(new Color(0,153,153));
        btBusca.setForeground(Color.WHITE);
    }//GEN-LAST:event_btBuscaMouseEntered

    private void btBuscaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btBuscaMouseExited
        btBusca.setBackground(new Color(240,240,240));
        btBusca.setForeground(Color.BLACK);
    }//GEN-LAST:event_btBuscaMouseExited

    private void btLimpaBuscaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btLimpaBuscaMouseEntered
        btLimpaBusca.setBackground(new Color(0,153,153));
        btLimpaBusca.setForeground(Color.WHITE);
    }//GEN-LAST:event_btLimpaBuscaMouseEntered

    private void btLimpaBuscaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btLimpaBuscaMouseExited
        btLimpaBusca.setBackground(new Color(240,240,240));
        btLimpaBusca.setForeground(Color.BLACK);
    }//GEN-LAST:event_btLimpaBuscaMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBusca;
    private javax.swing.JButton btLimpaBusca;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaPPS;
    private javax.swing.JTextField tfBusca;
    // End of variables declaration//GEN-END:variables
}
