package DAO;

import Conexao.ConexaoBanco;
import Dominio.Concentracao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class ConcentracaoDAO {
    private static final String SELECT = "SELECT * FROM concentracao";
    private static final String SELECT_WHERE_COD = "SELECT * FROM concentracao WHERE idconcentracao = ?";
    private static final String INSERT = "INSERT INTO concentracao(" +
                                         "tipoConcentracao)" +
                                         "VALUES (?)";
    private static final String DELETE ="DELETE FROM concentracao WHERE idconcentracao = ?";
    private static final String UPDATE ="UPDATE concentracao SET tipoConcentracao = ? WHERE idconcentracao = ?";
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static List<Concentracao> selectConcentracao() throws SQLException{
        List<Concentracao> concentracoes = new ArrayList<>();
        Concentracao concentracao;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            concentracao = new Concentracao(resultSet.getInt(1),resultSet.getString(2));
            concentracoes.add(concentracao);
        }
        return concentracoes;
    }
    public static Concentracao selectConcentracaoByID(int id) throws SQLException{
        Concentracao concentracao = null;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next())
            concentracao = new Concentracao(resultSet.getInt(1),resultSet.getString(2));
        return concentracao;
    }
    public static boolean insertConcentracao(Concentracao concentracao) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT);
        preparedStatement.setString(1, concentracao.getConcentracao());
        return preparedStatement.execute();
        
    }

    public static boolean delete(int id) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement(DELETE);
        preparedStatement.setInt(1, id);
        return preparedStatement.execute();     
    }

    public static boolean updateConcentracaoByid(Concentracao concentracao) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE);
        preparedStatement.setString(1, concentracao.getConcentracao());
        preparedStatement.setInt(2, concentracao.getId());
        return preparedStatement.execute();
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}
