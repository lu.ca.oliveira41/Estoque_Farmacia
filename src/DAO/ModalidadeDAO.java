package DAO;

import Conexao.ConexaoBanco;
import Dominio.Concentracao;
import Dominio.Modalidade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class ModalidadeDAO {
    private static final String SELECT = "SELECT * FROM modalidade";
    private static final String SELECT_WHERE_COD = "SELECT * FROM modalidade WHERE idmodalidade = ?";
    private static final String INSERT = "INSERT INTO modalidade(" +
                                         "tipoModalidade)" +
                                         "VALUES (?)";
     private static final String DELETE ="DELETE FROM modalidade WHERE idmodalidade = ?";
    private static final String UPDATE ="UPDATE modalidade SET tipoModalidade = ? WHERE idmodalidade = ?";
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static List<Modalidade> selectModalidade() throws SQLException{
        List<Modalidade> modalidades = new ArrayList<>();
        Modalidade modalidade;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            modalidade = new Modalidade(resultSet.getInt(1),resultSet.getString(2));
            modalidades.add(modalidade);
        }
        return modalidades;
    }
    public static Modalidade selectModalidadeByID(int id) throws SQLException{
        Modalidade modalidade=null;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next())
            modalidade = new Modalidade(resultSet.getInt(1),resultSet.getString(2));
        return modalidade;
    }
    public static boolean insertModalidade(Modalidade modalidade) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT);
        preparedStatement.setString(1, modalidade.getModalidade());
        return preparedStatement.execute();
        
    }

    public static boolean delete(int id) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement(DELETE);
        preparedStatement.setInt(1, id);
        return preparedStatement.execute();     
    }
    public static boolean updateModalidadeByid(Modalidade modalidade) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE);
        preparedStatement.setString(1, modalidade.getModalidade());
        preparedStatement.setInt(2, modalidade.getId());
        return preparedStatement.execute();
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}
