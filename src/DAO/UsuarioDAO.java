package DAO;

import Conexao.ConexaoBanco;
import Dominio.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class UsuarioDAO {
    private final static String LOGIN = "SELECT nome, senha FROM usuario WHERE nome = ?  AND senha = ?";
    private final static String UPDATE = "UPDATE usuario SET nome = ?, senha = ? WHERE idusuario = ?";
    private final static String SELECT = "SELECT * FROM usuario";
    private final static String INSERT = "INSERT INTO usuario (nome,senha) values ('adm','adm')";
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static boolean login(String usuario,String senha) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(LOGIN);
        preparedStatement.setString(1, usuario);
        preparedStatement.setString(2, senha);
        
        ResultSet rs = preparedStatement.executeQuery();
        
        if(rs.next()){
            return true;
        } else{
            return false;
        }        
    }
    public static void updateInfo(Usuario usuario) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE);
        preparedStatement.setString(1, usuario.getNome());
        preparedStatement.setString(2, usuario.getSenha());
        preparedStatement.setInt(3,usuario.getId());
        preparedStatement.execute();
        
    }
    public static Usuario selectUsuario() throws SQLException{
        Usuario usuario;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            usuario = new Usuario(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3));
            return usuario;
        }
        return null;
    }

    public static void novoUser() throws SQLException{
        if(selectUsuario()==null){
           PreparedStatement preparedStatement = conexao.prepareStatement(INSERT);
           preparedStatement.executeUpdate();
        }
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}
