package DAO;

import Conexao.ConexaoBanco;
import Dominio.Entrada;
import Dominio.Saida;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class SaidaDAO {
    private static final String SELECT = "SELECT * FROM saida";
    private static final String SELECT_WHERE_COD = "SELECT * FROM saida WHERE idsaida = ?";
     private static final String SELECT_WHERE_COD_ESTOQUE = "SELECT * FROM saida WHERE idestoque = ?";
      private static final String SELECT_MAX_WHERE_COD_ESTOQUE = "SELECT * FROM saida WHERE idestoque = ? ORDER BY dataSaida DESC LIMIT 1";
    private static final String INSERT = "INSERT INTO saida(" +
                                         "quantidade,dataSaida,idestoque)" +
                                         "VALUES (?,?,?)";
    
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static List<Saida> selectSaida() throws SQLException{
        List<Saida> saidas = new ArrayList<>();
        Saida saida;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            saida = new Saida(resultSet.getInt(1),resultSet.getInt(2),resultSet.getTimestamp(3),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4)));
            saidas.add(saida);
        }
        return saidas;
    }
    public static Saida selectSaidaByID(int id) throws SQLException{
        Saida saida;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
            saida = new Saida(resultSet.getInt(1),resultSet.getInt(2),resultSet.getTimestamp(3),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4)));
        return saida;
    }
    public static void insertSaida(Saida saida) throws SQLException{
        Timestamp data = new Timestamp(saida.getData().getTime());
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT);
        preparedStatement.setInt(1, saida.getQuantidade());
        preparedStatement.setTimestamp(2, data);
        preparedStatement.setInt(3, saida.getEstoque().getId());
        preparedStatement.executeUpdate();
        EstoqueDAO.updateSaidaEstoque(saida.getEstoque(), saida.getQuantidade());
        
    }
     public static List<Saida> selectSaidaByIDEstoque(int id) throws SQLException{
        List<Saida> saidas = new ArrayList<>();
        Saida saida;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD_ESTOQUE);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            saida = new Saida(resultSet.getInt(1),resultSet.getInt(2),resultSet.getTimestamp(3),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4)));
            saidas.add(saida);
        }
        return saidas;
    }
     
     public static Saida selectUltimaSaidaByIDEstoque(int id) throws SQLException{
        Saida saida = null;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_MAX_WHERE_COD_ESTOQUE);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next())
            saida = new Saida(resultSet.getInt(1),resultSet.getInt(2),resultSet.getTimestamp(3),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4)));
        return saida;
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}
