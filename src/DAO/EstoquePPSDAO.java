package DAO;

import Conexao.ConexaoBanco;
import Dominio.Estoque;
import Dominio.EstoquePPS;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class EstoquePPSDAO {
    private static final String SELECT = "SELECT * FROM estoquepps";
    private static final String SELECT_WHERE_COD = "SELECT * FROM estoquepps WHERE idestoquePps = ?";
    private static final String SELECT_WHERE_COD_ESTOQUE = "SELECT * FROM estoquepps WHERE idestoque = ?";
    private static final String SELECT_WHERE_LOTE = "SELECT * FROM estoquepps WHERE lote = ?";
    private static final String INSERT = "INSERT INTO estoquepps(" +
                                         "lote,idpps,idestoque)" +
                                         "VALUES (?,?,?)";
    private static final String UPDATE ="UPDATE estoquepps SET lote = ?, idpps = ?"
                                        + "WHERE idestoquePps = ?";
    private static final String DELETE = "DELETE FROM estoquepps WHERE idestoquePps = ?";
     private static final String SEARCH = "SELECT * FROM estoquepps INNER JOIN pps ON estoquepps.idestoquePps = pps.idpps WHERE (pps.descricao LIKE ? OR estoquepps.lote LIKE ?)";
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static List<EstoquePPS> selectEstoquePPS() throws SQLException{
        List<EstoquePPS> produtos = new ArrayList<>();
        EstoquePPS produto;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            produto = new EstoquePPS(resultSet.getInt(1),
                                    resultSet.getString(2),
                                    ProdutoParaSaudeDAO.selectPPSByID(resultSet.getInt(3)),
                                    EstoqueDAO.selectEstoqueByID(resultSet.getInt(4))
                                    );
            produtos.add(produto);
        }
        return produtos;
    }
    public static EstoquePPS selectEstoquePPSByID(int id) throws SQLException{
        EstoquePPS produto;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        produto = new EstoquePPS(resultSet.getInt(1),
                                resultSet.getString(2),
                                ProdutoParaSaudeDAO.selectPPSByID(resultSet.getInt(3)),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4))
                                );
        return produto;
    }
    public static EstoquePPS selectEstoquePPSByLote(String id) throws SQLException{
        EstoquePPS produto=null;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_LOTE);
        preparedStatement.setString(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next())
        produto = new EstoquePPS(resultSet.getInt(1),
                                resultSet.getString(2),
                                ProdutoParaSaudeDAO.selectPPSByID(resultSet.getInt(3)),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4))
                                );
        return produto;
    }
    
    public static void insertEstoquePPS(EstoquePPS produto) throws SQLException{
        Estoque estoque = new Estoque(produto.getQuantidade(),produto.getDataValidade());
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT);
        preparedStatement.setString(1, produto.getnLote());
        preparedStatement.setInt(2, produto.getPps().getId());
        preparedStatement.setInt(3, produto.getId());
        preparedStatement.executeUpdate();
    }
    
    public static void updateEstoquePPS(EstoquePPS produto) throws SQLException{
        Estoque estoque = new Estoque(produto.getQuantidade(),produto.getDataValidade());
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE);
        preparedStatement.setString(1,produto.getnLote());
        preparedStatement.setInt(2,produto.getPps().getId());
        preparedStatement.setInt(3,produto.getIdEstoquePPS());
        preparedStatement.execute();
        
    }
    
    public static void deleteEstoquePPS(EstoquePPS produto) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(DELETE);
        preparedStatement.setInt(1, produto.getId());
        preparedStatement.execute();
        EstoqueDAO.updateSaidaEstoque(new Estoque(produto.getId(), produto.getQuantidade(),produto.getDataValidade()),produto.getQuantidade());//zeranndo o estoque
    }

    public static List<EstoquePPS> buscaEstoquePPSByDescricao(String query) throws SQLException{
        List<EstoquePPS> estoquePPSs = new ArrayList<>();
        EstoquePPS estoquePPS;
        PreparedStatement preparedStatement = conexao.prepareStatement(SEARCH);
        preparedStatement.setString(1,"%"+query+"%");
        preparedStatement.setString(2,"%"+query+"%");
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            estoquePPS = new EstoquePPS(resultSet.getInt(1),
                                resultSet.getString(2),
                                ProdutoParaSaudeDAO.selectPPSByID(resultSet.getInt(3)),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4))
                                );
            estoquePPSs.add(estoquePPS);
        }
        return estoquePPSs;
    }
    
    public static EstoquePPS buscaEstoquePPSByIDEstoque(int cod) throws SQLException{
        EstoquePPS produto = null;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD_ESTOQUE);
        preparedStatement.setInt(1, cod);
        ResultSet resultSet = preparedStatement.executeQuery();
         
        while(resultSet.next()){
            produto = new EstoquePPS(resultSet.getInt(1),
                                    resultSet.getString(2),
                                    ProdutoParaSaudeDAO.selectPPSByID(resultSet.getInt(3)),
                                    EstoqueDAO.selectEstoqueByID(resultSet.getInt(4))
                                    );
        }
        return produto;
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}
