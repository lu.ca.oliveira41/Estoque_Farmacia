package DAO;

import Conexao.ConexaoBanco;
import Dominio.Entrada;
import Dominio.Estoque;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class EntradaDAO {
    private static final String SELECT = "SELECT * FROM entrada";
    private static final String SELECT_WHERE_COD = "SELECT * FROM entrada WHERE identrada = ?";
    private static final String SELECT_WHERE_COD_ESTOQUE = "SELECT * FROM entrada WHERE idestoque = ?";
    private static final String SELECT_MAX_WHERE_COD_ESTOQUE = "SELECT * FROM entrada WHERE idestoque = ? ORDER BY dataEntrada DESC LIMIT 1";
    private static final String INSERT = "INSERT INTO entrada(" +
                                         "quantidade,dataEntrada,idestoque)" +
                                         "VALUES (?,?,?)";
    private static final String UPDATE_ENTRADA = "UPDATE entrada SET quantidade = ? WHERE identrada = ?";
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static List<Entrada> selectEntrada() throws SQLException{
        List<Entrada> entradas = new ArrayList<>();
        Entrada entrada;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            entrada = new Entrada(resultSet.getInt(1),resultSet.getInt(2),resultSet.getTimestamp(3),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4)));
            entradas.add(entrada);
        }
        return entradas;
    }
    public static Entrada selectEntradaByID(int id) throws SQLException{
        Entrada entrada;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        entrada = new Entrada(resultSet.getInt(1),resultSet.getInt(2),resultSet.getTimestamp(3),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4)));
        return entrada;
    }
    public static void insertEntrada(Entrada entrada) throws SQLException{
        Timestamp data = new Timestamp(entrada.getData().getTime());
        System.out.println(data);
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT);
        preparedStatement.setInt(1, entrada.getQuantidade());
        preparedStatement.setTimestamp(2, data);
        preparedStatement.setInt(3, entrada.getEstoque().getId());
        preparedStatement.executeUpdate();
        EstoqueDAO.updateEntradaEstoque(entrada.getEstoque(), entrada.getQuantidade());
        
    }
    public static List<Entrada> selectEntradaByIDEstoque(int id) throws SQLException{
        List<Entrada> entradas = new ArrayList<>();
        Entrada entrada;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD_ESTOQUE);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            entrada = new Entrada(resultSet.getInt(1),resultSet.getInt(2),resultSet.getTimestamp(3),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4)));
            entradas.add(entrada);
        }
        return entradas;
    }
    public static Entrada selectUltimaEntradaByIDEstoque(int id) throws SQLException{
        Entrada entrada = null;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_MAX_WHERE_COD_ESTOQUE);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next())
            entrada = new Entrada(resultSet.getInt(1),resultSet.getInt(2),resultSet.getTimestamp(3),
                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4)));
        return entrada;
    }
    
    public static boolean AtualizaEntrada(Entrada entrada) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE_ENTRADA);
        preparedStatement.setInt(1, entrada.getCodEntrada());
        preparedStatement.setInt(2, entrada.getQuantidade());
        return preparedStatement.execute();
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}
