package DAO;

import Conexao.ConexaoBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import Dominio.Apresentacao;
import java.util.ArrayList;
public final class ApresentacaoDAO {
    private static final String SELECT = "SELECT * FROM apresentacao";
    private static final String SELECT_WHERE_COD = "SELECT * FROM apresentacao WHERE idapresentacao = ?";
    private static final String INSERT = "INSERT INTO apresentacao(" +
                                         "tipoApresentacao)" +
                                         "VALUES (?)";
    private static final String DELETE ="DELETE FROM apresentacao WHERE idapresentacao = ?";
    private static final String UPDATE ="UPDATE apresentacao SET tipoApresentacao = ? WHERE idapresentacao = ?";
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static List<Apresentacao> selectApresentacao() throws SQLException{
        List<Apresentacao> apresentacoes = new ArrayList<>();
        Apresentacao apresentacao;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            apresentacao = new Apresentacao(resultSet.getInt(1),resultSet.getString(2));
            apresentacoes.add(apresentacao);
        }
        return apresentacoes;
    }
    public static Apresentacao selectApresentacaoByID(int id) throws SQLException{
        Apresentacao apresentacao = null;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next())
            apresentacao = new Apresentacao(resultSet.getInt(1),resultSet.getString(2));
        return apresentacao;
    }
    public static boolean insertApresentacao(Apresentacao apresentacao) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT);
        preparedStatement.setString(1, apresentacao.getApresentacao());
        return preparedStatement.execute();
        
    }
     public static boolean delete(int id) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement(DELETE);
        preparedStatement.setInt(1, id);
        return preparedStatement.execute();     
    }

    public static boolean updateApresentacaoByid(Apresentacao apresentacao) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE);
        preparedStatement.setString(1, apresentacao.getApresentacao());
        preparedStatement.setInt(2, apresentacao.getId());
        return preparedStatement.execute();
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}

