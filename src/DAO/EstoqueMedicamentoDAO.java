package DAO;

import Conexao.ConexaoBanco;
import Dominio.Estoque;
import Dominio.EstoqueMedicamento;
import Dominio.Medicamento;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class EstoqueMedicamentoDAO {
    private static final String SELECT = "SELECT * FROM estoquemed";
    private static final String SELECT_WHERE_COD = "SELECT * FROM estoquemed WHERE idestoqueMed = ?";
    private static final String SELECT_WHERE_LOTE = "SELECT * FROM estoquemed WHERE lote = ?";
    private static final String SELECT_WHERE_COD_ESTOQUE = "SELECT * FROM estoquemed WHERE idestoque = ?";
    private static final String INSERT = "INSERT INTO estoquemed(" +
                                         "lote,idmedicamento,idestoque)" +
                                         "VALUES (?,?,?)";
    private static final String UPDATE ="UPDATE estoquemed SET lote = ?, idmedicamento = ?"
                                        + "WHERE idestoqueMed = ?";
    private static final String DELETE = "DELETE FROM estoquemed WHERE idestoqueMed = ?";
    private static final String SEARCH = "SELECT * FROM estoquemed INNER JOIN medicamento ON estoquemed.idmedicamento = medicamento.idmedicamento "
            + " WHERE (medicamento.dcb LIKE ? OR estoquemed.lote LIKE ?)";
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static List<EstoqueMedicamento> selectMedicamento() throws SQLException{
        List<EstoqueMedicamento> medicamentos = new ArrayList<>();
        EstoqueMedicamento medicamento;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            medicamento = new EstoqueMedicamento(resultSet.getInt(1),
                                                resultSet.getString(2),
                                                MedicamentoDAO.selectMedicamentoByID(resultSet.getInt(3)),
                                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4))
                                               );
            medicamentos.add(medicamento);
        }
        return medicamentos;
    }
    public static EstoqueMedicamento selectMedicamentoByID(int id) throws SQLException{
        EstoqueMedicamento medicamento;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
         medicamento = new EstoqueMedicamento(resultSet.getInt(1),
                                                resultSet.getString(2),
                                                MedicamentoDAO.selectMedicamentoByID(resultSet.getInt(3)),
                                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4))
                                                );
        return medicamento;
    }
    public static EstoqueMedicamento selectMedicamentoByLote(String id) throws SQLException{
        EstoqueMedicamento medicamento = null;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_LOTE);
        preparedStatement.setString(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next())
         medicamento = new EstoqueMedicamento(resultSet.getInt(1),
                                                resultSet.getString(2),
                                                MedicamentoDAO.selectMedicamentoByID(resultSet.getInt(3)),
                                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4))
                                                );
        return medicamento;
    }
    public static void insertEstoqueMedicamento(EstoqueMedicamento medicamento) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT);
        System.out.println("lote"+medicamento.getnLote());
        preparedStatement.setString(1, medicamento.getnLote());
        preparedStatement.setInt(2, medicamento.getMedicamento().getId());
        preparedStatement.setInt(3, medicamento.getId());
        preparedStatement.executeUpdate();
    }
    public static void updateEstoqueMedicamento(EstoqueMedicamento medicamento) throws SQLException{
        Estoque estoque = new Estoque(medicamento.getId(), medicamento.getQuantidade(),medicamento.getDataValidade());
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE);
        preparedStatement.setString(1,medicamento.getnLote());
        preparedStatement.setInt(2,medicamento.getMedicamento().getId());
        preparedStatement.setInt(3,medicamento.getIdEstoqueMedicamento());
        preparedStatement.execute();
        
    }
    public static void deleteEstoqueMedicamento(EstoqueMedicamento medicamento) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(DELETE);
        preparedStatement.setInt(1, medicamento.getId());
        preparedStatement.execute();
        EstoqueDAO.updateSaidaEstoque(new Estoque(medicamento.getId(), medicamento.getQuantidade(),medicamento.getDataValidade()),medicamento.getQuantidade());//zeranndo o estoque
    }
    public static List<EstoqueMedicamento> buscaEstoqueMedicamentoByDCB(String query) throws SQLException{
        List<EstoqueMedicamento> medicamentos = new ArrayList<>();
        EstoqueMedicamento medicamento;
        PreparedStatement preparedStatement = conexao.prepareStatement(SEARCH);
        preparedStatement.setString(1,"%"+query+"%");
         preparedStatement.setString(2,"%"+query+"%");
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            medicamento = new EstoqueMedicamento(resultSet.getInt(1),
                                                resultSet.getString(2),
                                                MedicamentoDAO.selectMedicamentoByID(resultSet.getInt(3)),
                                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4))
                                               );
            medicamentos.add(medicamento);
        }
        return medicamentos;
    } 
    public static EstoqueMedicamento buscaEstoqueMedicamentoByIDEstoque(int cod) throws SQLException{
        EstoqueMedicamento medicamento = null;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD_ESTOQUE);
        preparedStatement.setInt(1, cod);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next())
         medicamento = new EstoqueMedicamento(resultSet.getInt(1),
                                                resultSet.getString(2),
                                                MedicamentoDAO.selectMedicamentoByID(resultSet.getInt(3)),
                                                EstoqueDAO.selectEstoqueByID(resultSet.getInt(4))
                                                );
        return medicamento;
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}
