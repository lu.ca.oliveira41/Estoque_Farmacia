package DAO;

import Conexao.ConexaoBanco;
import Dominio.Modalidade;
import Dominio.Unidade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public final class UnidadeDAO {
    private static final String SELECT = "SELECT * FROM unidade";
    private static final String SELECT_WHERE_COD = "SELECT * FROM unidade WHERE idunidade = ?";
    private static final String INSERT = "INSERT INTO unidade(" +
                                         "descricao)" +
                                         "VALUES (?)";
    private static final String DELETE ="DELETE FROM unidade WHERE idunidade = ?";
    private static final String UPDATE ="UPDATE unidade SET descricao = ? WHERE idunidade = ?";
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static List<Unidade> selectUnidade() throws SQLException{
        List<Unidade> unidades = new ArrayList<>();
        Unidade unidade;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            unidade = new Unidade(resultSet.getInt(1),resultSet.getString(2));
            unidades.add(unidade);
        }
        return unidades;
    }
    public static Unidade selectUnidadeByID(int id) throws SQLException{
        Unidade unidade;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
       
        resultSet.next();
        unidade = new Unidade(resultSet.getInt(1),resultSet.getString(2));
        return unidade;
    }
    public static boolean insertUnidade(Unidade unidade) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT);
        preparedStatement.setString(1, unidade.getUnidade());
        return preparedStatement.execute();
        
    }
    public static boolean delete(int id) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement(DELETE);
        preparedStatement.setInt(1, id);
        return preparedStatement.execute();
        
    }

    public static boolean updateUnidadeByid(Unidade unidade) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE);
        preparedStatement.setString(1, unidade.getUnidade());
        preparedStatement.setInt(2, unidade.getId());
        return preparedStatement.execute();
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}
