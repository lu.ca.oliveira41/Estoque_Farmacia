package DAO;

import Conexao.ConexaoBanco;
import Dominio.Medicamento;
import Dominio.ProdutoParaSaude;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public final class ProdutoParaSaudeDAO {
    private static final String SELECT = "SELECT * FROM pps";
    private static final String SELECT_WHERE_COD = "SELECT * FROM pps WHERE idpps = ?";
    
    private static final String INSERT = "INSERT INTO pps(" +
                                         "descricao,idunidade,idmodalidade)" +
                                         "VALUES (?,?,?)";
    private static final String UPDATE ="UPDATE pps SET descricao = ?, idunidade = ?, idmodalidade = ?"
                                        + "WHERE idpps = ?";
    private static final String DELETE = "DELETE FROM pps WHERE idpps = ?";
    private static final String SEARCH = "SELECT * FROM pps WHERE (pps.descricao LIKE ?)";
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static List<ProdutoParaSaude> selectPPS() throws SQLException{
        List<ProdutoParaSaude> produtos = new ArrayList<>();
        ProdutoParaSaude produto;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            produto = new ProdutoParaSaude(resultSet.getInt(1),
                                        resultSet.getString(2),
                                        UnidadeDAO.selectUnidadeByID(resultSet.getInt(3)),
                                        ModalidadeDAO.selectModalidadeByID(resultSet.getInt(4))
                                        );
            produtos.add(produto);
        }
        return produtos;
    }
    public static ProdutoParaSaude selectPPSByID(int id) throws SQLException{
        ProdutoParaSaude produto;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
            produto = new ProdutoParaSaude(resultSet.getInt(1),
                                        resultSet.getString(2),
                                        UnidadeDAO.selectUnidadeByID(resultSet.getInt(3)),
                                        ModalidadeDAO.selectModalidadeByID(resultSet.getInt(4))
                                        );
        return produto;
    }
    public static int insertPPS(ProdutoParaSaude produto) throws SQLException{
        int key=-1;
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, produto.getDescricao());
        preparedStatement.setInt(2, produto.getUnidade().getId());
        preparedStatement.setInt(3, produto.getModalidade().getId());
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        if(rs.next()){
            key = rs.getInt(1);
        }
        return key;
    }
    
    public static boolean updatePPS(ProdutoParaSaude produto) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE);
        preparedStatement.setString(1, produto.getDescricao());
        preparedStatement.setInt(2, produto.getUnidade().getId());
        preparedStatement.setInt(3, produto.getModalidade().getId());
        preparedStatement.setInt(4, produto.getId());
        return preparedStatement.execute(); 
    }

    public static void deletePPS(ProdutoParaSaude produto) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(DELETE);
        preparedStatement.setInt(1, produto.getId());
        preparedStatement.execute(); 
    }

    public static List<ProdutoParaSaude> selectPPSByDescricao(String query) throws SQLException{
        List<ProdutoParaSaude> produtos = new ArrayList<>();
        ProdutoParaSaude produto;
        PreparedStatement preparedStatement = conexao.prepareStatement(SEARCH);
        preparedStatement.setString(1,"%"+query+"%");
        
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            produto = new ProdutoParaSaude(resultSet.getInt(1),
                                        resultSet.getString(2),
                                        UnidadeDAO.selectUnidadeByID(resultSet.getInt(3)),
                                        ModalidadeDAO.selectModalidadeByID(resultSet.getInt(4))
                                        );
            produtos.add(produto);
        }
        return produtos;
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}
