
package DAO;

import Conexao.ConexaoBanco;
import Dominio.Entrada;
import Dominio.EstoqueMedicamento;
import Dominio.ItemRelatorioMed;
import Dominio.ItemRelatorioPPS;
import Dominio.Saida;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

public final class RelatorioDAO {
    private static final String SELECT_MED_ENTRADA = "SELECT * FROM estoquemed"
            + " INNER JOIN medicamento ON estoquemed.idmedicamento = medicamento.idmedicamento"
            + " INNER JOIN entrada ON estoquemed.idestoque = entrada.idestoque"
            + " WHERE entrada.dataEntrada BETWEEN ? AND ?";
    private static final String SELECT_MED_SAIDA = "SELECT * FROM estoquemed"
            + " INNER JOIN medicamento ON estoquemed.idmedicamento = medicamento.idmedicamento"
            + " INNER JOIN saida ON estoquemed.idestoque = saida.idestoque"
            + " WHERE saida.dataSaida BETWEEN ? AND ?";
    private static final String SELECT_PPS_ENTRADA = "SELECT * FROM estoquepps"
            + " INNER JOIN pps ON estoquepps.idpps = pps.idpps"
            +"  INNER JOIN entrada ON estoquepps.idestoque = entrada.idestoque"
            + " WHERE entrada.dataEntrada BETWEEN ? AND ?";
    private static final String SELECT_PPS_SAIDA = "SELECT * FROM estoquepps"
            + " INNER JOIN pps ON estoquepps.idpps = pps.idpps "
            + " INNER JOIN saida ON estoquepps.idestoque = saida.idestoque"
            + " WHERE saida.dataSaida BETWEEN ? AND ?";
    
    
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static ArrayList<ItemRelatorioMed> selectRelatorioMed(Date inicio,Date fim) throws SQLException{
       ItemRelatorioMed item = null;
       Timestamp dataInicio = new Timestamp(inicio.getTime());
       Timestamp dataFim = new Timestamp(fim.getTime());
       ArrayList<ItemRelatorioMed> itemRelatorio = new ArrayList<>();
       PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_MED_ENTRADA);
       preparedStatement.setTimestamp(1, dataInicio);
       preparedStatement.setTimestamp(2, dataFim);
       ResultSet result = preparedStatement.executeQuery();
       while(result.next()){
           item = new ItemRelatorioMed(
                  EstoqueMedicamentoDAO.selectMedicamentoByLote(result.getString("lote")),
                  EntradaDAO.selectEntradaByID(result.getInt("identrada"))
                  );
           itemRelatorio.add(item);
       }
       preparedStatement = conexao.prepareStatement(SELECT_MED_SAIDA);
       preparedStatement.setTimestamp(1, dataInicio);
       preparedStatement.setTimestamp(2, dataFim);
       result = preparedStatement.executeQuery();
       while(result.next()){
           item = new ItemRelatorioMed(
                  EstoqueMedicamentoDAO.selectMedicamentoByLote(result.getString("lote")),
                  SaidaDAO.selectSaidaByID(result.getInt("idsaida"))
                  );
           itemRelatorio.add(item);
       }
       Collections.sort(itemRelatorio);
       return itemRelatorio;
   }
    
    public static ArrayList<ItemRelatorioPPS> selectRelatorioPPS(Date inicio,Date fim) throws SQLException{
       ItemRelatorioPPS item;
       ArrayList<ItemRelatorioPPS> itemRelatorio = new ArrayList();
       PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_PPS_ENTRADA);
       Timestamp dataInicio = new Timestamp(inicio.getTime());
       Timestamp dataFim = new Timestamp(fim.getTime());
       preparedStatement.setTimestamp(1, dataInicio);
       preparedStatement.setTimestamp(2, dataFim);
       ResultSet result = preparedStatement.executeQuery();
       while(result.next()){
           item = new ItemRelatorioPPS(
                  EstoquePPSDAO.selectEstoquePPSByLote(result.getString("lote")),
                  EntradaDAO.selectEntradaByID(result.getInt("identrada"))
                  );
           itemRelatorio.add(item);
       }
       preparedStatement = conexao.prepareStatement(SELECT_PPS_SAIDA);
       preparedStatement.setTimestamp(1, dataInicio);
       preparedStatement.setTimestamp(2, dataFim);
       result = preparedStatement.executeQuery();
       while(result.next()){
           item = new ItemRelatorioPPS(
                  EstoquePPSDAO.selectEstoquePPSByLote(result.getString("lote")),
                  SaidaDAO.selectSaidaByID(result.getInt("idsaida"))
                  );
           itemRelatorio.add(item);
       }
       Collections.sort(itemRelatorio);
       return itemRelatorio;
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}


