package DAO;

import Conexao.ConexaoBanco;
import Dominio.Estoque;
import Dominio.EstoqueMedicamento;
import Dominio.Medicamento;
import Dominio.ProdutoParaSaude;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class MedicamentoDAO {
    private static final String SELECT = "SELECT * FROM medicamento";
    private static final String SELECT_WHERE_COD = "SELECT * FROM medicamento WHERE idmedicamento = ?";
    
    private static final String INSERT = "INSERT INTO medicamento(" +
                                         "dcb,idapresentacao,idconcentracao,idmodalidade)" +
                                         "VALUES (?,?,?,?)";
    private static final String UPDATE ="UPDATE medicamento SET dcb = ?, idapresentacao = ?, idconcentracao = ?, idmodalidade = ?"
                                        + "WHERE idmedicamento = ?";
    private static final String DELETE = "DELETE FROM medicamento WHERE idmedicamento = ?";
    private static final String SEARCH = "SELECT * FROM medicamento WHERE (medicamento.dcb LIKE ?)";
    private static Connection conexao = ConexaoBanco.getConexao();
    
    public static List<Medicamento> selectMedicamento() throws SQLException{
        List<Medicamento> medicamentos = new ArrayList<>();
        Medicamento medicamento;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            medicamento = new Medicamento(resultSet.getInt(1),
                                        resultSet.getString(2),
                                        ApresentacaoDAO.selectApresentacaoByID(resultSet.getInt(3)),
                                        ConcentracaoDAO.selectConcentracaoByID(resultSet.getInt(4)),
                                        ModalidadeDAO.selectModalidadeByID(resultSet.getInt(5))
                                        );
            medicamentos.add(medicamento);
        }
        return medicamentos;
    }
    public static Medicamento selectMedicamentoByID(int id) throws SQLException{
        Medicamento medicamento;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        medicamento = new Medicamento(resultSet.getInt(1),
                                    resultSet.getString(2),
                                    ApresentacaoDAO.selectApresentacaoByID(resultSet.getInt(3)),
                                    ConcentracaoDAO.selectConcentracaoByID(resultSet.getInt(4)),
                                    ModalidadeDAO.selectModalidadeByID(resultSet.getInt(5))
                                    );
        return medicamento;
    }
    public static int insertMedicamento(Medicamento medicamento) throws SQLException{
        int key =-1;
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, medicamento.getDenominacao());
        preparedStatement.setInt(2, medicamento.getApresentacao().getId());
        preparedStatement.setInt(3, medicamento.getConcentracao().getId());
        preparedStatement.setInt(4, medicamento.getModalidade().getId());
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        if(rs.next()){
            key = rs.getInt(1);
        }
        return key;
    }
    
    public static boolean updateMedicamento(Medicamento medicamento) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE);
        preparedStatement.setString(1,medicamento.getDenominacao());
        preparedStatement.setInt(2,medicamento.getApresentacao().getId());
        preparedStatement.setInt(3,medicamento.getConcentracao().getId());
        preparedStatement.setInt(4,medicamento.getModalidade().getId());
        preparedStatement.setInt(5,medicamento.getId());
        return preparedStatement.execute();
        
    }
    
     public static int deleteMedicamento(Medicamento medicamento) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(DELETE);
        preparedStatement.setInt(1, medicamento.getId());
        return preparedStatement.executeUpdate(); 
    }

    public static List<Medicamento> buscaEstoqueMedicamentoByDCB(String query) throws SQLException {
        List<Medicamento> medicamentos = new ArrayList<>();
        Medicamento medicamento;
        PreparedStatement preparedStatement = conexao.prepareStatement(SEARCH);
        preparedStatement.setString(1,"%"+query+"%");
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            medicamento = new Medicamento(resultSet.getInt(1),
                                        resultSet.getString(2),
                                        ApresentacaoDAO.selectApresentacaoByID(resultSet.getInt(3)),
                                        ConcentracaoDAO.selectConcentracaoByID(resultSet.getInt(4)),
                                        ModalidadeDAO.selectModalidadeByID(resultSet.getInt(5))
                                        );
            medicamentos.add(medicamento);
        }
        return medicamentos;
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
        
    }
}
