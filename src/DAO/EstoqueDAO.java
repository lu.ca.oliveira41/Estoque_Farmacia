package DAO;

import Conexao.ConexaoBanco;
import Dominio.Estoque;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public final class EstoqueDAO {
    private static final String SELECT = "SELECT * FROM estoque";
    private static final String SELECT_WHERE_COD = "SELECT * FROM estoque WHERE idestoque = ?";
    private static final String INSERT = "INSERT INTO estoque(" +
                                         "quantidade,validade)" +
                                         "VALUES (?,?)";
    private static final String UPDATE_QUANTIDADE_ENTRADA = "UPDATE estoque set quantidade = quantidade + ? where idestoque = ?";
    private static final String UPDATE_QUANTIDADE_BAIXA = "UPDATE estoque set quantidade = quantidade - ? where idestoque = ?";
    private static final String UPDATE = "UPDATE estoque set quantidade = ?,validade = ? where idestoque = ?";
    private static final String SELECT_VALIDATE ="SELECT * FROM estoque WHERE estoque.validade < ? AND estoque.quantidade>0";
    private static Connection conexao = ConexaoBanco.getConexao();
    private static List<Estoque> estoques = new ArrayList<>();
    public static List<Estoque> selectEstoque() throws SQLException{
        List<Estoque> estoques = new ArrayList<>();
        Estoque estoque;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            estoque = new Estoque(resultSet.getInt(1),resultSet.getInt(2),resultSet.getDate(3));
            estoques.add(estoque);
        }
        return estoques;
    }
    public static Estoque selectEstoqueByID(int id) throws SQLException{
        Estoque estoque;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_WHERE_COD);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        estoque = new Estoque(resultSet.getInt(1),resultSet.getInt(2),resultSet.getDate(3));
        return estoque;
    }
    public static int insertEstoque(Estoque estoque) throws SQLException{
        Date data = new Date(estoque.getDataValidade().getTime());
        PreparedStatement preparedStatement = conexao.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, estoque.getQuantidade());
        preparedStatement.setDate(2, data);
        preparedStatement.executeUpdate();
        ResultSet resultSet = preparedStatement.getGeneratedKeys();
        resultSet.next();
        return resultSet.getInt(1);
    }
    public static void updateEntradaEstoque(Estoque estoque,int entrada) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE_QUANTIDADE_ENTRADA);
        preparedStatement.setInt(1,entrada);
        preparedStatement.setInt(2,estoque.getId());
        preparedStatement.executeUpdate();
    } 
    public static void updateSaidaEstoque(Estoque estoque,int saida) throws SQLException{
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE_QUANTIDADE_BAIXA);
        preparedStatement.setInt(1,saida);
        preparedStatement.setInt(2,estoque.getId());
        preparedStatement.executeUpdate();
    }
    public static void updateEstoque(Estoque estoque) throws SQLException{
        Date data = new Date(estoque.getDataValidade().getTime());
        PreparedStatement preparedStatement = conexao.prepareStatement(UPDATE);
        preparedStatement.setInt(1,estoque.getQuantidade());
        preparedStatement.setDate(2, data);
        preparedStatement.setInt(3,estoque.getId());
        preparedStatement.executeUpdate();
    }
    public static List<Estoque> selectAlertas(int dias) throws SQLException{
        if(!estoques.isEmpty())
            estoques.clear();
        Estoque estoque;
        PreparedStatement preparedStatement = conexao.prepareStatement(SELECT_VALIDATE);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, dias);
        preparedStatement.setDate(1, new Date(cal.getTimeInMillis()));
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            estoque = new Estoque(resultSet.getInt(1),resultSet.getInt(2),resultSet.getDate(3));
            estoques.add(estoque);
        }
        return estoques;
    }
    public static int getEstoqueVencidoSize(){
        return estoques.isEmpty()?0:estoques.size();
    }
    public static void atualizaConexao(){
        conexao = ConexaoBanco.getConexao();
    }
}
