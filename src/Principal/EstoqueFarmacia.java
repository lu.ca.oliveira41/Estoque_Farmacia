/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;
import Conexao.ConexaoBanco;
import DAO.ApresentacaoDAO;
import DAO.EstoqueDAO;
import DAO.UsuarioDAO;
import Interfaces.LoginView;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class EstoqueFarmacia {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException, InstantiationException, IllegalAccessException {
        ConexaoBanco.iniciarConexao();
        
        try {
            EstoqueDAO.selectAlertas(90);
            LoginView login = new LoginView();
            login.setVisible(true);
        } catch (SQLException ex) {
           System.out.println(ex.getErrorCode()+": "+ex.getMessage());
        }
        
    }
    
    
    
}
