package Dominio;

public class Concentracao {
    private int id;
    private String concentracao;

    public Concentracao(int id, String concentracao){
        this.id = id;
        this.concentracao = concentracao;
    }
    public Concentracao(String concentracao){
        this.concentracao = concentracao;
    }
    public int getId() {
        return this.id;
    }
    public String getConcentracao() {
        return concentracao;
    }
    public void setConcentracao(String concentracao){
        this.concentracao = concentracao;
    }

    public String toString(){
        return concentracao;

    }
}
