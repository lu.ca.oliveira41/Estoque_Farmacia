package Dominio;

public class Medicamento {
    private int id;
    private String denominacao;
    private Apresentacao apresentacao;
    private Concentracao concentracao;
    private Modalidade modalidade;

    public Medicamento() {
    }
    
    public Medicamento(String denominacao,Apresentacao apresentacao,Concentracao concentracao,Modalidade modalidade){
        this.denominacao = denominacao;
        this.apresentacao = apresentacao;
        this.modalidade = modalidade;
        this.concentracao = concentracao;
    }
    public Medicamento(int id,String denominacao,Apresentacao apresentacao,Concentracao concentracao,Modalidade modalidade){
        this.id = id;
        this.denominacao = denominacao;
        this.apresentacao = apresentacao;
        this.modalidade = modalidade;
        this.concentracao = concentracao;
    }
    public int getId() {
        return id;
    }
     public void setId(int id) {
        this.id=id;
    }
    public String getDenominacao() {
        return denominacao;
    }
    public void setDenominacao(String denominacao) {
        this.denominacao = denominacao;
    }
    public Apresentacao getApresentacao() {
        return apresentacao;
    }
    public void setApresentacao(Apresentacao apresentacao) {
        this.apresentacao = apresentacao;
    }
    public Concentracao getConcentracao(){
        return concentracao;
    }
    public void setConcentracao(Concentracao concentracao){
        this.concentracao=concentracao;
    }
    
    public Modalidade getModalidade() {
        return modalidade;
    }
    public void setModalidade(Modalidade modalidade) {
        this.modalidade = modalidade;
    }

    public String toString(){
        return denominacao;
    }

}
