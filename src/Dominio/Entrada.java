package Dominio;

import java.util.Date;

public class Entrada {
    private Date data;
    private int codEntrada;
    private int quantidade;
    private Estoque estoque;
    public Entrada(int codEntrada,int quantidade,Date data, Estoque estoque){
        this.data = data;
        this.codEntrada = codEntrada;
        this.quantidade = quantidade;
        this.estoque = estoque;
    }
    public Entrada(int quantidade,Date data, Estoque estoque){
        this.data = data;
        this.quantidade = quantidade;
        this.estoque = estoque;
    }
    public Date getData() {
        return data;
    }
    public int getCodEntrada() {
        return codEntrada;
    }
    public int getQuantidade() {
        return quantidade;
    }
    public Estoque getEstoque() {
        return estoque;
    }
    public String toString(){
        return codEntrada+" -- "+data.toString();
    }
    public void setQtd(int qtd){
        this.quantidade=qtd;
    }
    
}
