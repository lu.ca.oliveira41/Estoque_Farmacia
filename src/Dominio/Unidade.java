package Dominio;

public class Unidade {
    private int id;
    private String unidade;

    public Unidade(int id, String unidade){
        this.id = id;
        this.unidade = unidade;
    }
    public Unidade(String unidade){
        this.unidade = unidade;
    }
    public int getId() {
        return id;
    }
    public String getUnidade() {
        return unidade;
    }
    public void setUnidade(String unidade){
        this.unidade = unidade;
    }
    public String toString(){
        return unidade;
    }
    
    
}
