package Dominio;


public class EstoquePPS extends Estoque{
    private ProdutoParaSaude pps;
    private int idEstoquePPS;
    private String nLote;
    public EstoquePPS(int id, String nLote,ProdutoParaSaude pps,Estoque estoque) {
        super(estoque.getId(),estoque.getQuantidade(),estoque.getDataValidade());
        this.pps = pps;
        this.idEstoquePPS = id;
        this.nLote = nLote;
        
    }
    public EstoquePPS(String nLote,ProdutoParaSaude pps,Estoque estoque) {
        super(estoque.getId(),estoque.getQuantidade(),estoque.getDataValidade());
        this.pps = pps;
        this.nLote = nLote;
        
    }
   
    public ProdutoParaSaude getPps() {
        return pps;
    }
    public int getIdEstoquePPS() {
        return idEstoquePPS;
    }
    public String getnLote() {
        return nLote;
    }
    public String toString(){
        return nLote +" "+pps;
    }
    
}
   