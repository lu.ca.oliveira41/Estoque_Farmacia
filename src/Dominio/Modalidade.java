package Dominio;

public class Modalidade {

   
    private int id;
    private String modalidade;

    public Modalidade(int id, String modalidade){
        this.id = id;
        this.modalidade = modalidade;
    }

    public Modalidade(String modalidade) {
       this.modalidade = modalidade;
    }
    public int getId() {
        return id;
    }
    public String getModalidade() {
        return modalidade;
    }

    public void setModalidade(String novoNome) {
       this.modalidade = novoNome;
    }
   
    @Override
    public String toString(){
        return this.modalidade;

    }
}
