package Dominio;



public class EstoqueMedicamento extends Estoque{
    
    private Medicamento medicamento;
    private int idEstoqueMedicamento;
    private String nLote;
         
    public EstoqueMedicamento(int id, String nLote,Medicamento medicamento,Estoque estoque) {
        super(estoque.getId(),estoque.getQuantidade(),estoque.getDataValidade());
        this.medicamento = medicamento;
        this.idEstoqueMedicamento = id;
        this.nLote = nLote;
    }
    public EstoqueMedicamento(String nLote,Medicamento medicamento,Estoque estoque) {
        super(estoque.getId(),estoque.getQuantidade(),estoque.getDataValidade());
        this.medicamento = medicamento;
        this.nLote = nLote;
    }
    
    public Medicamento getMedicamento(){
        return medicamento;
    }
    
    public int getIdEstoqueMedicamento() {
        return idEstoqueMedicamento;
    }
    public String getnLote() {
        return nLote;
    }
    public String toString(){
        return nLote+"  "+medicamento;
    }
    
}
