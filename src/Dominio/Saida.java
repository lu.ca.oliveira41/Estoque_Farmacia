package Dominio;

import java.util.Date;

public class Saida {
    private Date data;
    private int codSaida;
    private int quantidade;
    private Estoque estoque;
    
    public Saida( int codSaida,int quantidade,Date data, Estoque estoque){
        this.data = data;
        this.codSaida = codSaida;
        this.quantidade = quantidade;
        this.estoque = estoque;
    }
    public Saida(int quantidade,Date data, Estoque estoque){
        this.data = data;
        this.quantidade = quantidade;
        this.estoque = estoque;
    }
    public Date getData() {
        return data;
    }
    public int getCodSaida() {
        return codSaida;
    }
    public int getQuantidade() {
        return quantidade;
    }
    public Estoque getEstoque() {
        return estoque;
    }
    public String toString(){
        return codSaida+" -- "+data.toString();
    }
}
