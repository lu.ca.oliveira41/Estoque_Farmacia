package Dominio;


public class ProdutoParaSaude {
    private int id;
    private String descricao;
    private Unidade unidade;
    private Modalidade modalidade;
    
     public ProdutoParaSaude(String descricao,Unidade unidade,Modalidade modalidade){
        this.descricao = descricao;
        this.unidade = unidade;
        this.modalidade = modalidade;
    }
    public ProdutoParaSaude(int id,String descricao,Unidade unidade,Modalidade modalidade){
        this.id = id;
        this.descricao = descricao;
        this.unidade = unidade;
        this.modalidade = modalidade;
    }
    public int getId() {
        return id;
    }
    public void setId(int id){
        this.id=id;
    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public Unidade getUnidade() {
        return unidade;
    }
    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }
    public Modalidade getModalidade() {
        return modalidade;
    }
    public void setModalidade(Modalidade modalidade) {
        this.modalidade = modalidade;
    }
    public String toString(){
        return descricao;
    }
    
    

}
