package Dominio;

public class Usuario {
    private int id;
    private String nome;
    private String senha;

    public Usuario(int id,String nome,String senha){
        this.id=id;
        this.nome=nome;
        this.senha=senha;
    }
    public int getId() {
        return id;
    }
    
    public String getNome() {
        return nome;
    }
    
    public String getSenha() {
        return senha;
    }
    public void setNome(String nome){
        this.nome=nome;
    }
    public void setSenha(String senha){
        this.senha = senha;
    }
    public String toString(){
        return nome;
    }
    
}
