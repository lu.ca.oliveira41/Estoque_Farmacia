package Dominio;

public class ItemRelatorioMed implements Comparable<ItemRelatorioMed>{
    private String tipo;
    private EstoqueMedicamento estoquemed;
    private Entrada entrada;
    private Saida saida;
         
    public ItemRelatorioMed(EstoqueMedicamento estoque,Entrada entrada){
        this.tipo="Entrada";
        this.estoquemed= estoque;
        this.entrada=entrada;
        this.saida=null;
    }
    public ItemRelatorioMed(EstoqueMedicamento estoque,Saida saida){
        this.tipo="Saida";
        this.estoquemed= estoque;
        this.entrada=null;
        this.saida=saida;
    }
    
    @Override 
    public int compareTo(ItemRelatorioMed outroItem){
        if(this.tipo == "Entrada"){
            if(outroItem.getTipo() == "Entrada"){
                if(this.getEntrada().getData().compareTo(outroItem.getEntrada().getData())>0)
                    return 1;
                if(this.getEntrada().getData().compareTo(outroItem.getEntrada().getData())<0)
                    return -1;
                return 0;
            }
            else{
                if(this.getEntrada().getData().compareTo(outroItem.getSaida().getData())>0)
                    return 1;
                if(this.getEntrada().getData().compareTo(outroItem.getSaida().getData())<0)
                    return -1;
                return 0;
            }
        }else 
            if(outroItem.getTipo() == "Entrada"){
                if(this.getSaida().getData().compareTo(outroItem.getEntrada().getData())>0)
                    return 1;
                if(this.getSaida().getData().compareTo(outroItem.getEntrada().getData())<0)
                    return -1;
                return 0;
            }
            else{
                if(this.getSaida().getData().compareTo(outroItem.getSaida().getData())>0)
                    return 1;
                if(this.getSaida().getData().compareTo(outroItem.getSaida().getData())<0)
                    return -1;
                return 0;
            }
    }

    public String getTipo() {
        return tipo;
    }
    public EstoqueMedicamento getEstoquemed() {
        return estoquemed;
    }
    public Entrada getEntrada() {
        return entrada;
    }
    public Saida getSaida() {
        return saida;
    }
}