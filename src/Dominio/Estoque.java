package Dominio;

import java.util.Date;

public class Estoque {
    private int id;
    private int quantidade;
    private Date dataValidade;
    
    public Estoque(int id,int quantidade,Date dataValidade){
        this.id=id;
        this.quantidade=quantidade;
        this.dataValidade=dataValidade;
    }
    public Estoque(int quantidade,Date dataValidade){
        this.quantidade=quantidade;
        this.dataValidade=dataValidade;
    }
    public int getId() {
        return id;
    }
    public int getQuantidade() {
        return quantidade;
    }
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    public void setIdEstoque(int id){
        this.id = id;
    }
    public Date getDataValidade() {
        return dataValidade;
    }
    public void setDataValidade(Date dataValidade) {
        this.dataValidade = dataValidade;
    }
    public String toString(){
        return "validade: "+dataValidade.toString()+"-- qtd:"+quantidade;
    }
    
    
}
