package Dominio;

public class ItemRelatorioPPS implements Comparable<ItemRelatorioPPS>{
    private String tipo;
    private EstoquePPS estoquepps;
    private Entrada entrada;
    private Saida saida;
         
    public ItemRelatorioPPS(EstoquePPS estoque,Entrada entrada){
        this.tipo="Entrada";
        this.estoquepps= estoque;
        this.entrada=entrada;
        this.saida=null;
    }
    public ItemRelatorioPPS(EstoquePPS estoque,Saida saida){
        this.tipo="Saida";
        this.estoquepps= estoque;
        this.entrada=null;
        this.saida=saida;
    }
    
    @Override 
    public int compareTo(ItemRelatorioPPS outroItem){
         if(this.tipo == "Entrada"){
            if(outroItem.getTipo() == "Entrada"){
                if(this.getEntrada().getData().compareTo(outroItem.getEntrada().getData())>0)
                    return 1;
                if(this.getEntrada().getData().compareTo(outroItem.getEntrada().getData())<0)
                    return -1;
                return 0;
            }
            else{
                if(this.getEntrada().getData().compareTo(outroItem.getSaida().getData())>0)
                    return 1;
                if(this.getEntrada().getData().compareTo(outroItem.getSaida().getData())<0)
                    return -1;
                return 0;
            }
        }else 
            if(outroItem.getTipo() == "Entrada"){
                if(this.getSaida().getData().compareTo(outroItem.getEntrada().getData())>0)
                    return 1;
                if(this.getSaida().getData().compareTo(outroItem.getEntrada().getData())<0)
                    return -1;
                return 0;
            }
            else{
                if(this.getSaida().getData().compareTo(outroItem.getSaida().getData())>0)
                    return 1;
                if(this.getSaida().getData().compareTo(outroItem.getSaida().getData())<0)
                    return -1;
                return 0;
            }
    }
     
    

    public String getTipo() {
        return tipo;
    }
    public EstoquePPS getEstoquepps() {
        return estoquepps;
    }
    public Entrada getEntrada() {
        return entrada;
    }
    public Saida getSaida() {
        return saida;
    }
}