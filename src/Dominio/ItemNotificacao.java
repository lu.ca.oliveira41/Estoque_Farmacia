package Dominio;

import java.util.Date;

public class ItemNotificacao implements Comparable<ItemNotificacao>{
    private String tipo;
    private String lote;
    private String nome;
    private Date validade;
    public ItemNotificacao(String tipo,String lote,String nome,Date validade){
        this.lote=lote;
        this.tipo=tipo;
        this.nome=nome;
        this.validade=validade;
    }
    public String getTipo() {
        return tipo;
    }
    public String getLote() {
        return lote;
    }
    public String getNome() {
        return nome;
    }
    public Date getValidade() {
        return validade;
    }
    
    @Override 
    public int compareTo(ItemNotificacao outroItem){
        if(this.getValidade().compareTo(outroItem.getValidade())>0)
            return 1;
        if(this.getValidade().compareTo(outroItem.getValidade())<0)
            return -1;
         return 0;
    }
    
    
}
