package Dominio;

public class Apresentacao {
    private int id;
    private String apresentacao;

    public Apresentacao(int id, String apresentacao){
        this.id = id;
        this.apresentacao = apresentacao;
    }
    public Apresentacao( String apresentacao){
        this.apresentacao = apresentacao;
    }
    public int getId() {
        return id;
    }
    public String getApresentacao() {
        return apresentacao;
    }
    public void setApresentacao(String apresentacao){
        this.apresentacao = apresentacao;
    }

    public String toString(){
        return this.apresentacao;
    }
}
